<?php

/**
 * @title {Feedback controller}
 */

class FeedbackController extends ExtController
{
    public $layout = '//layouts/structLayout';
    public $modelClass = 'StructPage';

    public $formModelClass = 'FeedbackForm';
    public $formModel;
    public $_sent = NULL;


    public function actions()
    {
        return array(
            'captcha' => array(
                'foreColor' => 0x263d97,
                'class' => 'CCaptchaAction',
                'transparent' => true,
                'testLimit' => 1
            ),
        );
    }

    public function filters()
    {
        return array('captchaInit + captcha');
    }

    public function filterCaptchaInit($filterChain)
    {
        if (sizeof($_GET)) {
            $value = current($_GET);
            if ($value == 1) $_GET[CCaptchaAction::REFRESH_GET_VAR] = $value;
        }

        $filterChain->run();
    }


    /**
     * Категория, к которой привязан контроллер.
     * @var StructCategory
     */
    public $category = NULL;



    protected function beforeAction($action)
    {
        if($action->id != 'captcha')
        {
            // работает только как прикрепленный к категории контроллер
            if(!Yii::app()->struct->currentCategory)
                throw new CHttpException(404);

            $formModelClass = $this->formModelClass;
            $this->formModel = new $formModelClass;

            Yii::loadExtConfig($this, __FILE__);
            $this->category = Yii::app()->struct->currentCategory;
            $this->viewPath = Yii::app()->struct->structController->viewPath;

        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $this->render(
            Yii::app()->struct->structController->views['category'],
            array('category'=>$this->category)
        );
    }

    public function actionFeedback()
    {
        if(isset($_POST[get_class($this->formModel)]))
        {
            $this->formModel->setAttributes($_POST[get_class($this->formModel)]);
            if($this->formModel->validate())
            {
                $contentData = array();
                $labels = $this->formModel->attributeLabels();
                foreach($this->formModel->getAttributes() as $key => $value){
                    if(isset($labels[$key]))
                        $contentData[$labels[$key]] = $value;
                }

                Yii::app()->mailer->sendMessage
                    (
                        array
                        (
                            'to' => FeedbackOptionsForm::instance()->to_email,
                            'bcc' => FeedbackOptionsForm::instance()->bcc,
                            'content' => $contentData
                        ), 'common'
                    );

                $this->_sent = true;
                ExtCoreJson::message(Yii::t('custom', 'Спасибо, Ваша заявка отправлена.'));

                if(!Yii::app()->request->isAjaxRequest){
                    $this->_sent = true;
                    $this->forward('index');
                }
                else ExtCoreJson::response();
            }
            else
            {
                if(Yii::app()->request->isAjaxRequest)
                    ExtCoreJson::error(Yii::t('custom', 'Форма заполнена неправильно.'));
                else{
                    $this->_sent = false;
                    $this->forward('index');
                }
            }
        }
        else throw new CHttpException(404);
    }
}

?>
