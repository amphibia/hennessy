<?php
Yii::import(Yii::localExtension('structlandings', 'controllers.*'));

/**
 * @title {Main site controller}
 * @desc {Description}
 */

class SiteController extends LandingBaseController
{
	public $layout = '//layouts/mainPageLayout';

    public $modelClass = 'StructPage';

    /*
    public function actionInterview($lang = 'ru'){
        $category = Yii::app()->custom->getCategories('interview');
        
        $this->render(
            '/templates/interview/template',
            array('category'=>$category)
        );
    }
    */
}