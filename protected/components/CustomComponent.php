<?php

/**
 * @title Custom application components
 * @description { Initialize conjuncture of application. }
 * @preload yes
 * @vendor koshevy
 */
class CustomComponent extends CApplicationComponent
{

    public function init()
    {
        Yii::loadExtConfig($this, __FILE__);
        Yii::import('application.services.*');
        Yii::app()->registerService('feedback', 'FeedbackAdminService', 'admin',
            array('title' => 'Feedback email options', 'menuGroup' => 'application',
                'dictionary' => array('actions' => array('form' => 'Feedback email options'))
            )
        );
    }

    /**
     * Обращение к разделу главной страницы.
     */
    public function getMainPageCategory(){
        // Метод перенесен в StructComponent.
        // Оставлен здесь для обратной совместимости.
        return Yii::app()->struct->getMainPageCategory();
    }

    /**
     * Обращение к необходимым разделам
     * @param array|string $categories Массив ключей категорий, либо 1 значение
     * @return StructCategory|array из StructCategory
     */
    public function getCategories($categories){
        // Метод перенесен в StructComponent.
        // Оставлен здесь для обратной совместимости.
        return Yii::app()->struct->getCategories($categories);
    }

    /**
     * Заголовок страницы.
     * @return string
     */
    public function getPageTitle()
    {
        // Формирование заголовка страницы
        $title = Yii::t('custom', 'app_name');
        if (($currentInstance = Yii::app()->struct->currentPage) ||
            ($currentInstance = Yii::app()->struct->currentCategory)
        ) {
            $title = "{$title} &ndash; {$currentInstance->title}";
        } else {
            $controllerName = Yii::app()->controller->id;
            $actionName = Yii::app()->controller->action->id;
            if ($controllerName == 'site' && $actionName == 'index') {
                // Main page
                $mainPageCategory = Yii::app()->custom->getMainPageCategory();
                $title = "{$title} &ndash; {$mainPageCategory->title}";
            } else {
                $title2 = Yii::t('titles', ucfirst($controllerName) . ' ' . $actionName);
                $title = "{$title} &ndash; {$title2}";
            }
        }

        return $title;
    }

    /**
     * Язык браузера
     * @return string
     */
    public function getSystenLang()
    {
        //die($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
        preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]), $matches); // вычисляем соответствия с массивом $matches
        $langs = array_combine($matches[1], $matches[2]); // Создаём массив с ключами $matches[1] и значениями $matches[2]
        foreach ($langs as $n => $v)
            $langs[$n] = $v ? $v : 1; // Если нет q, то ставим значение 1
        arsort($langs); // Сортируем по убыванию q

        return key($langs); // Выводим язык по умолчанию
    }
    
}
