<?php

class ActiveRecord extends ExtCoreActiveRecord
{
    public function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => Yii::localExtension('AdvancedArBehavior', 'CAdvancedArBehavior')),
            'TimeStampArBehavior' => array(
                'class' => Yii::localExtension('TimeStampArBehavior', 'TimeStampArBehavior')),
        );
    }

    /**
     * Имя таблицы общее для всех объектов этого класса, а также может
     * использоваться как хелпер при описании внешних связей с этой моделью.
     *
     * @return string
     */
    public static function commonTableName($originalName)
    {
        static $tableName = NULL;
        if (!$tableName) {
            $postfix = NULL;
            if (Yii::app()->hasComponent('lang') && ($lang = Yii::app()->language) != Yii::app()->lang->defaultLanguage) {
                $postfix = "_$lang";
            }
            $tableName = "{{" . $originalName . "$postfix}}";
        }

        return $tableName;
    }
}
