<?

class FeedbackForm extends CFormModel
{
    public $name;
    public $phone;
    public $email;
    public $text;
    public $captcha;

    protected static $_instance = NULL;

    public function rules()
    {
        $rules = array(
            array('name, phone, email, text', 'required', "message" => Yii::t('custom', 'Поле обязательно для заполнения')),

            array
            (
                'name', 'length', 'min' => 5, 'max' => 64,
                'tooShort' => Yii::t('custom', 'Слишком короткое имя'),
                'tooLong' => Yii::t('custom', 'Слишком длинное имя'),
            ),
            array('phone', 'match', 'pattern' => '/^\+\d\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$/'),
            array('email', 'email', 'message' => Yii::t('custom', 'Неправильный электронный адрес')),
            array('captcha', 'captcha'),
        );

        return $rules;
    }

    public function beforeValidate(){
        $this->name = htmlspecialchars($this->name);
        $this->text = htmlspecialchars($this->text);
        return parent::beforeValidate();
    }

    public function relations()
    {
        return array();
    }

    public function attributeLabels(){
        return array(
            'name' => Yii::t('custom', 'Ваши имя и фамилия'),
            'phone' => Yii::t('custom', 'Контакнтый телефон'),
            'email' => Yii::t('custom', 'E-mail'),
            'text' => Yii::t('custom', 'Текст сообщения'),
            'captcha' => Yii::t('custom', 'Секретный код'),
        );
    }

}
