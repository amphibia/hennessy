<?
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content html
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self text gps_lat "Координата метки на карте (GPS Lat)"
 * @param self text gps_lng "Координата метки на карте (GPS Lng)"
 * @param self text gps_zoom "GPS Zoom"
 * @param self single_image marker_icon "Иконка на карте"
 * @param self textarea_512 _seo_keywords "Seo Keywords"
 * @param self textarea_512 _seo_description "Seo Description"
 * @param self form_editor form_data "Настройки формы отправки"
 *
 * @option children enabled on
 * @option children show_tech_fields on
 * @option children show_gallery off
 * @option children foreword none
 * @option children foreword_required off
 * @option children foreword_length_min none
 * @option children foreword_length_max none
 * @option children content none
 * @option children content_required off
 * @option children content_length_min none
 * @option children content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields off
 * @option pages show_gallery off
 * @option pages foreword none
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content text
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 */

?>
    <script>
        function afterYiiValidate($form, errors, hasError) {
            if (!hasError) {
                var data = $form.getFields();
                $form.addClass('loading');
                $form.request($form.attr('action'), data, function (data) {
                    var messages = data.__commands.message;
                    setTimeout(function () {
                        $form.removeClass('loading');
                        $form.html('');
                        for (var i in messages)
                            $form.append('<p class="' + (parseInt(data.result) ? 'success' : 'error') + '">' + messages[i] + '</p>');
                    }, 600);

                    $('html, body').animate({
                        scrollTop: ($form.offset().top - ((window.innerHeight / 2) - ($form.height() / 2)))
                    }, 800);
                });
            }

            return false;
        }
    </script>

    <div class="text"><?= $category->full_text ?></div>

<?php
if ($category->gps_lat && $category->gps_lng) {
    ?>

    <div id="googleMap" class="loading"></div>

    <?php
    if ($markerImage = Yii::app()->storage->decodeImages($category->marker_icon))
        $markerImageUrl = Yii::app()->storage->createUrl($markerImage[0]);
    ?>
    <script>
        var googleMapInitialize = function () {
            var myLatlng = new google.maps.LatLng(<?=$category->gps_lat?>, <?=$category->gps_lng?>);
            var myOptions = {
                zoom: <?=$category->gps_zoom ? $category->gps_zoom : 14 ?>,
                scrollwheel: false,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("googleMap"), myOptions);
            $(document).data('googleMap', map);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(<?=$category->gps_lat?>, <?=$category->gps_lng?>),
                map: map,
                flat: true,
                <? if(isset($markerImageUrl) && $markerImageUrl)
                 echo "icon: 'http://{$_SERVER['SERVER_NAME']}/{$markerImageUrl}'," ?>
                visible: true
            });

            $('#googleMap').removeClass('loading');
        }

        $(function ($) {
            // инициализация GoogleMaps
            if ($('#googleMap').length) {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=googleMapInitialize";
                document.body.appendChild(script);
            }
        });
    </script>
<?php
}


$form = $this->widget(Yii::localExtension('structlandings', 'MagicForm'), array(
    'id' => 'contacts-form',

    // Для форм во всплывающих окон данные свойство указываеться
    // с использованием метода Yii::app()->struct->getCategories(...).
    'source' => $category,

    // Подставляет нужную модель: универсальную болванку для формы (на ее
    // основе можно развернуть несколько подобных форм на странице),
    // либо именно ту модель, которая обработала данные из поста.
    // Проверка ID формы производится на тот случай, если со страницы было
    // отправлено несколько форм (чтобы определить именно ту, которая отправлялась).
    // У форм в вспллывающих окнах модель указывать не нужно.
    'model' => ($this->_sentFormModel && ($this->formID == 'contacts-form'))
        ? $this->_sentFormModel : $this->_formModel,

    'customField' => 'form_data',       // Поле категории, в котором хранятся настройки формы.
    'ajaxSend' => true,                 // Указывайте только этот параметр. Настройки валидации указывать не нужно.
    'htmlOptions' => array(
        'class' => 'block-form'
    )
));
