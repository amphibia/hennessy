<?
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self textarea_512 _seo_keywords "Seo Keywords"
 * @param self textarea_512 _seo_description "Seo Description"
 *
 * @option pages enabled on
 * @option pages show_tech_fields off
 * @option pages show_gallery on
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content html
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages text title_internal "Внутренний заголовок"
 * @param pages single_image img "Иллюстрация"
 * @param pages datepicker date_display "Дата публикации"
 * @param pages text video_title "Заголовок перед видео"
 * @param pages text video_link "Ссылка на видео"
 * @param pages tiny_mce text2 "Текст после видео"
 */
?>

<? define('PAGE_SIZE', 100); ?>

<?
$newsCategoryUrl = $category->createUrl();
$dataProvider = $this->model->getActiveDataProvider();
$dataProvider->pagination->pageSize = PAGE_SIZE;
$dataProvider->pagination->route = $newsCategoryUrl;
?>

<!-- .content -->
<main class="content">
    <? if ($dataProvider->totalItemCount): ?>
    <div class="events">
        <div class="container">
            <? foreach ($dataProvider->data as $index => $page): ?>
            <div class="event-item">
                <div class="event-date">
                    <div class="event-date_in"> <span><?= Yii::app()->dateFormatter->format("d", $page->date_display) ?></span> <?= Yii::app()->dateFormatter->format("MMMM", $page->date_display) ?> </div>
                </div>
                <div class="event-desc">
                    <div class="country"><?= nl2br($page->short_text) ?></div>
                    <div class="title"> <?= $page->title ?> </div> <a href="<?= $page->createUrl() ?>"><?= Yii::t('custom', 'Смотреть') ?></a> </div>
            </div>
            <? endforeach ?>
        </div>
    </div>
    <?php endif ?>
    <div class="interview-animate-figures animate-on">
        <div class="figure-left">
            <div class="figure figure-small"></div>
            <div class="figure figure-medium"></div>
            <div class="figure figure-large"></div>
        </div>
        <div class="figure-right">
            <div class="figure figure-large"></div>
            <div class="figure figure-medium"></div>
            <div class="figure figure-small"></div>
        </div>
    </div>
</main>
<!-- end .content -->
