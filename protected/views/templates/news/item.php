<!-- .content -->
<main class="content">
    <div class="events inside">
        <div class="container">
            <div class="content-in">
                <div class="title-date"><?= Yii::app()->dateFormatter->format("d MMMM", $page->date_display) ?></div>
                <h3><?= $page->title_internal ?></h3>
                <?= $page->full_text ?>
            </div>

            <div class="content-in">
                <?php if($page->video_link): ?>
                <div class="video-block"> <iframe width="100%" height="430px" src="<?= $page->video_link ?>&modestbranding=1&autohide=1&showinfo=0" frameborder="0" gesture="media" allowfullscreen></iframe> </div>
                <?php endif ?>
                <br/><h3><?= $page->video_title ?></h3>
                <?= $page->text2 ?>
            </div>

            <?php if ($images = Yii::app()->storage->decodeImages($page->gallery)): ?>
                <?php if(count($images) > 1): ?>
                <div class="slider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php foreach($images as $img): ?>
                                <div class="swiper-slide"><img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" alt="" class="img-responsive"></div>
                            <?php endforeach ?>
                        </div>
                        <div class="slider-controls">
                            <div class="btn-slide btn-slide-prev"><i class="icon icon-arrow-left"></i></div>
                            <div class="btn-slide btn-slide-next"><i class="icon icon-arrow-right"></i></div>
                        </div>
                    </div>
                </div>
                <?php else: ?>
                    <div class="img-block"> <img src="<?= Yii::app()->storage->createUrl($images[0], 'original') ?>" alt="" class="img-responsive"> </div>
                <?php endif ?>
            <?php endif ?>
        </div>
    </div>
    <div class="interview-animate-figures animate-on">
        <div class="figure-left">
            <div class="figure figure-small"></div>
            <div class="figure figure-medium"></div>
            <div class="figure figure-large"></div>
        </div>
        <div class="figure-right">
            <div class="figure figure-large"></div>
            <div class="figure figure-medium"></div>
            <div class="figure figure-small"></div>
        </div>
    </div>
</main>
<!-- end .content -->
