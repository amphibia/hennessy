<?
if($page->issetCustomField('img')){
    $images = Yii::app()->storage->decodeImages($page->img);
    if($images[0]){
        echo CHtml::image(Yii::app()->storage->createUrl($images[0], 'inContent'), $page->title, array('class' => 'imgInContent'));
    }
}

if($page->issetCustomField('date_display'))
    echo CHtml::tag('div', array('class' => 'date'), Yii::app()->dateFormatter->formatDateTime($page->date_display, 'medium', null));
?>
    <h3><strong><?= $page->title ?></strong></h3>
    <div id="fullText"><?= $page->full_text ?></div>
<?
if(($gallery = $page->gallery) && sizeof($gallery = explode(',', $gallery)))
{
    ?>
    <div id="gallery">
        <div class="sub">
            <?
            foreach($gallery as $image)
            {
                $smallUrl = Yii::app()->storage->createUrl($image, 'galleryImageSmall');
                $fullUrl = Yii::app()->storage->createUrl($image);

                echo CHtml::openTag('a',
                    array('class'=>'item loading', 'href'=>$fullUrl, 'rel'=>'gallery')
                );
                echo CHtml::image($smallUrl);
                echo CHtml::closeTag('a');
            }
            ?>
            <div class="clear"></div>
        </div>
    </div>
<?
}
?>