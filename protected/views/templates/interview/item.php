<!-- .content -->
<main class="content">
    <?php
    $imgUrl = '/dist/images/samples/infopic06.jpg';
    if ($image = Yii::app()->storage->decodeImages($page->img)){
        $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
    }
    ?>
    <div class="interview-inside-promo in-<?= $page->alignText ?>" style="background-image: url(<?= $imgUrl ?>)">
        <div class="container">
            <div class="m-interview-inside-pic">
                <img src="<?= $imgUrl ?>" alt="">
            </div>
            <div class="interview-inside-promo-desc">
                <div class="interview-desc-in"> <b><?= $page->city ?></b>
                    <h2><?= $page->title ?></h2>
                    <?= $page->short_text ?>
                </div>
            </div>
        </div>
    </div>
    <section class="section section-interview-inside">
        <div class="interview-animate-figures">
            <div class="figure-left">
                <div class="figure figure-small"></div>
                <div class="figure figure-medium"></div>
                <div class="figure figure-large"></div>
            </div>
            <div class="figure-right">
                <div class="figure figure-large"></div>
                <div class="figure figure-medium"></div>
                <div class="figure figure-small"></div>
            </div>
        </div>
        <div class="interview-inside-desc">
            <div class="container">
                <div class="inside-img-desc">
                    <?php
                    $imgUrl = '/dist/images/samples/inside-img-desc-pic.jpg';
                    if ($image = Yii::app()->storage->decodeImages($page->img_biography)){
                        $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
                    }
                    ?>
                    <div class="img-desc"> <img src="<?= $imgUrl ?>" alt=""> </div>
                    <div class="img-text"> <b><?= Yii::t('custom', 'БИОГРАФИЯ') ?></b>
                        <?= $page->full_text ?>
                    </div>
                </div>
                <div class="inside-info-desc">
                    <?php if ($images = Yii::app()->storage->decodeImages($page->questions1)): ?>
                        <?php foreach($images as $img): ?>
                            <div class="info-desc-item"> <b><?= $img['title'] ?></b>
                                <p><?= nl2br($img['description']) ?></p>
                            </div>

                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>
            <?php
            $imgUrl = '/dist/images/samples/desc-img-02.jpg';
            if ($image = Yii::app()->storage->decodeImages($page->img_bg1)){
                $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
            }
            ?>
            <div class="inside-gallery-desc" style="background-image: url(<?= $imgUrl ?>)">
                <?php if(trim($page->title1)): ?>
                <div class="container">
                    <div class="gallery-title">
                        <h2><?= nl2br($page->title1) ?></h2>
                    </div>
                </div>
                <?php endif ?>
            </div>

            <div class="container">
                <div class="inside-info-desc">
                    <?php if ($images = Yii::app()->storage->decodeImages($page->questions2)): ?>
                        <?php foreach($images as $img): ?>
                            <div class="info-desc-item"> <b><?= $img['title'] ?></b>
                                <p><?= nl2br($img['description']) ?></p>
                            </div>

                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>

            <?php
            $imgUrl = '/dist/images/samples/desc-img-02.jpg';
            if ($image = Yii::app()->storage->decodeImages($page->img_bg2)){
                $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
            }
            ?>
            <div class="inside-gallery-desc" style="background-image: url(<?= $imgUrl ?>)">
                <?php if(trim($page->title2)): ?>
                <div class="container">
                    <div class="gallery-title">
                        <h2><?= nl2br($page->title2) ?></h2>
                    </div>
                </div>
                <?php endif ?>
            </div>

            <div class="container">
                <div class="inside-info-desc">
                    <?php if ($images = Yii::app()->storage->decodeImages($page->questions3)): ?>
                        <?php foreach($images as $img): ?>
                            <div class="info-desc-item"> <b><?= $img['title'] ?></b>
                                <p><?= nl2br($img['description']) ?></p>
                            </div>

                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>
            <!-- New block -->
            <?php
            $imgUrl = '/dist/images/osm-0388.jpg';
            if ($image = Yii::app()->storage->decodeImages($page->work_img)){
                $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
            }
            ?>
            <div class="portfolio-link-block" style="background-image: url(<?= $imgUrl ?>)">
                <div class="block-in">
                    <span class="title"><?= $page->work_title ?></span>
                    <span class="desc"><?= $page->work_desc ?></span>
                    <a href="<?= $page->work_link ?>" class="link">
                        <span><?= Yii::t('custom', 'Смотреть') ?></span>
                    </a>
                </div>
            </div>
            <div class="pages-change">
                <?php
                $criteria = new CDbCriteria();
                $criteria->addCondition('t.position='.($page->position-1));
                $pagePrev = StructPage::model()->categories(array($category->id))->find($criteria);
                if($pagePrev):
                ?>
                <a href="<?= $pagePrev->createUrl() ?>" class="col prev">
                    <div class="block-in">
                        <span class="title"><?= $pagePrev->city ?></span>
                        <span class="desc"><?= $pagePrev->title ?></span>
                        <span class="arrow">
                            <i class="icon icon-arrow-left"></i>
                        </span>
                    </div>
                </a>
                <?php endif ?>
                <?php
                $criteria = new CDbCriteria();
                $criteria->addCondition('t.position='.($page->position+1));
                $pageNext = StructPage::model()->categories(array($category->id))->published()->find($criteria);
                if($pageNext):
                ?>
                <a href="<?= $pageNext->createUrl() ?>" class="col next">
                    <div class="block-in">
                        <span class="title"><?= $pageNext->city ?></span>
                        <span class="desc"><?= $pageNext->title ?></span>
                        <span class="arrow">
                            <i class="icon icon-arrow-right"></i>
                        </span>
                    </div>
                </a>
                <?php endif ?>
            </div>
            <!-- end new block -->
        </div>
        <!-- <div class="interview-inside-gallery">
<div class="container">
    <div class="gallery-title">
        <h2>В своем проекте я хочу рассказать о впечатляющем качестве, о том, что может заполнить образовавшуюся в людях пустоту.</h2>
    </div>
</div>
<div class="gallery-items">
    <div class="gallery-item" style="background-image: url(./images/samples/desc-img-03.jpg)"></div>
</div>
</div> -->
        <!-- <div class="interview-inside-animation-gallery">
<div class="animation-gallery">
    <div class="container">
        <div class="animation-gallery-title">
            <h2>Одиссея вкуса</h2>
            <b>Дегустационная комиссия Hennessy выделила семь нот букета Hennesy X.O. в 7 отдельных глав, которые плавно перетекают одна в другую. Откройте их для себя!</b>
        </div>
    </div>
</div>
</div> -->
    </section>
</main>
<!-- end .content -->
