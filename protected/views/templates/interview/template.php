<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword html
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content html
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages text city "Город"
 * @param pages dropdownlist alignText "Позиция текста в баннере" "right,left"
 * @param pages single_image img_biography "Иллюстрация в биографии"
 * @param pages gallery questions1 "Вопросы 1"
 * @param pages single_image img_bg1 "Баннер 1"
 * @param pages full_textarea title1 "Заголовок в баннере 1"
 * @param pages gallery questions2 "Вопросы 2"
 * @param pages single_image img_bg2 "Баннер 2"
 * @param pages full_textarea title2 "Заголовок в баннере 2"
 * @param pages gallery questions3 "Вопросы 3"
 * @param pages single_image work_img "Работа иллюстрация"
 * @param pages text work_title "Работа заголовок"
 * @param pages text work_desc "Работа Содержание"
 * @param pages text work_link "Работа ссылка"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

?>

<!-- .content -->
<main class="content">
    <section class="section section-promo">
        <?php
        $imgUrl = '';
        if ($image = Yii::app()->storage->decodeImages($category->img)){
            $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
        }
        ?>
        <div class="promo-slide" style="background-image: url(<?= $imgUrl ?>)">
            <div class="promo-slide-img">
                <img src="<?= $imgUrl ?>" alt=""> <!-- отдельно подключить для мобилы -->
            </div>
            <div class="container">
                <div class="promo-words">
                    <div class="words"><img src="/dist/images/svg/words.svg" alt=""></div>
                </div>
                <div class="watch-video-link">
                    <a href="#" class="link-open-video" data-toggle="modal" data-target="#modalVideo">
                        <div class="video-icon-link">
                            <img src="/dist/images/svg/icon-play.svg" alt="">
                        </div>
                        <b><?= Yii::t('custom', 'Посмотреть /nвидео', array('/n' => '<br/>')) ?></b>
                    </a>
                </div>
                <div class="promo-map">
                    <div class="inside-items">
                        <div class="map"> <img src="/dist/images/svg/promo-map.svg" alt=""> </div>
                        <div class="road">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="394" height="65" viewBox="0 0 394 65">
                                <path id="a" d="M767 290.79s17.27-.36 24.68 3.22c7.41 3.57 18.53 17.21 22.75 20.45 4.22 3.24 7.44 2.77 16.14 0 8.71-2.77 17.41-6.64 25.62-3.32 8.2 3.32 11.49 8.32 25.16 9.78 13.68 1.46 23.88-3.23 31.7-7.45 7.82-4.22 12.02 1 17.44 4.22 5.42 3.23 19.47 7.01 33.87-8.34 14.41-15.34 25.94-49.26 40.32-44.98 14.39 4.28-3.4 33.49-5.2 40.66-1.81 7.17-2.62 14.53 1.75 17.19 4.37 2.67 8.39-3.98 10.53-9.75 2.14-5.76 7.09-8.41 19.79-5.76 12.7 2.64 29.04 31.97 126.54 12.52"/>
                                <g transform="translate(-766 -263)">
                                    <use fill="#fff" fill-opacity="0" stroke="#8e734c" stroke-dasharray="5 8" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" xlink:href="#a"/>
                                </g>
                            </svg>
                        </div>
                        <div class="pins">
                            <div class="pin"></div>
                            <div class="pin"></div>
                            <div class="pin"></div>
                            <div class="pin"></div>
                            <div class="pin"></div>
                            <div class="pin"></div>
                            <div class="pin"></div>
                            <div class="pin"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-logo">
            <div class="logo"> <img src="/dist/images/svg/logo.svg" alt=""> </div>
        </div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="video-player">
                        <div class="block-in">
                            <?php if(Yii::app()->language == 'ru'): ?>
                            <video class="video-modal" id="video" controls>
                                <source src="/dist/video/video.mp4" type="video/mp4">
                                <source src="/dist/video/video.webm" type="video/webm">
                            </video>
                            <?php else: ?>
                            <video class="video-modal" id="video" controls>
                                <source src="/dist/video/video-en.mp4" type="video/mp4">
                                <source src="/dist/video/video-en.webm" type="video/webm">
                            </video>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-close"> <button type="button" class="btn btn-default" data-dismiss="modal">×</button> </div>
    </div>
    <section class="section section-info-slides">
        <?php foreach($category->pages as $page): ?>
            <?php
            $imgUrl = '/dist/images/samples/infopic01.jpg';
            if ($image = Yii::app()->storage->decodeImages($page->img)){
                $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
            }
            ?>
        <div class="info-slide" style="background-image: url(<?= $imgUrl ?>)">
            <div class="container">
                <div class="m-info-slide-img">
                    <img src="<?= $imgUrl ?>" alt="">  <!-- отдельно подключить для мобилы -->
                </div>
                <div class="info-slide-about">
                    <div class="info-in"> <b><?= $page->city ?></b>
                        <h2><?= $page->title ?></h2>
                        <?= $page->short_text ?>
                    </div>
                    <div class="info-link"> <a href="<?= $page->createUrl() ?>" class="link-more"><?= Yii::t('custom', 'Читать') ?></a> </div>
                </div>
            </div>
        </div>
        <?php endforeach ?>
    </section>
</main>
<!-- end .content -->

