<?
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword text
 * @option self content text
 *
 * @param self full_textarea code_head "Код в HEAD"
 * @param self full_textarea code_before_end_body "Код в конце BODY"
 *
 * @param self text years "Года"
 * @param self document doc_regulations "Документ правила"
 * @param self document doc_conditions "Документ условия"

 * @param self text facebook "facebook"
 * @param self text youtube "youtube"

 * @option children enabled on
 * @option children show_tech_fields on
 * @option children show_gallery off
 * @option children foreword text
 * @option children content html
 *
 */

//Переход на языковую версию пользователя
$systemLang = (explode('-', Yii::app()->custom->getSystenLang())[0]);
if(Yii::app()->language != $systemLang){
    if (in_array($systemLang, Yii::app()->lang->languages)){
        $this->redirect('?language='.$systemLang);
    }
}

$categoryInterview = Yii::app()->custom->getCategories('interview');
?>

<!-- .content -->
<main class="content">
    <div class="animate-figures">
        <div class="figure-left"> <img src="/dist/images/svg/figure.svg" class="animated fadeIn" alt=""> </div>
        <div class="figure-right"> <img src="/dist/images/svg/figure.svg" class="animated fadeIn" alt=""> </div>
    </div>
    <div class="section section-index-select">
        <div class="container">
            <div class="index-select-title">
                <h2><?= nl2br($category->short_text) ?></h2>
            </div>
            <div class="index-select-items">
                <div class="slect-item"> <select class="lang-select">
                        <option><?= Yii::t('custom', 'ЯЗЫК') ?></option>
                        <?php foreach (Yii::app()->lang->languages as $key => $value): ?>
                        <option value="<?= $key ?>"><?= $value['long'] ?></option>
                        <?php endforeach ?>
                    </select> </div>
                <div class="slect-item"> <select class="date-select" disabled="disabled">
                        <option><?= Yii::t('custom', 'ГОД РОЖДЕНИЯ') ?></option>
                        <?php
                        $years = $category->years = explode("-", $category->years);
                        //die("<pre>".print_r($category->years, true)."</pre>");
                        $year = (isSet($years[1]))?$years[1]:0;
                        $yearMin = $category->years[0];
                        for($year; $year >= $yearMin; $year--):
                        ?>
                        <option value="<?= $year ?>"><?= $year ?></option>
                        <?php endfor ?>
                    </select> </div>
                <div class="select-success-btn"> <button class="btn btn-success" data-url="<?= $categoryInterview->createUrl() ?>" disabled="disabled"><?= Yii::t('custom', 'ПРОДОЛЖИТЬ') ?></button> </div>
                <div class="prediction">

                    <p><?= Yii::t('custom', 'Войдя на сайт, вы соглашаетесь с нашими') ?>
                        <? if ($category->issetCustomField('doc_regulations') && ($doc = Yii::app()->storage->decodeFiles($category->doc_regulations))): ?>
                            <a href="http://<?= $docs[0]['fileName'] ?>"><?= $docs[0]['title'] ?></a>
                        <? endif ?>
                        <?= Yii::t('custom', 'и') ?>
                        <? if ($category->issetCustomField('doc_conditions') && ($doc = Yii::app()->storage->decodeFiles($category->doc_conditions))): ?>
                            <a href="http://<?= $docs[0]['fileName'] ?>"><?= $docs[0]['title'] ?></a>
                        <? endif ?>
                 </div>
            </div>
            <div class="index-select-footer"> <small>
                    <?= nl2br($category->full_text) ?>
                </small> </div>
        </div>
    </div>
</main>
<!-- end .content -->