<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content html
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self single_image img "Иллюстрация к статье"
 * @param self full_textarea list "Перечисление"
 * @param self text title2 "Заголовк 2"
 * @param self text title3 "Заголовк 3"
 * @param self full_textarea text2 "Текст 2"
 * @param self tiny_mce text3 "Текст 3"
 * @param self gallery gallery1 "Галлерея 3"
 * @param self tiny_mce text4 "Текст 4"
 * @param self gallery gallery4 "Галлерея 4"

 * @param self text title_bottom "Заголовок внизу"
 * @param self single_image img1 "Иллюстрация 1"
 * @param self text link1 "Ссылка 1"
 * @param self single_image img2 "Иллюстрация 2"
 * @param self text link2 "Ссылка 2"
 * @param self single_image img3 "Иллюстрация 3"
 * @param self text link3 "Ссылка 3"
 * @param self single_image img4 "Иллюстрация 4"
 * @param self text link4 "Ссылка 4"
 * @param self single_image img5 "Иллюстрация 5"
 * @param self text link5 "Ссылка 5"
 * @param self single_image img6 "Иллюстрация 6"
 * @param self text link6 "Ссылка 6"
 * @param self single_image img7 "Иллюстрация 7"
 * @param self text link7 "Ссылка 7"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

?>

<!-- .content -->
<main class="content">
    <section class="about">
        <div class="promo-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5"> <img class="img-title" src="/dist/images/svg/words.svg">
                        <?= $category->full_text ?>
                    </div>
                    <?php if ($image = Yii::app()->storage->decodeImages($category->img)): ?>
                    <div class="col-lg-7">
                        <div class="about-map"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""> </div>
                    </div>
                    <?php endif ?>
                </div>

            </div>
        </div>
        <div class="content-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <div class="titles-block">
                            <div class="text-center">
                                <h1 class="content-title"><?= $category->title2 ?></h1>
                                <h2 class="content-subtitle"><?= nl2br($category->text2) ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-block">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $category->text3 ?>
                        </div>
                        <div class="col-lg-6">
                            <div class="promo-carousel">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php if ($images = Yii::app()->storage->decodeImages($category->gallery1)): ?>
                                            <?php foreach($images as $img): ?>
                                                <div class="swiper-slide"> <img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" alt="" class="img-fluid"> </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div class="promo-carousel-arrow promo-carousel-next"> <i class="icon icon-arrow-right"></i> </div>
                                <div class="promo-carousel-arrow promo-carousel-prev"> <i class="icon icon-arrow-left"></i> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <h1 class="content-title text-center"><?= $category->title3 ?></h1>
                <br>
                <div class="carousel-block">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="promo-carousel">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <?php if ($images = Yii::app()->storage->decodeImages($category->gallery4)): ?>
                                        <?php foreach($images as $img): ?>
                                            <div class="swiper-slide"> <img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" alt="" class="img-fluid"> </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="promo-carousel-arrow promo-carousel-next">
                                <i class="icon icon-arrow-right"></i>
                            </div>
                            <div class="promo-carousel-arrow promo-carousel-prev">
                                <i class="icon icon-arrow-left"></i>
                            </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <?= $category->text4 ?>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <h1 class="content-title text-center"><?= $category->title_bottom ?></h1>
                </div>
                <div class="diamond-grid__centered">
                    <div class="diamond-grid">
                        <div class="card-items">
                            <?php if ($image = Yii::app()->storage->decodeImages($category->img1)): ?>
                                 <div class="card-item card-item__medium-1">
                                    <div class="pic-item" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                                    <a href="<?= $category->link1 ?>" class="box">
                                        <div class="box-in">
                                        <span class="box-title"><?= $image[0]['title'] ?></span>
                                        <span class="box-desc"><?= $image[0]['description'] ?></span>
                                        </div>
                                    </a>
                                </div>
                            <?php endif ?>
                            <?php if ($image = Yii::app()->storage->decodeImages($category->img2)): ?>
                                 <div class="card-item card-item__medium-2">
                                    <div class="pic-item" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                                    <a href="<?= $category->link2 ?>" class="box">
                                        <div class="box-in">
                                            <span class="box-title"><?= $image[0]['title'] ?></span>
                                            <span class="box-desc"><?= $image[0]['description'] ?></span>
                                        </div>
                                    </a>
                                </div>
                            <?php endif ?>
                            <div class="card-item card-item__clear"></div>
                        </div>
                        <div class="card-items">
                            <div class="card-item__group">
                                <div class="card-item card-item__clear"></div>
                                <?php if ($image = Yii::app()->storage->decodeImages($category->img3)): ?>
                                     <div class="card-item card-item__medium">
                                        <div class="pic-item" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                                        <a href="<?= $category->link3 ?>" class="box">
                                            <div class="box-in">
                                                <span class="box-title"><?= $image[0]['title'] ?></span>
                                                <span class="box-desc"><?= $image[0]['description'] ?></span>
                                            </div>
                                        </a>
                                    </div>
                                <?php endif ?>
                            </div>
                            <?php if ($image = Yii::app()->storage->decodeImages($category->img4)): ?>
                                <div class="card-item card-item__large">
                                    <div class="pic-item" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                                    <a href="<?= $category->link4 ?>" class="box">
                                        <div class="box-in">
                                            <span class="box-title"><?= $image[0]['title'] ?></span>
                                            <span class="box-desc"><?= $image[0]['description'] ?></span>
                                        </div>
                                    </a>
                                </div>
                            <?php endif ?>
                            <?php if ($image = Yii::app()->storage->decodeImages($category->img5)): ?>
                                <div class="card-item card-item__large">
                                    <div class="pic-item" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                                    <a href="<?= $category->link5 ?>" class="box">
                                        <div class="box-in">
                                            <span class="box-title"><?= $image[0]['title'] ?></span>
                                            <span class="box-desc"><?= $image[0]['description'] ?></span>
                                        </div>
                                    </a>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="card-items">
                            <?php if ($image = Yii::app()->storage->decodeImages($category->img6)): ?>
                                <div class="card-item card-item__large">
                                    <div class="pic-item" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                                    <a href="<?= $category->link6 ?>" class="box">
                                        <div class="box-in">
                                            <span class="box-title"><?= $image[0]['title'] ?></span>
                                            <span class="box-desc"><?= $image[0]['description'] ?></span>
                                        </div>
                                    </a>
                                </div>
                            <?php endif ?>
                            <?php if ($image = Yii::app()->storage->decodeImages($category->img7)): ?>
                                <div class="card-item card-item__medium">
                                    <div class="pic-item" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                                    <a href="<?= $category->link7 ?>" class="box">
                                        <div class="box-in">
                                            <span class="box-title"><?= $image[0]['title'] ?></span>
                                            <span class="box-desc"><?= $image[0]['description'] ?></span>
                                        </div>
                                    </a>
                                </div>
                            <?php endif ?>
                            <div class="card-item card-item__clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- end .content -->

