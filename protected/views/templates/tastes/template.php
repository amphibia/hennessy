<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword text
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content text
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self text title2 "Заголовок 2"
 * @param self gallery gallery_top "Верхняя галлерея"
 * @param self gallery gallery_bottom "Нижняя галлерея"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

?>

<!-- .content -->
<main class="content">
    <section class="section section-tastes">
        <div class="container">
            <div class="tastes-promo">
                <video id="tastes-video" autoplay>
                    <source src="./dist/video/hennessy_bottles.mp4" type="video/mp4">
                    <source src="./dist/video/hennessy_bottles.webm" type="video/webm">
                </video>
            </div>
            <div class="tastes-info-desc">
                <h1><?= $category->title2 ?></h1>
                <div class="desc">
                    <p><?= nl2br($category->short_text) ?></p>
                    <p><?= nl2br($category->full_text) ?></p>
                </div>
            </div>
            <div class="tastes-items">
                <?php if ($images = Yii::app()->storage->decodeImages($category->gallery_bottom)): ?>
                    <?php foreach($images as $img): ?>
                        <div class="item">
                            <div class="item-img"> <img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" alt=""> </div>
                            <div class="item-desc"> <b><?= $img['title'] ?></b>
                                <p><?= nl2br($img['description']) ?></p>
                            </div>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </div>
    </section>
</main>
<!-- end .content -->

