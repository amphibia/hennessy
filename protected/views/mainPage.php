<?php
/**
 * Представление главной страницы.
 * Вызывается в методе SiteController::actionIndex().
 * Для получения информации об аргументах, смотрите реализацию этого метода.
 */

$category = Yii::app()->custom->getMainPageCategory();
if ($category) {
    include Yii::app()->struct->getTemplatePath('category', $category->template);
}