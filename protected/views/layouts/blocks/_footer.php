    <!-- .footer -->
    <footer class="footer">
    <div class="container">
        <div class="footer-logotype">
        <a href="/"><img src="/dist/images/logotype-dark.svg" alt=""></a>
        </div>
        <div class="footer-copyright">
        © <?= Yii::t('custom', '2017, Hennessy X.O. Все права защищены.') ?>
        </div>
        <div class="footer-social">
        <ul>
            <li><a href="<?= $mainPageCategory->facebook ?>"><i class="icon icon-facebook"></i></a></li>
            <li><a href="<?= $mainPageCategory->youtube ?>"><i class="icon icon-youtube"></i></a></li>
        </ul>
        </div>
        <div class="footer-made_in">
        <?= Yii::t('custom', 'Сделано в') ?> <a href="http://amphibia.kz">Amphibia</a>
        </div>
    </div>
    <div class="disclimer">
        <div class="container">
        <p><?= Yii::t('custom', 'Чрезмерное употребление алкоголя опасно для вашего здоровья') ?></p>
        </div>
    </div>
    </footer><!-- end .footer -->

</div>
</div>
<!-- end .wrapper -->
<?= $mainPageCategory->code_before_end_body ?>
</body>

</html>