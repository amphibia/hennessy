<?php
$categories = Yii::app()->custom->getCategories(array('mainMenu'));

// Атрибуты тега body
$bodyAttributes = array();
if (isset($currentCategory)) {
    if(Yii::app()->struct->currentTemplate)
    {
        $bodyAttributes['data-template'] = Yii::app()->struct->currentTemplate;
    }
    else{
        $bodyAttributes['data-template'] = 'mainpage';
    }
}

Yii::app()->clientScript->scriptMap = array('jquery.js' => false);
//Yii::app()->clientScript->registerScriptFile('/js/jquery-1.11.2.min.js');

Yii::app()->clientScript->registerCssFile('/dist/styles/vendor.min.css');
Yii::app()->clientScript->registerCssFile('/dist/styles/main.min.css');
//Yii::app()->clientScript->registerScriptFile('/js/forms.js');
//Yii::app()->clientScript->registerScriptFile('/dist/scripts/vendor.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('/dist/scripts/main.min.js', CClientScript::POS_END);

?>
<!doctype html>
<html lang="ru">

<head>
    <script src="/dist/scripts/vendor.min.js"></script>

    <!-- IE SUPPORT -->
    <!--[if lte IE 9]>
    <script src="/js/libs/jquery.placeholder.min.js"></script>
    <script>$().ready(function(){ $('[placeholder]').placeholder(); });</script>
    <![endif]-->

    <meta charset="utf-8" />
    <?php
    if (
        (($currentCategoryOrPage = Yii::app()->struct->currentPage) || ($currentCategoryOrPage = Yii::app()->struct->currentCategory))
        && $currentCategoryOrPage->issetCustomField('_seo_keywords') && $currentCategoryOrPage->_seo_keywords
        && $currentCategoryOrPage->issetCustomField('_seo_description') && $currentCategoryOrPage->_seo_description
    ):
        ?>
        <meta name="keywords" content="<?= $currentCategoryOrPage->_seo_keywords ?>" />
        <meta name="description" content="<?= $currentCategoryOrPage->_seo_description ?>" />
    <?php elseIf(
        $mainPageCategory
        && $mainPageCategory->issetCustomField('_seo_keywords') && $mainPageCategory->_seo_keywords
        && $mainPageCategory->issetCustomField('_seo_description') && $mainPageCategory->_seo_description
    ): ?>
        <meta name="keywords" content="<?= $mainPageCategory->_seo_keywords ?>" />
        <meta name="description" content="<?= $mainPageCategory->_seo_description ?>" />
    <?php endIf; ?>

    <title><?= Yii::app()->custom->getPageTitle() ?></title>

    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="theme-color" content="#000000">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">


    <?= $mainPageCategory->code_head ?>
</head>

<body <?= CHtml::renderAttributes($bodyAttributes) ?>>

<!--[if lt IE 10]>
<p class="browserupgrade">
    You are using an <strong>outdated</strong> browser.
    Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<![endif]-->
<!-- .wrapper -->
<div class="wrapper">
    <div class="wrapper-in">
        <!-- .header -->

        <div class="m-header">
            <div class="m-logo">
                <a href="/main"><img src="/dist/images/svg/m-logo.svg" alt=""></a>
            </div>
            <div class="link-menu">
                <span></span>
            </div>
        </div>

        <div class="m-nav">
            <ul>
                <?php $categoryAbout = Yii::app()->custom->getCategories('about'); ?>
                <li><a href="<?= $categoryAbout->createUrl() ?>"><?= $categoryAbout->title ?></a></li>
                <?php $categoryTastes = Yii::app()->custom->getCategories('tastes'); ?>
                <li><a href="<?= $categoryTastes->createUrl() ?>"><?= $categoryTastes->title ?></a></li>
                <?php $categoryNews = Yii::app()->custom->getCategories('news'); ?>
                <li><a href="<?= $categoryNews->createUrl() ?>"><?= $categoryNews->title ?></a></li>

            </ul>
        </div>

        <header class="header">
            <div class="container">
                <div class="col">
                    <div class="left-nav">
                        <ul>
                            <?php $categoryAbout = Yii::app()->custom->getCategories('about'); ?>
                            <li><a href="<?= $categoryAbout->createUrl() ?>"><?= $categoryAbout->title ?></a></li>
                            <?php $categoryTastes = Yii::app()->custom->getCategories('tastes'); ?>
                            <li><a href="<?= $categoryTastes->createUrl() ?>"><?= $categoryTastes->title ?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <?php if($currentCategory->alias != 'main1'): ?>
                        <a href="/main" class="logo"> <img src="/dist/images/svg/logo.svg" alt=""> </a>
                    <?php else: ?>
                        <img src="/dist/images/svg/logo.svg" alt="">
                    <?php endif ?>
                </div>
                <div class="col">
                    <div class="right-nav">
                        <ul>
                            <?php $categoryNews = Yii::app()->custom->getCategories('news'); ?>
                            <li><a href="<?= $categoryNews->createUrl() ?>"><?= $categoryNews->title ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- end .header -->