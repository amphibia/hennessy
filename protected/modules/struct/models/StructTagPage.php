<?php

class StructTagPage extends ActiveRecord
{
	public function relations()
	{
		return array
		(
			'tagsBinds' => array(self::HAS_MANY, 'StructTag', 'tag_id'),
			'pagesBinds' => array(self::HAS_MANY, 'StructPage', 'page_id'),
		);
	}

	public function tableName(){
		return self::commonTableName('struct_tag_page');
	}
}