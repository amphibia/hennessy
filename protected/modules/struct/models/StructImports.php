<?php

class StructImports extends CFormModel
{
    public $zip_file;

    public function rules()
    {
         $rules = array(
            array('zip_file', 'required', "message" => Yii::t('custom', 'Поле обязательно для заполнения')),
            array('zip_file', 'file', 'types' => array('zip')),
        );

        return $rules;
    }

    public function attributeLabels()
    {
        return array(
            'zip_file' => Yii::t('custom', 'Импорт дерева сайта'),
        );
    }
}