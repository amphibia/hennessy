<?php

class StructBind extends ActiveRecord
{
	public function relations()
	{
		return array
		(
			'categoriesBinds' => array(self::HAS_MANY, 'StructCategory', 'category_id'),
			'pagesBinds' => array(self::HAS_MANY, 'StructPage', 'page'), /** @todo: это ошибка, и нужно page_id? Или так и нужно? **/
		);
	}

    public function tableName()
    {
        return self::commonTableName('struct_bind');
    }
}