<?php

/**
 * Модель настроек для разделов (StructCatgeory) и страниц (StructPage).
 * Определяет использование стандартных наборов полей (технические поля разделов,
 * предисловия, контент, галлереи). Также позволяет определять дополнительные
 * поля, определяя логику их поведения с помощью пресетов.
 * 
 * Описания пресетов также содержатся в этом классе (свойсва $fieldPresets
 * и $fieldPresetsAttributes).
 * 
 * Модель имеет три контекстных свойства, которые определяют для каких именно
 * целей будет применяться данная модель:
 *	"self" - настройки непесредственно этой модели;
 *	"children" - для потомков категории (вложенные настройки, используемые в
 *	категориях-потомках, не имеющих собственных настроек;
 *  "pages" - настройки материалов, относящихся к этому разделу.
 */
class StructCategoryOptions extends ActiveRecord
{
	public $contentFormElements = array();
	
	/**
	 * Варианты значения для полей "ДА/НЕТ".
	 * @var type 
	 */
	public $booleanRange = array();

	/**
	 * Варианты значения для поля "rel".
	 * Указывает назначение настроек, по умолчанию:
	 *  "self" - для этой категории;
	 *  "children" - для потомков этой категории;
	 *  "pages" - для материалов этой категории.
	 * @var array
	 */
	public $relRange = array();

	/**
	 * Варианты значения для поля "foreword" (использование предисловия).
	 * @var type 
	 */
	public $forewordRange = array();

	/**
	 * Варианты значения для поля "content" (использование содержимого).
	 * @var type 
	 */
	public $contentRange = array();
	
	/**
	 * Пресеты типов дополнительный полей (описания, аттрибуты в массиве
	 * $fieldPresetsAttributes)
	 * @var array
	 */
	public $fieldPresets = array();

	/**
	 * Аттрибуты пресетов полей.
	 * 
	 * Атрибуты:
	 * formElementAttributes - атрибуты элемента формы
	 * validators - валидаторы, обслуживающие это поле
	 * showInIndex - отображение поля в индексной таблице (работает, если создать поле в таблице)
	 * requiredValuе - говорит о том, что атрибут поля "value" обязателен
	 * valueDesciption - описание требуемого поля заполнения для этого пресета
	 * valueRule - правило проверки поля "value"
	 * 
	 * initCallback - функция, которая вызывается при первом обращении к типу поля,
	 * например, чтобы импортировать нужные классы
	 *
	 * 
	 * addValidatorCallback - функция, добавляющая валидатор к списку валидаторов
	 * моделей StructCategory и StructPage. Если это свойство указано, при
	 * обращении вместо "validators", идет обращение к нему. 
	 * 
	 * Функция принимает параметры - $validators (от сюда же), $value
	 * (значение аттрибут поля).
	 *
	 * 
	 * ПРИМЕЧАНИЕ: все свойства являются обязательными, чтобы указать "значение
	 * поу умолчанию", используется NULL
	 * 
	 * @var array
	 */
	public $fieldPresetsAttributes = array();

	/**
	 * Ошибки, относящиеся к конкретному аттрибуту динамических полей (дполнительные поля).
	 * @var array
	 */
	public $customFieldErrors = array();


	/**
	 * Получение списка кастумных полей модели.
	 * Возвращает массив c ["name" => "label"]
	 * @return array
	 */
	protected $_customAttributes = array();
	public function getCustomAttributes()
	{
		$fields = array();
		if(sizeof($this->_customAttributes)) return $this->_customAttributes;
		if(is_array($this->custom_fields)) foreach($this->custom_fields as $field){
			if(!is_array($field) || !isset($field['name']) || !isset($field['label']))
				continue;

			$fields[$field['name']] = $field['label'];
		}

		return $this->_customAttributes = $fields;
	}

	/**
	 * Интерпретация данных в "custom_fields" и прикрепление этих данных к массиву
	 * с перечислением валидаторов (для классов StructCategory и StructPage).
	 * 
	 * @param array $rules Массив с перечислением валидаторов (такой, какой
	 * выдается методом "rules()").
	 */
	public function applyRules(&$rules)
	{
		// применение валидаторов для контентной части
		if($this->foreword != 'none'){
			if((int)$this->foreword_length_min || (int)$this->foreword_length_max)
				$rules[] = array('short_text', 'length',
					'min'=>$this->foreword_length_min,
					'max' => $this->foreword_length_max);
			if($this->foreword_required) $rules[] = array('short_text', 'required');
		}
		if($this->content != 'none'){
			if((int)$this->content_length_min || (int)$this->content_length_max)
				$rules[] = array('full_text', 'length',
					'min'=>$this->content_length_min,
					'max' => $this->content_length_max);
			if($this->content_required) $rules[] = array('full_text', 'required');
		}

		// применение валидаторов галереи
		if($this->show_gallery)
		{
            $rules[] = array('gallery',
				Yii::localExtension('multiuploader', 'UploadValidator'),
				'min'=>$this->gallery_min,
				'max'=>$this->gallery_max,
				'fileType' => 1 // UploadValidator::TYPE_IMAGE
			);

            $rules[] = array('gallery', 'default', 'value'=>array());
		}

		// применение валидаторов для расширенных полей
		if(is_array($this->custom_fields))
		foreach($this->custom_fields as $customField)
		{
			$preset = isset($this->fieldPresetsAttributes[$customField['preset']])
				? $this->fieldPresetsAttributes[$customField['preset']] : NULL;

			if(!$preset) continue;

            if(isset($preset['initCallback']) && $preset['initCallback']){
                $function = create_function('$customField,$value', $preset['initCallback']);
                call_user_func_array($function,
                    array(
                        &$customField,
                        isset($customField['value'])?$customField['value']:null
                    ));
            }

            if(isset($preset['addValidatorCallback']) && $preset['addValidatorCallback']){
				$function = create_function('$validators, $value', $preset['addValidatorCallback']);
				$validators = call_user_func_array(
					$function,
					array(
						&$preset['validators'],
						isset($customField['value'])?$customField['value']:null
					)
				);
			}
			else $validators = $preset['validators'];

			// добавление валидатора для расширенного поля
			if($validators)
			foreach($validators as $validatorAttributes){
				array_unshift($validatorAttributes, $customField['name']);
				$rules[] = $validatorAttributes;
			}
		}

	}

	/**
	 * Статическая функция-хелпер для административных сервисов.
	 * Используется при создании формы создания/редактирования с учетом настроек.
	 * 
	 * Применение настроек с учетом настроек раздела (хранятся в связанной
	 * модели StructCategoryOptions, смотрите StructCategory::getOptionsModel()).
	 * 
	 * @param ExtCoreAdminService $service административный сервис, формирующий и обрабатывающий форму
	 * @param string $parentAttribute аттрибут, по которому проверяется наличие предка
	 * @throws CException возникает если в настройках указан непрвильный пресет
	 */
	public static function applyFormOptions($service, $parentAttribute = 'parent_id')
	{
		if($options = $service->model->getOptionsModel())
		{
			// применение настроек для технических полей
			if($options->show_tech_fields)
				$service->createForm['elements'] = array_merge(
					$service->createForm['elements'],
					$service->techSection
				);

			//  применение настроек контентной части
			if(($options->foreword != 'none') || ($options->content != 'none'))
			{
				// отображение поля "предисловие"
				if($options->foreword != 'none'){
					  $service->contentSection['short_text'] = $options->contentFormElements[$options->foreword];
				}else unset($service->contentSection['short_text']);

				// отображение поля "контент"
				if($options->content != 'none'){
					  $service->contentSection['full_text'] = $options->contentFormElements[$options->content];
				}else unset($service->contentSection['full_text']);

				$service->createForm['elements'] = array_merge(
					$service->createForm['elements'],
					$service->contentSection
				);
			}

			// применение расширеннных полей
			if( $options->custom_fields && is_array($options->custom_fields)
				&& sizeof($options->custom_fields) )
			{
				$service->createForm['elements'][] = $service->customFieldsHeader;

				foreach($options->custom_fields as $field){
					$name = $field['name'];
					$presetName = $field['preset'];
					if(!isset($options->fieldPresetsAttributes[$presetName]))
						throw new CException(Yii::t('struct',
							'Can`t find "{preset}" field preset!',
							array('{$presetName}' => $preset)
						));

					$preset = $options->fieldPresetsAttributes[$presetName];
					$formElementAttributes = $preset['formElementAttributes'];
					if(isset($preset['valueOption']) && ($valueOption = $preset['valueOption']))
						$formElementAttributes[$valueOption] = $field['value'];

					$service->createForm['elements'][$name] = $formElementAttributes;
				}
			}

			// применение настроек галереи
			if($options->show_gallery)
				$service->createForm['elements'] = array_merge(
					$service->createForm['elements'],
					$service->gallerySection
				);
		}
		else
		{
			// Новый раздел, без явного указания предка, к которому, возможно,
			// он будет отнесен. Поэтому отображаются только базовые поля.
			// Остальное будет доступно после сохранения.

			if(!$service->model->getIsNewRecord() || $service->model->$parentAttribute)
			// Никаких настроек нет (удалось проследить иерархию категории
			// но, ни у самого раздела, ни у предков, не указаны настрйки).
			// Поэтому используются настройки по умолчанию.
			{
				$service->createForm['elements'] = array_merge(
					$service->createForm['elements'],
					$service->techSection,
					$service->contentSection,
					$service->gallerySection
				);
			}
			
		}
	}
	
    public function init() {
        parent::init();
        Yii::loadExtConfig($this, __FILE__);
    }

    public function behaviors()
	{
		return array
		(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_created',
				'updateAttribute' => 'date_changed',
			),
		);
    }

    public function tableName()
    {
        return self::commonTableName('struct_category_options');
    }


	public function relations(){
		return array(
			'category' => array(self::BELONGS_TO, 'StructCategory', 'category_id'),
		);
	}

	public function rules()
    {
		$rules = array
        (
			// числовые поля
            array(
				'category_id, '.
				'foreword_length_min, foreword_length_max, '.
				'content_length_min, content_length_max, ' .
				'gallery_min, gallery_max, date_changed',
				'numerical', 'integerOnly' => true,
				'message' => Yii::t('struct', 'Value must be numerical')
			),

			// поля - переключатели
			array('enabled, show_tech_fields, foreword_required, content_required, show_gallery', 'boolean'),

			// поля с наборами возможных значений
			array('rel', 'in', 'range'=>array_keys($this->relRange)),
			array('foreword', 'in', 'range' => array_keys($this->forewordRange)),
			array('content', 'in', 'range' => array_keys($this->contentRange)),


			// json - свойство с описанием дополнительных (виртуальных полей).
			array('custom_fields', 'checkAdditionalFields'),
		);
		
		return $rules;
	}
	
	public function beforeValidate()
    {
		if(!is_array($this->custom_fields) && $this->custom_fields)
			$this->custom_fields = unserialize($this->custom_fields);

		// нужен массив - прямой или сериализированный
		if($this->custom_fields && !is_array($this->custom_fields)){
			$this->addError('custom_fields', Yii::t('struct', 'Invalid data!'));
			return false;
		}

		// группировка данных
        if(isset($_POST[get_class($this)]['custom_fields']))
        {
            $sorted = array();
            if($this->custom_fields)
            foreach($this->custom_fields as $key => $row)
            {
                if(!is_array($row)){
                    $this->addError('custom_fields', Yii::t('struct', 'Invalid data!'));
                    return false;
                }

                foreach($row as $index => $value){
                    if(!isset($sorted[$index])) $sorted[$index] = array();
                    $sorted[$index][$key] = $value;
                }
            }

            $this->custom_fields = $sorted;
        }

		return parent::beforeValidate();
	}

	/**
	 * Метод - валидатор для свойства "additional_fields", которое хранит в себе
	 * описание для динамических полей.
	 *
	 * Формат:
	 * $this->additional_fields[0-255][name, label, widgetClass, validatorClass, attributes[]]
	 * 
	 * @param type $attr
	 */
	public function checkAdditionalFields($attr)
	{
        // чтобы можно было удалить все кастумные поля @todo ПРОВЕРИТЬ!)
		if( !isset($_POST[get_class($this)][$attr]) &&
            !isset($_POST['StructCategory']) ) // при сохранении категории, могут меняться и настройки - принудительно
            $this->$attr = NULL;

		if(!$this->$attr) return;

		$usedFieldsNames = array();
		foreach($this->$attr as $index => $field)
		{
			if(!is_array($field)){
				$this->addError($attr, Yii::t('struct', 'Bad field record.'));
				break; }
			if(!is_numeric($index)){
				$this->addError($attr, Yii::t('struct', 'Bad field index.'));
				break;
            }

			// обязательные свойства виртуального аттрибута
			foreach(array('name', 'label', 'preset') as $fieldAttribute)
				$this->_requireFieldOption($field, $fieldAttribute, $index);
			
			// имя
			if(isset($field['name'])){
				// техническое слово
				if(!preg_match('/^[a-zA-Z_][a-zA-Z0-9_]{1,32}$/', $field['name']))
					$this->_addCustomError($index, 'name', Yii::t('struct', 'Field name must be a valid technical name.'));

				// проверка уникальности имени поля
				if(in_array($field['name'], $usedFieldsNames))
				$this->_addCustomError($index, 'name', Yii::t('struct', 'Field name must be unique.'));
				$usedFieldsNames[] = $field['name'];
			}

			// проверка пресета
			if(isset($field['preset'])){
				if(!isset($this->fieldPresetsAttributes[$field['preset']]))
					$this->_addCustomError($index, 'preset', Yii::t('struct', 'Unknown preset.'));
				else
				{
					// в некоторых пресета значение "value" обязательно
					$preset = $this->fieldPresetsAttributes[$field['preset']];
					if($preset['requiredValuе'] && (!isset($field['value']) || !$field['value']))
						$this->_addCustomError($index, 'value', $preset['valueDesciption']);

					// проверка указанного значения
					else if(
						$preset['valueRule'] && isset($field['value']) && $field['value'] &&
						!preg_match($preset['valueRule'], $field['value'])
					)
						$this->_addCustomError($index, 'value', $preset['valueDesciption']);
				}
			}
		}

		if(sizeof($this->customFieldErrors))
			$this->addError($attr, Yii::t('struct', 'Errors with fields parse.'));
	}

	protected function _requireFieldOption($field, $fieldAttribute, $index)
	{
		if(!isset($field[$fieldAttribute]) || !$field[$fieldAttribute])
			$this->_addCustomError($index, $fieldAttribute, Yii::t('struct', 'Required property.'));
			
	}

	protected function _addCustomError($index, $fieldAttribute, $message)
	{
		if(!isset($this->customFieldErrors[$index])) $this->customFieldErrors[$index] = array();
			$this->customFieldErrors[$index][$fieldAttribute] = $message;
	}

	// десериализация сложных данных
	public function save($runValidation = true, $attributes = null) {
		$result = parent::save($runValidation, $attributes);
		if($this->custom_fields && !is_array($this->custom_fields))
			$this->custom_fields = unserialize($this->custom_fields);
		return $result;
	}

	protected function afterFind(){
		
		foreach($this->getSafeAttributeNames() as $attr){
			if($this->$attr === '0') $this->$attr = NULL;
		}
		
		if($this->custom_fields && !is_array($this->custom_fields))
			$this->custom_fields = unserialize($this->custom_fields);
		return parent::afterFind();
	}
	
	// сериализация сложных данных
	protected function beforeSave() {
		$result = parent::beforeSave();
		if(is_array($this->custom_fields))
			$this->custom_fields = serialize($this->custom_fields);
		return $result;
	}

}