<?php

/**
 * @author koshevy
 * @package struct
 * Административный сервис управления категориями сайта (настройка над программной
 * структурой).
 *
 */
class StructCategoriesAdminService extends ExtCoreAdminService
{
    /**
     * Уровень, который и ниже которого могут редактировать только
     * супер-пользователи.
     * @var integer
     */
    public $lockedLevel = NULL;

    /**
     * Форма конфигурации раздела.
     * @var array
     */
    public $optionsForm = array();

    public $techSection = array();
    public $contentSection = array();
    public $gallerySection = array();

    /**
     * Заголовок, который идет перед расширенными полями.
     * @var string
     */
    public $customFieldsHeader = NULL;


    public $assets = array();

    public $views = array(
        'create' => 'category/create',
        'update' => 'category/update',
        'index' => 'category/index',
        'options' => 'category/options'
    );

    public $dictionary = array(
        'actions' => array(
            'index' => 'Site structure management',
            'create' => 'Add category',
        )
    );

    public function filters()
    {
        return array('ajaxOnly + setAttributes, simpleAttr, manageBar, moveItem, groupOperationsBar');
    }

    protected function beforeAction($action)
    {
        $this->viewPath = Yii::getPathOfAlias('application.modules.struct.views');
        Yii::import('application.modules.struct.models.*');
        Yii::import('application.modules.struct.*');
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

        // обратная связь клиента с сервером
        Yii::app()->clientScript->registerScript('',
            "$(document).data('manageBarActionUrl','{$this->createUrl('managebar', false)}');" .
            "$(document).data('setAttributesActionUrl','{$this->createUrl('setattributes', false)}');" .
            "$(document).data('moveItemUrl','{$this->createUrl('moveitem', false)}');" .
            "$(document).data('updateUrl','{$this->createUrl('update', false)}');",
            CClientScript::POS_HEAD
        );

        return parent::beforeAction($action);
    }

    public function actionIndex($orderBy = NULL, $scope = NULL, $search = NULL, $page = NULL, $parent = NULL)
    {
        $this->render($this->views['index']);
    }

    public function actionCreate($parent_id = NULL)
    {

        $modelClass = $this->modelClass;
        if ($parent_id) { // должна отсутствовать возможность смены предопределенного родителя
            if (!$parent = $modelClass::model()->findByPk($parent_id))
                throw new CException(Yii::t('struct', 'Cant find parent category.'));
            $this->model->parent_id = $parent_id;
            $this->model->parent = $parent_id;
            $this->createForm['elements']['parent']['disabled'] = 'disabled';
        } else $parent_id = NULL;

        // настройки формы
        StructCategoryOptions::applyFormOptions($this);

        $form = new CForm($this->createForm, $this->model);
        if (isset($_POST[$this->modelClass])) {
            $form->loadData();
            if ($form->model->parent) {
                $this->_formSaved($form->model->appendTo(
                    $modelClass::model()->findByPk($form->model->parent)), $form, true, true);
            } else {
                $root = $modelClass::model()->alias('root')->find();
                if (!$root) {
                    $root = new $modelClass;
                    $root->setAttributes(array('alias' => 'root'));
                    $root->save(false);
                }
                $this->_formSaved($form->model->appendTo($root), $form, true, true);
            }
        }

        $this->render($this->views['create'], compact('form', 'parent'));
    }

    public function actionUpdate($id)
    {
        $this->loadModel($id);

        // настройки формы
        StructCategoryOptions::applyFormOptions($this);

        $form = new CForm($this->createForm, $this->model);

        if (isset($_POST[$this->modelClass])) {
            // настройки формы, взятые из StructCategoryOptions
            $form->loadData();
            $this->_formSaved($form->model->save(), $form);
        }

        $this->render($this->views['update'], compact('form'));
    }

    /**
     * Текст сообщения с подтверждением удаления
     * @param array $elements
     */
    protected function _deleteConfirmation(Array $elements)
    {
        $ids = array();
        $titles = array();
        foreach ($elements as $element) $titles[] = "'{$element->title}'";
        $titles = implode(', ', $titles);

        // вместо ID подставляется TITLE
        return Yii::t($this->messageContext,
            $this->deleteConfirmationMessage,
            array(sizeof($ids), '{elements}' => $titles)
        );
    }

    protected function _successMessage()
    {
        if ($this->lastActionID == 'create')
            return Yii::t('struct',
                'Site category "{title}" created. For editing "{title}" category, go to {edit form}.<br/><br/>' .
                '<strong>Notice!</strong> This form will create new category again. If you want to update previous form, go to {edit form}.',
                array(
                    '{title}' => $this->_model->title,
                    '{edit form}' => CHtml::link(
                            Yii::t('struct', 'edit form'),
                            $this->createUrl('update', array($this->_model->id))
                        )
                )
            );

        else return Yii::t('struct', 'struct category "{title}" saved.',
            array('{title}' => $this->_model->title));
    }

    /**
     * Перерисовка панели управления элементами.
     * @param integer $selectedItems перечисление ID выбранных элементов/
     */
    public function actionManageBar(Array $selectedItems = array())
    {
        $this->renderPartial('category/_manageBar', compact('selectedItems'));
    }

    /**
     * Перемещение существующего элемента. Ставит элемент либо перед, либо после
     * определённого элемента, либо добавляет внутрь другого элемента.
     * @param integer $id перемещаемый элемент
     * @param integer $beforeID
     * @param integer $afterID
     * @param integer $appendTo
     */
    public function actionMoveItem($id, $beforeID = NULL, $afterID = NULL, $appendTo = NULL)
    {
        $this->loadModel($id);
        $className = $this->modelClass;
        $error = false;

        try {
            if ($beforeID && $beforeID = intval($beforeID)) {
                $target = $className::model()->findByPk($beforeID);
                $this->_checkElementLevelAccess($this->model->{$target->levelAttribute},
                    $target->{$target->levelAttribute});

                $this->model->parent_id = $target->parent_id;
                if ($this->model->validate()) {
                    $this->model->moveBefore($target);
                    ExtCoreJson::message(Yii::t('struct',
                            'Category "{item}" moved before "{target}".',
                            array('{item}' => $this->model->title, '{target}' => $target->title))
                    );
                } else $error = true;
            } else if ($afterID && $afterID = intval($afterID)) {

                $target = $className::model()->findByPk($afterID);
                $this->_checkElementLevelAccess($this->model->{$target->levelAttribute},
                    $target->{$target->levelAttribute});

                $this->model->parent_id = $target->parent_id;
                if ($this->model->validate()) {
                    $this->model->moveAfter($target);
                    ExtCoreJson::message(Yii::t('struct',
                            'Category "{item}" moved after "{target}".',
                            array('{item}' => $this->model->title, '{target}' => $target->title))
                    );
                } else $error = true;
            } else if ($appendTo && $appendTo = intval($appendTo)) {

                $target = $className::model()->findByPk($appendTo);
                $this->_checkElementLevelAccess($this->model->{$target->levelAttribute},
                    $target->{$target->levelAttribute} + 1);

                $this->model->parent_id = $target->id;
                if ($this->model->validate()) {
                    $this->model->moveAsFirst($target);
                    ExtCoreJson::message(Yii::t('struct',
                            'Category "{item}" moved to "{target}".',
                            array('{item}' => $this->model->title, '{target}' => $target->title))
                    );
                } else $error = true;
            } else ExtCoreJson::error(Yii::t('struct',
                'Can`t move element to undefined position.'));

            if ($error || !$this->model->save()) {
                foreach ($this->model->errors as $field => $errors) {
                    ExtCoreJson::error(
                        '<strong>' . Yii::t('struct', $field) . '</strong>: ' .
                        implode("\n<br/>", $errors)
                    );
                }
            }
        } catch (Exception $err) {
            ExtCoreJson::error(Yii::t('struct', 'Error! Cant move element. Please, reload page and try again.'));
            ExtCoreJson::error(Yii::t('struct', $err->getMessage()));
        }

        ExtCoreJson::response();
    }

    /**
     * Проверка, может ли элемент быть перемещен из или на уровень.
     * Предполагается, что перемещать элемент или
     * @param int $level
     */
    protected function _checkElementLevelAccess($srcLevel, $destLevel)
    {
        // изменять положение категорий первого уровня
        // может только суперпользователь
        if (Yii::app()->hasComponent('users') && Yii::app()->hasComponent('user')) {
            if (isset(Yii::app()->user->model) && ($userModel = Yii::app()->user->model)) {
                if (!$userModel->role->super_user
                    && (($srcLevel <= $this->lockedLevel) || ($destLevel <= $this->lockedLevel))
                ) {
                    ExtCoreJson::error(Yii::t('struct', "Only super users can move to/from base levels!"));
                    ExtCoreJson::response();
                }
            }
        }
    }

    /**
     *
     * @param integer $categoryId
     */
    public function actionOptions($category, $rel = 'self')
    {
        $this->loadModel($category);

        // при первом обращении, создается модель настроек (но сохранится она только
        // после отправки формы)
        $optionsModel = StructCategoryOptions::model()->findByAttributes(array(
            'rel' => $rel, 'category_id' => $this->model->id));

        if (!$optionsModel) {
            $optionsModel = new StructCategoryOptions;
            $optionsModel->setAttributes(array('rel' => $rel, 'category_id' => $this->model->id));
        }

        $form = new CForm($this->optionsForm, $optionsModel);
        if (isset($_POST[get_class($optionsModel)])) {
            $form->loadData();
            if (Yii::app()->request->isAjaxRequest) {
                echo CActiveForm::validate($form->model);
                Yii::app()->end();
            }
            $this->_formSaved($optionsModel->save(), $form, false);
        }

        $this->render($this->views['options'], compact('form', 'category'));
    }

    /**
     * Экспортирует дерево каталога со всеми настройками и матералами
     * Возвращает zip файл
     */
    public function actionExports($categoryId){

        $category = StructCategory::model()->findByPk((int)$categoryId);
        if($category) {
            $dataCategory = $this->_getDataCategory($category);
            //die("<pre>" . print_r($dataCategory, true) . "</pre>");

            Yii::import('ext.zip.ZipFile');
            $name = "exports";
            $zipfile = new ZipFile();
            $zipfile->addFile(json_encode($dataCategory), $name);
            $zipdata = $zipfile->file();

            // http headers for zip downloads
            $filename = "Exports_".$category->alias.".zip";
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: ".strlen($zipdata));
            ob_end_flush();

            echo $zipdata;
        }
    }

    private function _getDataCategory($category){
        $options = StructCategoryOptions::model()->findAllByAttributes(array('category_id' => $category->id));

        $dataCategory = array();
        $dataCategory['attributes'] = $category->attributes;

        $dataCategory['attributes_options'] = array();
        if($options) {
            foreach ($options as $option) {
                $dataCategory['attributes_options'][] = $option->attributes;
            }
        }

        $dataCategory['pages'] = array();
        foreach($category->pages as $page){
            $dataCategory['pages'][] = $page->attributes;
        }

        $dataCategory['children'] = array();
        foreach($category->childrenTech as $children){
            $dataCategory['children'][] = $this->_getDataCategory($children);
        }
        return (object)$dataCategory;
    }


    /**
     * Импортирует дерево каталога из ранее экспортированного zip файла
     */
    public function actionImports($categoryId){

        $zip_file = $_FILES['zip_file']['tmp_name'];
        $buf = null;

        if ($zip_file)
        {
            $zip = zip_open($zip_file);
            //$zip = zip_open("/home/vitaszanoza/Загрузки/Exports_o_kompanii.zip");
            if (is_resource($zip)) {
                while ($zip_entry = zip_read($zip)) {
                    if(zip_entry_name($zip_entry) == 'exports') {
                        if (zip_entry_open($zip, $zip_entry, "r")) {
                            $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                            zip_entry_close($zip_entry);
                        }
                    }
                    else{
                        die(Yii::t('error', 'Не верный файл'));
                    }
                    break;
                }
                zip_close($zip);

                if (
                    $buf
                    && ($data = (array)json_decode($buf, true))
                    && isSet($data['attributes'])
                ) {
                    //die("<pre>" . print_r($data, true) . "</pre>");
                    $this->_appendCategory($data, (int)$categoryId);
                    die('ok');
                }
                else{
                    die(Yii::t('error', 'Не верный файл'));
                }
            }
            else{
                die(Yii::t('error', 'Не верный файл'));
            }

        }

    }

    protected  function _appendCategory(array $dataCategory, $categoryIdTo){

        $categoryTo = StructCategory::model()->findByPk((int)$categoryIdTo);
        if($categoryTo && isSet($dataCategory['attributes'])){
            //Создаем категорию с базовыми полями
            $categoryNew = new StructCategory;
            $categoryNew->title = $dataCategory['attributes']['title'];
            $categoryNew->alias = $dataCategory['attributes']['alias'];
            $categoryNew->published = $dataCategory['attributes']['published'];
            $categoryNew->parent_id = $categoryTo->id;
            $categoryNew->appendTo($categoryTo);

            //Если уже существует категория с текущим алисом
            if(!$categoryNew->id) {
                $categoryNew->alias = $dataCategory['attributes']['alias'].'_'.strtotime("now");
                $categoryNew->appendTo($categoryTo);
            }

            if($categoryNew->id) {

                if(isSet($dataCategory['attributes_options'])) {
                    //Создаем настройки для данной категории
                    foreach ($dataCategory['attributes_options'] as $dataOtions) {
                        $optionNew = $this->_cloneCategoryOptions($dataOtions);
                        $optionNew->setAttributes(array('category_id' => $categoryNew->id));
                        $optionNew->save(false);
                    }
                }

                //Прополняем всеми полями
                $categoryNew->resetOptionsModel();
                $categoryNew = $this->_cloneCategory($dataCategory['attributes'], $categoryNew);
                $categoryNew->save();

                if(isSet($dataCategory['pages'])) {
                    foreach ($dataCategory['pages'] as $dataPage) {
                        $page = $this->_clonePage($dataPage, array($categoryNew->id));
                        $page->save();
                    }
                }

                if(isSet($dataCategory['children'])) {
                    foreach ($dataCategory['children'] as $dataChildren) {
                        $this->_appendCategory($dataChildren, $categoryNew->id);
                    }
                }
            }
        }
    }

    private function _cloneCategoryOptions($dataOtions){
        $exceptAttributes = [
            'id',
        ];
        $categoryOptions = new StructCategoryOptions;
        foreach($dataOtions as $key => $value)
        {
            if(in_array($key, $exceptAttributes)){
                continue;
            }
            $categoryOptions->$key = $value;
        }
        return $categoryOptions;
    }

    private function _cloneCategory($dataCategory, $category){
        $exceptAttributes = [
            'id',
            '_left',
            '_right',
            '_root',
            '_level',
            'parent_id',
            'tech_id',
        ];
        foreach($dataCategory as $key => $value)
        {
            if(in_array($key, $exceptAttributes)){
                continue;
            }

            $category->$key = $value;
        }
        return $category;
    }

    private function _clonePage($dataPage, $categoryIDs){
        $exceptAttributes = [
            'id',
            'first_category_index',
        ];
        $page = new StructPage;
        $page->categories = $categoryIDs;
        foreach ($dataPage as $key => $value) {
            if (in_array($key, $exceptAttributes)) {
                continue;
            }
            $page->$key = $value;
        }

        return $page;
    }
}