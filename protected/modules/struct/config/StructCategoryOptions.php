<?php

return array
(
    'contentFormElements' => array(
        'text' => array('type' => 'textarea', 'class'=>'contentText span8'),
        //'html' => array('type' => Yii::localExtension('tinymce', 'TinyMCE')),
        'html' => array('type' => Yii::localExtension('redactorjs', 'Redactor')),
    ),

    /**
     * Варианты значения для полей "ДА/НЕТ".
     * @var type
     */
    'booleanRange' => array(
        Yii::t('struct', 'No'), Yii::t('struct', 'Yes')
    ),

    /**
     * Варианты значения для поля "rel".
     * Указывает назначение настроек, по умолчанию:
     *  "self" - для этой категории;
     *  "children" - для потомков этой категории;
     *  "pages" - для материалов этой категории.
     * @var array
     */
    'relRange' => array(
        'self' => Yii::t('struct', 'This category'),
        'children' => Yii::t('struct', 'children categories'),
        'pages' => Yii::t('struct', 'Pages'),
    ),

    /**
     * Варианты значения для поля "foreword" (использование предисловия).
     * @var type
     */
    'forewordRange' => array(
        'none' => Yii::t('struct', 'Not using foreword'),
        'html' => Yii::t('struct', 'HTML'),
        'text' => Yii::t('struct', 'Plain text'),
    ),

    /**
     * Варианты значения для поля "content" (использование содержимого).
     * @var type
     */
    'contentRange' => array(
        'none' => Yii::t('struct', 'Not using content field'),
        'html' => Yii::t('struct', 'HTML'),
        'text' => Yii::t('struct', 'Plain text'),
    ),

    /**
     * Пресеты типов дополнительный полей (описания, аттрибуты в массиве
     * $fieldPresetsAttributes)
     * @var array
     */
    'fieldPresets' => array(
        'text' => Yii::t('struct', 'Text field'),
        'required_text' => Yii::t('struct', 'Required text field'),
        'required_numerical_text' => Yii::t('struct', 'Required numerical text field'),
        'indexed_text' => Yii::t('struct', 'Indexed text field'),
        'textarea_512' => Yii::t('struct', 'Medium textarea (512 symbols max)'),
        'full_textarea' => Yii::t('struct', 'Full textarea'),
        'tiny_mce' => Yii::t('struct', 'TinyMCE visual editor'),
        //'tiny_mce2' => 'TinyMCE 4.0.6 beta',
        'toggler' => Yii::t('struct', 'Toggle button (on/of)'),
        'dropdownlist' => Yii::t('struct', 'Dropdownlist'),
        'checkboxlist' => Yii::t('struct', 'Checkboxlist'),
        //'radiobuttonlist' => Yii::t('struct', 'Radiobuttonlist'),
        'gallery' => Yii::t('struct', 'Gallery'),
        'single_image' => Yii::t('struct', 'Single image'),
        'documents' => Yii::t('struct', 'Attached documents'),
        'document' => Yii::t('struct', 'Single attached document'),
        'datepicker' => Yii::t('struct', 'Datepicker'),
        'datepickerOptional' => Yii::t('struct', 'Datepicker optional'),
        'daterange' => Yii::t('struct', 'Daterange'),
        'email' => Yii::t('struct', 'Email'),
        'ip' => Yii::t('struct', 'Incoming IP'),
        'regexp' => Yii::t('struct', 'Text with regular expression'),
        'color' => Yii::t('struct', 'Color picker'),
        'vote' => Yii::t('struct', 'Vote'),
        'form_editor' => Yii::t('struct', 'Редактор для лэндинг-форм'),
    ),

    /**
     * Аттрибуты пресетов полей.
     *
     * Атрибуты:
     * formElementAttributes - атрибуты элемента формы
     * validators - валидаторы, обслуживающие это поле
     * showInIndex - отображение поля в индексной таблице (работает, если создать поле в таблице)
     * requiredValuе - говорит о том, что атрибут поля "value" обязателен
     * valueDesciption - описание требуемого поля заполнения для этого пресета
     * valueRule - правило проверки поля "value"
     * valueOption - свойство валидатора, которое будет указыватья как "value"
     *
     * initCallback - функция, которая вызывается при первом обращении к типу поля,
     * например, чтобы импортировать нужные классы
     *
     *
     * addValidatorCallback - функция, добавляющая валидатор к списку валидаторов
     * моделей StructCategory и StructPage. Если это свойство указано, при
     * обращении вместо "validators", идет обращение к нему.
     * Функция принимает параметры - $validators (от сюда же), $value
     * (значение аттрибут поля).
     *
     *
     * ПРИМЕЧАНИЕ: все свойства являются обязательными, чтобы указать "значение
     * поу умолчанию", используется NULL
     *
     * @var array
     */
    'fieldPresetsAttributes' => array
    (
        /**
         * @TODO настроить пресеты
         */
        'text' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'text', 'class'=>'span6'),
            'validators' => array(
                array('safe'),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'required_text' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'text'),
            'validators' => array(
                array('required'),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'required_numerical_text' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'text'),
            'validators' => array(
                array('numerical', 'integerOnly' => true),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'indexed_text' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => true,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'text'),
            'validators' => array(
                array('required'),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'textarea_512' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'textarea', 'class'=>'span8 textarea512'),
            'validators' => array(
                array('length', 'max' => 512),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'full_textarea' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'textarea', 'class'=>'span8', 'rows' => 8),
            'validators' => array(
                array('safe'),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'tiny_mce' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            //'formElementAttributes' => array('type' => Yii::localExtension('tinymce', 'TinyMCE')),
            'formElementAttributes' => array('type' => Yii::localExtension('redactorjs', 'Redactor')),
            'validators' => array(
                array('safe'),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
        ),

        'toggler' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array(
                'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
                'attributes' => array(
                    'enabledLabel' => Yii::t('struct', 'On'),
                    'disabledLabel' => Yii::t('struct', 'Off'),
                    'enabledStyle' => 'success',
                )
            ),
            'validators' => array(
                array('boolean'),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
        ),

        /*'tiny_mce2' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => Yii::localExtension('tinymce2', 'ETinyMce')),
            'validators' => array(
                array('safe'),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
        ),*/

        'dropdownlist' => array(
            'initCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'default'),
            'validators' => array(array('in')),
            'addValidatorCallback' => '$validators[0]["range"] = explode(",", $value); return $validators;',
            'requiredValuе' => true,
            'valueDesciption' => Yii::t('struct', 'Enter values, separated by comma'),
            'valueRule' => NULL,
        ),

        'checkboxlist' => array(
            'initCallback' => null, //' CVarDumper::dump($value,3,1);',
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'checkboxlist'),
            'validators' => array(
                array(Yii::localExtension('bootforms', 'CheckBoxListValidator'))
            ),
            'addValidatorCallback' => '$validators[0]["items"] = explode(",", $value); return $validators;',
            'requiredValuе' => true,
            'valueDesciption' => Yii::t('struct', 'Enter values, separated by comma'),
            'valueRule' => NULL,
        ),

        'gallery' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => Yii::localExtension('multiuploader', 'UploadFormElement')),
            'validators' => array(
                array(Yii::localExtension('multiuploader', 'UploadValidator'), 'max' => 32, 'fileType' => 1),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'single_image' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => Yii::localExtension('multiuploader', 'UploadFormElement')),
            'validators' => array(
                array(Yii::localExtension('multiuploader', 'UploadValidator'), 'max' => 1, 'fileType' => 1),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'document' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => Yii::localExtension('multiuploader', 'UploadFormElement')),
            'validators' => array(
                array(Yii::localExtension('multiuploader', 'UploadValidator'), 'max' => 1, 'fileType' => 2),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
            'indexFieldPreconvertor' => NULL
        ),

        'documents' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => Yii::localExtension('multiuploader', 'UploadFormElement')),
            'validators' => array(
                array(Yii::localExtension('multiuploader', 'UploadValidator'), 'max' => 16, 'fileType' => 2),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
            'indexFieldPreconvertor' => NULL
        ),

        'email' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'text'),
            'validators' => array(
                array('required'),
                array('email')
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'ip' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'text'),
            'validators' => NULL,
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'datepicker' => array(
            'initCallback' => NULL,
            'showInIndex' => true,
            'indexFieldPreconvertor' => 'if($isHeader) return $fieldValue; return Yii::app()->dateFormatter->formatDateTime($fieldValue, "short", NULL);',
            'formElementAttributes' => array(
                'type' => Yii::localExtension('datetimeworks', 'ArDatePicker'),
                'attributes' => array(
                    'htmlOptions' => array(
                        'class' => 'span2',
                        'prepend'=>'<i class="icon-calendar"></i>'
                    ),
                )
            ),
            'validators' => array(
                array(Yii::localExtension('datetimeworks', 'DateTimeValidator')),
                array('default', 'value'=>time()),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'datepickerOptional' => array(
            'initCallback' => NULL,
            'showInIndex' => true,
            'indexFieldPreconvertor' => 'if($isHeader) return $fieldValue; return Yii::app()->dateFormatter->formatDateTime($fieldValue, "short", NULL);',
            'formElementAttributes' => array(
                'type' => Yii::localExtension('datetimeworks', 'ArDatePicker'),
                'attributes' => array(
                    'allowEmptyDate' => true,
                    'htmlOptions' => array(
                        'class' => 'span2',
                        'prepend'=>'<i class="icon-calendar"></i>'
                    ),
                )
            ),
            'validators' => array(
                array(Yii::localExtension('datetimeworks', 'DateTimeValidator'), 'allowEmptyDate' => true),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'daterange' => array(
            'initCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'text'),
            'validators' => array(
                array(Yii::localExtension('datetimeworks', 'DateTimeValidator')),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'range' => array(
            'initCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'text'),
            'validators' => NULL,
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'regexp' => array(
            'initCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => 'text'),
            'validators' => NULL,
            'requiredValuе' => true,
            'valueDesciption' => Yii::t('struct', 'Enter validation rule (regular expression) for this field.'),
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'color' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'formElementAttributes' => array(
                'type' => 'TbColorPicker',
            ),
            'validators' => array(
                array('safe'),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
        ),

        'vote' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => Yii::localExtension('vote', 'VoteFormElement')),
            'validators' => array(
                array(Yii::localExtension('vote', 'VoteValidator')),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
            'indexFieldPreconvertor' => NULL
        ),

        'form_editor' => array(
            'initCallback' => NULL,
            'addValidatorCallback' => NULL,
            'showInIndex' => false,
            'indexFieldPreconvertor' => NULL,
            'formElementAttributes' => array('type' => Yii::localExtension('structlandings', 'FormEditor')),
            'validators' => array(
                array(Yii::localExtension('structlandings', 'FormDataValidator')),
            ),
            'requiredValuе' => false,
            'valueDesciption' => NULL,
            'valueRule' => NULL,
            'valueOption' => NULL,
            'indexFieldPreconvertor' => NULL
        ),

    ),
);

?>