<?

if(!isset($index)) $index = NULL;

if(!isset($value) || !$value || !is_array($value) || !sizeof($value)){
	$billet = true; $value = array();
}
if(!isset($billet)) $billet = false;
$value = array_merge(array('name'=>NULL, 'label'=>'', 'preset'=>'', 'value'=>''), $value);

$attributeName = get_class($this->model)."[{$this->attribute}]";
$options = array(
	'class' => 'input-block-level',
	'placeholder' => NULL,
);

if($billet) $options['disabled'] = 'disabled';

?>
<div class="fieldDescription <?if($billet){?> billet<? } ?>">
	<div class="sortHandle alert alert-success">
		
	<label class="line">
		<?
		echo Yii::t('struct', 'Field name');
		echo CHtml::textField("{$attributeName}[name][]", $value['name'], $options);
		$this->_errors($index, 'name');
		?>
	</label>

	<label class="line">
		<label></label>
		<?
		echo Yii::t('struct', 'Field label');
		echo CHtml::textField("{$attributeName}[label][]", $value['label'], $options);
		$this->_errors($index, 'label');
		?>
	</label>

	<label class="line">
		<?
		echo Yii::t('struct', 'Preset');
		echo CHtml::dropDownList("{$attributeName}[preset][]", $value['preset'], $this->model->fieldPresets, $options);
		$this->_errors($index, 'preset');
		?>
	</label>
		
	<label class="line">
		<?
		echo Yii::t('struct', 'Value(s)');
		$options['placeholder'] = Yii::t('struct', 'Optional arguments');
		echo CHtml::textArea("{$attributeName}[value][]", $value['value'], $options);
		$this->_errors($index, 'value');
		?>
	</label>


	<div id="removeField" class="btn btn-small"><i class="icon icon-ban-circle"> </i><?= Yii::t('struct', 'Remove field'); ?></div>
	</div>
</div>