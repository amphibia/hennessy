<style>
	#optionsPage .fieldDescription{ display:block; margin-bottom:1px; }
	#optionsPage .sortHandle{ cursor:move; }
	#optionsPage .billet{ display:none; }
	#optionsPage .fieldDescription .alert{ display:block; max-width:400px; padding:12px; }
	#optionsPage .fieldDescription textarea{ resize:none; }
	#optionsPage .fieldsPlaceholder{ display:block; max-width:400px; padding:12px; }
	#optionsPage .buttons{ margin-bottom:8px; }
	#optionsPage .additionalSection{ display:none; }
	#optionsPage .unexistsSection{ max-width:350px; }
</style>
<script>

// прячет/показывает дополнительные поля
function toggleAdditionalFields(rel)
{
	var $element = $('#optionsPage [name][data-name="'+rel+'"]'),
		value = ($element.attr('type')=='checkbox')
			? $element.attr('checked') : $element.val();

	if(!value || (value == 'none')){
		$('#optionsPage .additionalSection[rel="'+rel+'"]').hide()
			.find('input, select, textarea').attr('disabled');
	}
	else
	{
		$('#optionsPage .additionalSection[rel="'+rel+'"]').show()
			.find('input, select, textarea').attr('disabled');
	}
}

$(document).ready(function(){
	$('#optionsPage .items').sortable({
		placeholder: 'fieldsPlaceholder well',
		handle: '.sortHandle'
	});

	// поля с переключаемой видимостью
	$('#optionsPage [name][data-name]').each(function(){
		toggleAdditionalFields($(this).attr('data-name'));
		$(this).change(function(){
			toggleAdditionalFields($(this).attr('data-name')); });
	});
	
});

// добавление нового поля
$('#optionsPage #addField').live('click', function(){
	var $billet =  $('#optionsPage .fieldDescription.billet').clone();
	$billet.removeClass('billet').find('input, textarea, select').removeAttr('disabled');
	$('#optionsPage .items').append($billet);
});

// удаление поля из списка
$('#optionsPage #removeField').live('click', function(){
	var $field = $(this).closest('.fieldDescription').fadeOut('2000', function(){
		$field.remove();
	});
});

</script>
<?php

// загоовка для добавления новых полей
$this->render('customFields/fieldDescriptions', array('billet'=>true));

?>


<div class="items">
<?

// вывод значений
if(is_array($this->value))
foreach($this->value as $index => $item){
	$this->render('customFields/fieldDescriptions', array('value'=>$item, 'index'=>$index));
}

?>
</div>

<div class="buttons">
	<div id="addField" class="btn"><i class="icon icon-plus"> </i><?= Yii::t('struct', 'Add field'); ?></div>
</div>