<?

$this->widget(Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__));

// вывод автоформы
$this->widget(Yii::localExtension('bootforms', 'BootAutoForm', __FILE__),
	array(
		'form' => $form,
		'autoElements' => false,
	)
);

?>
