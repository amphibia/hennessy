<?php

if(!$ids || !sizeof($ids)){

	echo CHtml::tag('span', array(), Yii::t('admin', 'No items selected.'));
	echo " &nbsp; ";
	echo CHtml::link(Yii::t('admin', 'Select all'), 'javascript:;', array('class'=>'selectAll'));

	return;
}

$count = sizeof($ids);

echo CHtml::tag('span', array(),
	Yii::t('admin', 'Selected {count} item.|Selected {count} items.',
		array($count, '{count}'=>$count)
	)
);

echo " &nbsp; ";

echo CHtml::link(Yii::t('admin', 'Select all'), 'javascript:;', array('class'=>'selectAll'));
echo " / ";
echo CHtml::link(Yii::t('admin', 'Select none'), 'javascript:;', array('class'=>'selectNone'));

echo " &nbsp; ";

$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
	array(
		'label'=>Yii::t('admin', 'Delete item|Delete items', array($count)),
		'type' => 'danger',
		'icon' => 'icon-trash icon-white',
		'url' => $this->createUrl('delete', array('id' => $ids)),

		'htmlOptions' => array('class' => 'deleteButton'),
	)
);

echo " &nbsp; ";

$pageModel = StructPage::model();
$pageModel->init();
$publishedRange = $pageModel->publishedRange;
array_unshift($publishedRange, Yii::t('struct', 'Set published status'));

echo CHtml::dropDownList('setPublishStatus', 0, $publishedRange, array('class'=>'setPublishStatus'))

?>
