<?

/**
 * @author koshevy <koshevy@gmail.com>
 * Вывод вложенного Nested - меню категорий (без учета привязанных материалов).
 */
$_level = $categories->levelAttribute;

echo CHtml::openTag('ul', $this->htmlAttributes);

if ($this->showRoot) {
    // Вывод главной категории
    $mainPageModel = new StructCategory;
    $mainPage = $mainPageModel->published()->findByAttributes(
        array($mainPageModel->levelAttribute => 2, 'alias' => 'main'));

    if ($mainPage) {
        $liHtmlOptions['class'] = 'item';

        if (Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'index') {
            $liHtmlOptions['class'] .= ' active';
        }

        $liHtmlOptions['data-id'] = $mainPage->id;
        $liHtmlOptions['data-alias'] = $mainPage->alias;
        $liHtmlOptions['data-type'] = $mainPage->type;
        echo CHtml::openTag('li', $liHtmlOptions);
        echo CHtml::link($mainPage->title, '/');
        echo CHtml::closeTag('li');
    }
}

$prevLevelElement = NULL;
$prevElement = NULL;
$prevLevel = 0;

// URL будет создаваться из того что есть, чтобы не гонять
// запросы к базе на каждом елементе с помощью createUrl()
$urlElements = $this->parentCategory->getAncestorsAliases();
$currentCategory = Yii::app()->struct->currentCategory;

foreach ($categories->findAll() as $category) {
    $openUl = false;
    $closeUl = false;

    $liHtmlOptions = array();
    $liHtmlOptions['class'] = 'item';
    $liHtmlOptions['data-id'] = $category->id;
    $liHtmlOptions['data-alias'] = $category->alias;
    $liHtmlOptions['data-type'] = $category->type;
    if (($currentCategory && ($currentCategory->id == $category->id)) ||
        ($currentCategory &&
            ($currentCategory->_left > $category->_left) &&
            ($currentCategory->_right < $category->_right))
    ) $liHtmlOptions['class'] .= ' active';

    if($category->tech_id){
        $liHtmlOptions['data-tech_id'] = $category->tech_id;
    }

    // подуровни
    if ($prevLevel && ($category->$_level > $prevLevel)) {
        if ($prevElement && ($prevElement->type != 'group'))
            $urlElements[] = $prevElement->alias;
        $prevLevelElement = $category;

        echo CHtml::openTag('ul', array(
            'data-absolute-level' => $category->_level,
            'data-relative-level' => ($category->_level - $this->_startLevel)
        ));
        echo CHtml::openTag('li', $liHtmlOptions);
        $openUl = true;
    } else if ($prevLevel && ($category->$_level < $prevLevel)) {
        $closeUl = true;
        if (($dif = $prevLevel - $category->$_level) > 0)
            for ($i = 0; $i < $dif; $i++) {
                echo CHtml::closeTag('ul');
                echo CHtml::closeTag('li');
            }
        echo CHtml::openTag('li', $liHtmlOptions);


        if ($prevLevelElement && $prevLevelElement->type != 'group') {
            for ($i = 0; $i < ($prevLevel - $category->$_level); $i++)
                array_pop($urlElements);
        }
    } else if ($prevLevel) {
        echo CHtml::closeTag('li');
        echo CHtml::openTag('li', $liHtmlOptions);
    } else echo CHtml::openTag('li', $liHtmlOptions);

    $url = sizeof($urlElements) ? '/' . implode('/', $urlElements) : NULL;


    if ($category->type == 'group')
        $title = "<a>{$category->title}</a>";
    else {
        $itemUrl = ($category->type == 'external_reference')
            ? $category->external_reference : "{$url}/{$category->alias}";
        if($currentCategory && $currentCategory->id==$category->id){
            $title = "<a>{$category->title}</a>";
        }
        else{
                $title = CHtml::link($category->title, $itemUrl);
        }


    }
    echo $title;


    $prevLevel = $category->$_level;
    $prevElement = $category;
}

/**
 * @todo Проверить, правильно ли закрывает тэги. Может порвать верстку.
 */
if ($this->_startLevel && $prevLevel && (($dif = $prevLevel - $this->_startLevel) > 0))
    for ($i = 1; $i < $dif; $i++) {
        echo CHtml::closeTag('ul');
        echo CHtml::closeTag('li');
    }

echo CHtml::closeTag('ul');

//echo CHtml::closeTag('ul');
?>
