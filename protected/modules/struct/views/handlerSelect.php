<?php

$name = get_class($this->model)."[{$this->attribute}]";
$value = $this->model->{$this->attribute};

?>
<div class="handlerSelect" <?= CHtml::renderAttributes($this->htmlOptions) ?>>
    <div class="controllers">
        <select data-value="<?=$value?>" class="formElement" name="<?= $name ?>">
            <?
                foreach($this->_controllers as $alias => $title){
                    ?><option value="<?=$alias?>"<?=$alias == $value ? ' selected' : '' ?>><?=$title?></option><?
                }
            ?>
        </select>
    </div>
    <div class="modules">
        <? if($this->_modules){ ?>
        <select data-alue="<?=$value?>" class="formElement" name="<?= $name ?>">
            <?
                foreach($this->_modules as $alias => $title){
                    ?><option value="<?=$alias?>"<?=$alias == $value ? ' selected' : '' ?>><?=$title?></option><?
                }
            ?>
        </select>
        <? }else{ ?>
            <input value="<?=$value?>" placeholder="<?= Yii::t('struct', 'Module name'); ?>" type="text" class="formElement span2" /><br/><br/>
            <p class="alert alert-block">
                <?
                    echo Yii::t('struct',
                        '<strong>Cant search modules!</strong><br/> System cant modules, because commponents/modules system is not installed. '.
                        'You can only set module manually, but it may fail, if module not installed.');
                ?>
            </p>
        <? } ?>
    </div>

</div>
