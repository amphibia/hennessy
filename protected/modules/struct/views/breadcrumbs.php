<?php

$ancestorsAliases = array( );

echo CHtml::openTag( 'ol', $this->htmlAttributes );
foreach ( $ancestors as $key => $value ) {
	if ( $value->alias && $value->alias != 'main' ) {
		$ancestorsAliases[] = $value->alias;
	}
	$class = '';
	if (!(Yii::app()->struct->currentPage) && isset( Yii::app()->struct->currentCategory ) && Yii::app()->struct->currentCategory->id == $value->id ) {
		$class = 'active';
	}
	echo CHtml::openTag( 'li', array( 'id' => 'menuItem' . $value->id, 'class' => $class ) );
	if ( $class == 'active' ) {
		if ( $this->showCurrent ) {
			echo $value->title;
		}
	} else {
		echo CHtml::link( $value->title, '/' . implode( '/', $ancestorsAliases ) );
		if ( $this->divider ) {
			echo CHtml::tag( 'span', array( 'class' => 'divider' ), $this->divider );
		}
	}
	echo CHtml::closeTag( 'li' );
}
if(Yii::app()->struct->currentPage){
	echo CHtml::openTag( 'li', array( 'id' => 'menuItem' . Yii::app()->struct->currentPage->id, 'class' => 'active' ) );
	echo Yii::app()->struct->currentPage->title;
	echo CHtml::closeTag( 'li' );
}
echo CHtml::closeTag( 'ol' );