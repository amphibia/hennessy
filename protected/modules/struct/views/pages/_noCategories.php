<br/>
<div class="alert alert-block alert-warning fade in">
	<strong><?= Yii::t('struct', 'No categories'); ?></strong>
	<p><?= Yii::t('struct', 'You can`t create pages, until not create one or more categories.'); ?></p>
</div>