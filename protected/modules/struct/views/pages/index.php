<? if(!Yii::app()->request->isAjaxRequest){ ?>
    <div class="page-header">
    
    <h1><?= Yii::t('struct', 'Page management'); ?></h1><br/>

    <?
	
    $this->widget(
        Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__)
    );

	if(isset($_GET['parent']) && $_GET['parent'])
	{
		$category = $_GET['parent'];

		// подмена кнопки "Add" - будет добавляться страница для этой
		// категории
		$config['headerButtons'] = array(
			array(
				'buttonType'=>'link',
				'icon'=>'icon-plus',
				'label'=>Yii::t('admin', 'Add'),
				'route'=>array('create', array('catergory_id'=>$category->id))
			)
		);

		// кнопка "назад к списку категорий"
		$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
			'size'=>'small',
			'buttonType'=>'link',
			'icon'=>'icon-arrow-left',
			'label'=>Yii::t('struct', 'To categories index'),
			'url'=>Yii::app()->getService('categories')->createUrl('index'),
		));

		?>
		</div>
		<h2 class="category_name">
			<?= Yii::t('struct', 'Pages of category:') ?><br/>
			<a href="<?= Yii::app()->getService('categories')->createUrl('update', array($category->id)) ?>">
				<?= $category->title ?>
			</a>
		</h2>
		<br/>
		<a href="<?= $this->createUrl('index') ?>"><?= Yii::t('struct', 'Show all categories pages'); ?></a>
		<br/><br/>
		<?
	}
	else
	{
		// кнопка "в индекс админки"
		$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
			'size'=>'small',
			'buttonType'=>'link',
			'icon'=>'icon-arrow-left',
			'label'=>Yii::t('admin', 'Administration index'),
			'url'=>'/admin/',
		));

		?>
		</div>

		<div class="selectedTags">
			<? $this->renderPartial($this->views['_tagsSelect']); ?>
		</div>
		<div class="selectedCategories">
			<? $this->renderPartial($this->views['_categoriesSelect']); ?>
		</div>

		<?
 	}   
    
}

// область кэшируется перед выводом
$cacheId = md5(serialize($_POST) . $_SERVER['REQUEST_URI']) . "__indexViewPages";
$cacheOptions = array(
	'duration' => Yii::app()->struct->cacheExpire,
	'dependency' => Yii::app()->struct->getCategoriesCacheDepency(),
);
if($this->beginCache($cacheId, $cacheOptions)){
	$this->widget(Yii::localExtension('ActiveTable', 'ActiveTable'), $config);
$this->endCache(); }

?>
