<script>

$('#createConfiguration').live('click', function(){
	$(this).closest('.unexistsSection').remove();
	$('#formBillet').fadeIn(300);
});

</script>
<div id="optionsPage">

	<div class="page-header">
	<h1><?= Yii::t('struct', 'Site category options') . ":"; ?><div class="itemTitle"><?= $this->model->title ?></div></h1><br/>
	<?

	// кнопка "в индекс раздела"
	$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
		'size'=>'small',
		'buttonType'=>'link',
		'icon'=>'icon-arrow-left',
		'label'=>Yii::t('struct', 'Site categories management'),
		'url'=>$this->createUrl('index'),
	));

	?>
	</div>

	<?php

	$this->widget(Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__));

	$rel = (isset($_GET['rel']) ? $_GET['rel'] : 'self')
		or $rel = 'self';
	?>
	<div>
		<ul class="context">
			<li class="self<?= ($rel=='self')?" active":"" ?>"><a href="<?= $this->createUrl($this->action->id, array('category'=>$this->model->id, 'rel' => 'self')) ?>"><?= Yii::t('struct', 'Self options'); ?></a></li>
			<li class="children<?= ($rel=='children')?" active":"" ?>"><a href="<?= $this->createUrl($this->action->id, array('category'=>$this->model->id, 'rel' => 'children')) ?>"><?= Yii::t('struct', 'Child categories options'); ?></a></li>
			<li class="pages<?= ($rel=='pages')?" active":"" ?>"><a href="<?= $this->createUrl($this->action->id, array('category'=>$this->model->id, 'rel' => 'pages')) ?>"><?= Yii::t('struct', 'Pages options'); ?></a></li>
		</ul>
	</div>

	<?
	
	// вывод автоформы
	$formOutput = $this->widget(Yii::localExtension('bootforms', 'BootAutoForm', __FILE__),
		array(
			'form' => $form,
			'autoElements' => false,
			'type' => 'horizontal',
			'clientOptions' => array(
				'validateOnType' => true,
				'validateOnSubmit' => true,
				'validateOnChange' => true,
			),
			'enableClientValidation' => true,
			'enableAjaxValidation' => true,
			'referer' => Yii::app()->request->scriptUrl,
			'repeatActionOption' => false,
		), true
	);

	// вывод приглашения создать модель настроек, если она не создана
	if($form->model->isNewRecord && !isset($_POST[get_class($form->model)])){
		?><div class="unexistsSection"><?
			?><p><?
				echo Yii::t('struct',
					'Category "{categoryTitle}" has`t settings session for this section.',
					array("{categoryTitle}" => $this->model->title)
				);
				?><br/><br/><?
				echo Yii::t('struct', 'You can create it.');
			?></p><?

			?><br/><?
			?><div id="createConfiguration" class="btn btn-success btn-large"><?
				echo Yii::t('struct', 'Create configuration');
			?></div><?
		
		?></div><?
		
		?><div id="formBillet" style="display:none;"><?
			echo $formOutput;
		?></div><?
	}
	else echo $formOutput;

	?>
	
</div>

