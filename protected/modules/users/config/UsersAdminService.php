<?php

return array(

    /**
     * Поля, отображаемые в таблице индекса (actionIndex).
     * @var array
     */
    'indexTableOptions' => array
    (
        /**
         * Поле сортировки.
         * @var string
         */
        'sortField' => 'date_created',

        'htmlAttributes' => array
        (
            "container"         => array(),

            "table"             => array("cellspacing"=>0, "cellpadding"=>0),
            "row"               => array(),
            "headRow"           => array(),
            "cell"              => array("valign"=>"top"),

            // дополнительные поля
            "additionalField"   => array(),

            // настройка формы фильтров по таблице
            "form"          => array("method" => "POST"),
            "searchBlock"   => array(),
            "searchField"   => array(),
            "searchSubmit"  => array(),
            "scopeButton"   => array(),
            "scopeValue"    => array(),

            "info"          => array(),
        ),

        'fieldsOrder' => array('username', 'role_id', 'status', 'email', 'banned', 'date_created', 'attensions', 'update'),

        'preConvertors' => array
        (
            'username' => function($fieldName, $fieldValue, $row, $isHeader = false, $model)
                {
                    if($isHeader) return $fieldValue;
                    return "<span class=\"icon-user\"></span> ".
                    CHtml::link($fieldValue,
                        Yii::app()->controller->createUrl('update', array('id'=>$row['id'])));
                },

            // роль
            'role_id' => function($fieldName, $fieldValue, $row, $isHeader = false, $model){
                    if($isHeader) return Yii::t('users', 'Role');
                    if(!(int)$fieldValue) return;
                    return (int)$fieldValue
                        ?  "<span class=\"icon-tags\"></span> ". CHtml::link($row->role->title, Yii::app()->getService('roles')->createUrl('update', array($fieldValue)))
                        : '-';
                },

            // предупреждения (внутренняя система анализа)
            'attensions' => function($fieldName, $fieldValue, $row, $isHeader = false, $model){
                    if($isHeader) return $fieldValue;
                    return (int)$fieldValue ? $fieldValue : '-';
                },

            'status' => function($fieldName, $fieldValue, $row, $isHeader = false, $model){
                    if($isHeader) return $fieldValue;

                    /*
                    return CHtml::dropDownList(
                        'status', $fieldValue,
                        array_combine($model->statusRange, $model->statusRange),
                        array(
                            'class'=>'span2 statusToggle',
                            'data-id'=>$row['id']
                        )
                    )
                     */
                    Yii::app()->controller->widget('ext.bootstrap.widgets.TbEditableField', array(
                        'type' => 'select',
                        'model' => $row,
                        'attribute' => 'status',
                        'url' => Yii::app()->controller->createUrl('simpleattr', false),
                        'source' => array_combine($model->statusRange, $model->statusRange),
                        'title' => Yii::t('users', 'Select user status'),
                        //'enabled' => true
                    ));
                }
        ),

        'route' => 'index',

        'buttons' => array
        (
            'update' => array(

                'htmlOptions'=>array('class'=>'pageManagement pull-right'),

                'buttons' => array(
                    array(
                        'label' => ' ',
                        'icon'=>'icon-edit',
                        'items' => array(
                            'update' => array('label'=>Yii::t('app', 'Open in edit form'), 'icon'=>'icon-pencil',),
                            'profile' => array('label'=>Yii::t('app', 'User profile'), 'icon'=>'icon-user', 'route'=>array('profile', 'user_id'=>'id'), 'itemOptions'=>array('class'=>'profileButton')),
                            'delete' => array('label'=>Yii::t('app', 'Delete item'), 'icon'=>'icon-trash', 'itemOptions'=>array('class'=>'deleteButton')),
                        ),
                    )
                )
            )
        ),

        'pageSize' => 15,
        'sortVar' => 'orderBy',

        'scopes' => array
        (
            'registered' => function($model){ $model->dbCriteria->addCondition("t.status='registered'"); },
            'imported' => function($model){ $model->dbCriteria->addCondition("t.status='imported'"); },
            'verified' => function($model){ $model->dbCriteria->addCondition("t.status='verified'"); },
            'banned' => function($model){ $model->dbCriteria->addCondition("t.status='banned'"); },
        ),

        'scopeLabels' => array
        (
            'registered' => Yii::t('users', 'Registered users'),
            'imported' => Yii::t('users', 'Imported from socials'),
            'verified' => Yii::t('users', 'Verified'),
            'banned' => Yii::t('users', 'Banned'),
        ),
    ),

    // свойства формы создания
    'createForm' => array
    (
        'elements' => array
        (
            'username' => array('type'=>'text', 'class'=>'span4'),
            'email' => array('type'=>'text', 'class'=>'span3'),
            'role_id' => array(
                'type' => 'dropdownlist',
                'class' => 'span3',
                'items' => CHtml::listData
                        (
                            array_merge(array(0=>''), AuthRole::model()->findAll()),
                            'id', 'title'
                        ),
            ),
            'status' => array(
                'type' => 'default',
                'class' => 'span2',
            ),

            '<br/><div class="control-group hint-top"><div class="controls">'.
            Yii::t('users', 'You can change password of user any time.').
            '</div></div>',

            'password' => array(
                'type' => 'password',
                'class' => 'span3', 'value' => '',
                'placeholder' => Yii::t('users', 'Type new password for change...'),
            ),
            'retype_password' => array(
                'type' => 'password',
                'class' => 'span3', 'value' => '',
                'placeholder' => Yii::t('users', 'Retype new password...'),
            ),

            '<br/>'
        ),

        'showErrorSummary'  => true,
        'buttons'=>array(
            'save' => array('type' => 'submit', 'label' => Yii::t('admin', 'Save')),
        ),
    ),

    // свойства формы профайла
    'profileForm' => array
    (
        'elements' => array(
            'first_name' => array('type'=>'text', 'class'=>'span4'),
            'last_name' => array('type'=>'text', 'class'=>'span4'),
            'socid_vk' => array('type'=>'text', 'class'=>'span3'),
            'socid_facebook' => array('type'=>'text', 'class'=>'span3'),
            'socid_twitter' => array('type'=>'text', 'class'=>'span3'),
            'socid_google' => array('type'=>'text', 'class'=>'span3'),
            'avatar' => array('type' => Yii::localExtension('multiuploader', 'UploadFormElement')),
        ),

        'showErrorSummary'  => true,
        'buttons'=>array(
            'save' => array('type' => 'submit', 'label' => Yii::t('users', 'Save profile')),
        ),
    ),

);