<?php

return array
(

    /**
     * Поля, отображаемые в таблице индекса (actionIndex).
     * @var array
     */
    'indexTableOptions' => array
    (
        'htmlAttributes' => array
        (
            "container"         => array(),

            "table"             => array("cellspacing"=>0, "cellpadding"=>0),
            "row"               => array(),
            "headRow"           => array(),
            "cell"              => array("valign"=>"top"),

            // дополнительные поля
            "additionalField"   => array(),

            // настройка формы фильтров по таблице
            "form"          => array("method" => "POST"),
            "searchBlock"   => array(),
            "searchField"   => array(),
            "searchSubmit"  => array(),
            "scopeButton"   => array(),
            "scopeValue"    => array(),

            "info"          => array(),
        ),

        'fieldsOrder' => array('title', 'super_user', 'guest_role', 'default_role', 'enabled', 'update'),


        'route' => 'index',

        'buttons' => array(
            'update' => array(

                'htmlOptions'=>array('class'=>'pageManagement pull-right', 'rel'=>'tooltip', 'title' => Yii::t('extcore', 'Item manipulations')),

                'buttons' => array(
                    array(
                        'label' => ' ',
                        'icon'=>'icon-edit',
                        'items' => array(
                            'update' => array('label'=>Yii::t('app', 'Open in edit form'), 'icon'=>'icon-pencil',),
                            'delete' => array('label'=>Yii::t('app', 'Delete item'), 'icon'=>'icon-trash', 'itemOptions'=>array('class'=>'deleteButton')),
                        ),
                    )
                )
            )
        ),

		'preConvertors' => array
		(
			'title' => function($fieldName, $fieldValue, $row, $isHeader = false, $model)
			{
				if($isHeader) return $fieldValue;
				return CHtml::link($fieldValue,
					Yii::app()->controller->createUrl('update', array('id'=>$row['id'])));
			},

			// кнопка ВКЛ/ВЫКЛ
			'enabled' => function($fieldName, $fieldValue, $row, $isHeader = false, $model)
			{
				if($isHeader) return $fieldValue;
				return CHtml::checkBox($fieldName, $fieldValue, array(
					'class' => 'enabledToggle',
					'data-id' => $row['id']
				));
			},

			'super_user' => function($fieldName, $fieldValue, $row, $isHeader = false, $model)
			{
				if($isHeader) return $fieldValue;
				return CHtml::checkBox($fieldName, $fieldValue, array(
					'class' => 'superUserToggle',
					'data-id' => $row['id']
				));
			},

			'guest_role' => function($fieldName, $fieldValue, $row, $isHeader = false, $model)
			{
				if($isHeader) return $fieldValue;
				return CHtml::checkBox($fieldName, $fieldValue, array(
					'class' => 'guestRoleToggle',
					'data-id' => $row['id']
				));
			},
					
			'default_role' => function($fieldName, $fieldValue, $row, $isHeader = false, $model)
			{
				if($isHeader) return $fieldValue;
				return CHtml::checkBox($fieldName, $fieldValue, array(
					'class' => 'defaultRoleToggle',
					'data-id' => $row['id']
				));
			},
		),

        'scopes' => array
		(
			'super_users' => function($model){ $model->dbCriteria->addCondition("t.super_user>0"); },
			'enabled' => function($model){ $model->dbCriteria->addCondition("t.enabled>0"); },
			'disabled' => function($model){ $model->dbCriteria->addCondition("t.enabled=0"); },
        ),

		'scopeLabels' => array
		(
			'super_users' => Yii::t('users', 'Super users'),
			'enabled' => Yii::t('users', 'Enabled roles'),
			'disabled' => Yii::t('users', 'Disabled roles'),
		),

        'pageSize' => 15,
        'sortVar' => 'orderBy',
    ),

    // свойства формы создания
    'createForm' => array
    (
        'elements' => array
        (
            'title' => array('type'=>'text', 'class'=>'span6'),
			'description' => array('type'=>'textarea', 'class'=>'span6'),			
			'super_user' => array('type'=>'checkbox'),
			'operations' => array('type'=>Yii::localExtension('mtmselect', 'MTMSelect'), 'route'=>'OperationsMTMPage', 'titleField'=>'name'),

			'<div class="control-group"><div class="controls"><div class="super_user_info alert alert-info">' .
				Yii::t('users', 'Super user already have all privilege.') .
			'</div></div></div>',

			'guest_role' => array('type'=>'checkbox'),
			"<div class=\"control-group hint-bottom\"><div class=\"controls\">".
				Yii::t('users', "Guest role with associated with unauthorized user.").
			"</div></div>",

			'default_role' => array('type'=>'checkbox'),
			"<div class=\"control-group hint-bottom\"><div class=\"controls\">".
				Yii::t('users', "This option set role as default for new users.<br/> You must have default role, if you use registration services.").
			"</div></div>",
			
			'enabled' => array('type'=>'checkbox'),
			"<div class=\"control-group hint-bottom\"><div class=\"controls\">".
				Yii::t('users', "'Enabled' attribute turn on/off all users with this role.").
			"</div></div>",
		),

        'showErrorSummary'  => true,
        'buttons'=>array(
            'save' => array('type' => 'submit', 'label' => Yii::t('admin', 'Save')),
        ),
    ),

);