<?php

/**
 * Контроллер-точка входа/выхода при аутентификации пользователя.
 * Подключается через controllerMap компонентом UsersComponent.
 */
class AuthController extends ExtController
{
	public $viewsPath = 'application.modules.users.views';

	public $layout = 'application.modules.users.views.layouts.auth';
	
	/**
	 * Компонент, с которым работает контроллер.
	 * @var UsersComponent
	 */
    public $usersComponent = NULL;


	public $views = array(
		'loginForm' => 'auth/loginForm'
	);

	public $assets = array(
		'css' => array('auth.css')
	);


    protected function beforeAction($action){
		$this->viewPath = Yii::getPathOfAlias($this->viewsPath);
        Yii::loadExtConfig($this, __FILE__, __CLASS__.".php");
		Yii::applyAssets($this, __FILE__);
		$this->usersComponent->onBeforeActions(new CEvent($this, compact('action')));
        return parent::beforeAction($action);
    }

	/**
	 * Название операции, которую использует RBAC авторизация.
	 * 
	 * @return mixed Cтрока с названием операции, или массив пар ключ => значение,
	 * в котором ключ соответсвует действию, значение название операции. Если
	 * указанно NULL, контроль RBAC авторизации не применяется.
	 * 
	 * @example
	 * return array
	 * (
	 *     "edit"       => "editPost",
	 *     "delete"     => "deletePost",
	 *     "default"    => "controllerAction",
	 *     "error"      => NULL,
	 * );
	 */
	public function getRbacOperation() {
		// эта операция не должна использовать RBAC - авторизацию
		// (в данном случае - во измежание бесконечного перехода)
		return NULL;
	}

    public function actionIndex() {
        throw new CHttpException(403);
    }

    /**
     * Точка входа на сайт.
     */
    public function actionLogin()
    {
		// если вход уже выполнен
		if(!Yii::app()->user->isGuest)
			$this->usersComponent->redirectToDefaultPage();

		// вывод формы входа в систему
		else
		{
			Yii::import('application.modules.users.models.*');
			Yii::import('application.modules.components.models.*');
			$this->model = new $this->usersComponent->formModelClass;
			
			// был сабмит формы
			if(isset($_POST[$this->usersComponent->formModelClass]))
			{
				// AJAX-валидация
				if(Yii::app()->request->isAjaxRequest) {
					echo CActiveForm::validate($this->model);
					Yii::app()->end();
				}

				$this->model->setAttributes($_POST[$this->usersComponent->formModelClass]);
				$this->model->usersComponent = $this->usersComponent;
				$this->model->userIdentity = new $this->usersComponent->userIdentityClass(
					$this->model->username,
					$this->model->password
				);

				$this->model->login();
			}

			$this->render($this->views['loginForm']);
		}
	}

    /**
     * Выход из системы.
     */
	public function actionLogout(){
		$this->usersComponent->logout();
	}

}

?>