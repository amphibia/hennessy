<?php

/**
 * Регистрация пользователя.
 * 
 * Предоставляет функционал регистрации, подтверждения, управления профилем,
 * восстановления пароля.
 */		
class UsersController extends ExtController
{
	/**
	 * Разрешена ли регистрация на сайте.
	 * @var boolean
	 */
	public $allowRegistration = false;

	/**
	 * Компонент, с которым работает контроллер.
	 * @var UsersComponent
	 */
    public $usersComponent = NULL;

	/**
	 * Метод (функция), которая выводит профайл пользователя.
	 * @var callback
	 */
	public $profileMethod = array();
	
	/**
	 * Настройки формы регистрации.
	 * @var array
	 */
	public $registrationFormOptions = array();


	public $views = array();
	public $assets = array();


    public function getRbacOperation(){
		// операция контроллера (кроме profile) не должны использовать
		// RBAC - авторизацию (в данном случае - во измежание бесконечного перехода)
		return array(
			'default' => NULL,
			'profile' => 'Profile access',
			'passwordRecovery' => 'Password recovery request',
			'passwordChange' => 'Password changing',
		);
    }

    protected function beforeAction($action){
		$this->viewPath = array(); // ХАК, чтобы можно было переопределять из конфига
        Yii::loadExtConfig($this, __FILE__);
		Yii::applyAssets($this, __FILE__);

		$this->usersComponent->onBeforeActions(new CEvent($this, compact('action')));
        return parent::beforeAction($action);
    }


    public function actionIndex(){
		throw new CHttpException(403);
	}

	/**
	 * Интерфейс регистрации нового пользователя.
	 */
	public function actionRegistration()
	{
		if(!$this->allowRegistration) throw new CHttpException(404);
		

		// если пользователь выполнил вход, открывается профиль
		if(!Yii::app()->user->isGuest) $this->redirect('profile');

		$this->modelClass = $this->usersComponent->registrationModelClass;
        $this->_model = new $this->modelClass('registration');

		// указание связанных моделей в соответствии с настройками UsersController
		$this->model->userModelClass = $this->usersComponent->modelClass;
		$this->model->roleModelClass = $this->usersComponent->roleModelClass;
		$this->model->profileModelClass = $this->usersComponent->profileModelClass;

		// при сабмите формы
		if(isset($_POST[$this->modelClass]))
		{
			$this->model->setAttributes($_POST[$this->modelClass]);

            // AJAX-валидация
            if(Yii::app()->request->isAjaxRequest){
                echo CActiveForm::validate($this->model);
                Yii::app()->end();
            }

			if($this->model->validate() && $this->model->save()){

				// связанные события - при регистрации
				$this->usersComponent->raiseEvent(
					'onRegistration', new CEvent($this, array(
						'formModel' => $this->model,
						'userModel' => $this->model->user
					))
				);

				// типа все нормально
				$this->render($this->views['registrationSuccessfull']);
				return;
			}
		}

		$this->registrationFormOptions['model'] = $this->_model;
		$form = new CForm($this->registrationFormOptions);

		// форма регистрации
		$this->render($this->views['registrationForm'], compact('form'));
	}

	/**
	 * Подтверждение решистрации.
	 * Проверяет соответствие кода соответствия сводным данным, а также
	 * актуальность этого кода.
	 * 
	 * Метод работает с моделью User и использует ее функционал проверки кода.
	 * 
	 * @param string $verifyCode
	 * @param string $timestamp
	 * @param string $username
	 * @param string $email
	 */
	public function actionVerify($verifyCode, $timestamp, $username, $email)
	{
		$user = User::model()->findByAttributes(compact('email', 'username'));
		if(!$user) throw new CHttpException(404);
				
		if($user->checkVerifyCode($verifyCode, $timestamp))
		{
			// только для пользователей со статусом "registered"
			if($user->status != 'registered'){
				$this->render($this->views['alreadyVerified'], get_defined_vars());
				return;
			}

			$user->status = 'verified';
			if($user->save(true, array('status')))
				 $this->render($this->views['accountVerified'], get_defined_vars());
			else $this->render($this->views['accountVerifyError'], get_defined_vars());
		}
		else $this->render($this->views['wrongVerifyCode'], get_defined_vars());
	}

	/**
	 * Страница запроса на изменение пароля.
	 * Выводится форма, в которой запрашиваются данные (email, телефон) для
	 * отправки кода подтверждения.
	 */
	public function actionPasswordRecovery()
	{
		// если пользователь выполнил вход, открывается профиль
		if(!Yii::app()->user->isGuest) $this->redirect('profile');

		$this->modelClass = $this->usersComponent->recoveryModelClass;
		$this->model->userModelClass = $this->usersComponent->modelClass;

		// была отправка формы
		if(isset($_POST[$this->modelClass]))
		{
			$this->model->setAttributes($_POST[$this->modelClass]);
			
			// AJAX-валидация
			if(Yii::app()->request->isAjaxRequest) {
				echo CActiveForm::validate($this->model);
				Yii::app()->end();
			}

			if($this->model->resolve())
			{
				// связанные события - при запросе на восстановление пароля
				$this->usersComponent->raiseEvent(
					'onPasswordRecovery', new CEvent($this, array(
						'formModel' => $this->model,
						'userModel' => $this->model->user
					))
				);

				$this->render($this->views['passwordSendedMessage']);
				return;
			}
		}

		// форма регистрации
		$this->render($this->views['passwordRequestForm']);
	}
	
	public function actionPasswordChange($verifyCode = NULL, $timestamp = NULL, $username = NULL, $email = NULL)
	{
		if(!Yii::app()->user->isGuest)
			throw new CHttpException(404);

		$userModel = new $this->usersComponent->modelClass;
		$user = $userModel->findByAttributes(compact('username', 'email'));
		if(!$user) throw new CHttpException(404);

		if(!$user->checkVerifyCode($verifyCode, $timestamp, 'RECOVERY_EMAIL'))
			throw new CHttpException(404);


		$this->modelClass = $this->usersComponent->changeModelClass;
		$this->model->user = $user;

		// была отправка формы
		if(isset($_POST[$this->modelClass]))
		{
			$this->model->setAttributes($_POST[$this->modelClass]);
			
			// AJAX-валидация
			if(Yii::app()->request->isAjaxRequest) {
				echo CActiveForm::validate($this->model);
				Yii::app()->end();
			}

			// сохранение нового пароля
			if($this->model->save()){

				// связанные события - при запросе на восстановление пароля
				$this->usersComponent->raiseEvent(
					'onPasswordChange', new CEvent($this, array(
						'formModel' => $this->model,
						'userModel' => $this->model->user
					))
				);

				$this->render($this->views['passwordSuccessMessage']);
				return;
			}
		}

		// форма регистрации
		$this->render($this->views['passwordChangeForm']);
	}

	/**
	 * Форма редактирования профиля.
	 * Вызывает функцию, указанную в {$this->profileMethod}, которая
	 * занимается непосредственным отоброжением профайла.
	 * 
	 * Если свойство {$this->profileMethod} не задано, вызывает $this->_profile().
	 */
	public function actionProfile(){
		if(!$this->profileMethod) $this->_profile();
	}

	/**
	 * Функция по умолчанию для отображения профиля.
	 */
	protected function _profile()
	{
        die('11');
		$this->modelClass = $this->usersComponent->profileModelClass;
		$form = new CForm($this->model);
		
		$this->render();
	}

}
