<?php
/**
 * Профайл пользователя.
 *
 * Предоставляет функционал редактирование профайла,
 * изменение е-мейла, пароля
 */

class ProfileController extends ExtController
{

    /**
     * Компонент, с которым работает контроллер.
     * @var UsersComponent
     */
    public $usersComponent = NULL;

    public $viewsPath = 'application.modules.users.views';

    public $views = array(
        'nav' => 'profile/nav',
        'index' => 'profile/index',
        'profile' => 'profile/profile',
        'email' => 'profile/email',
        'password' => 'profile/password',
    );

    public $modelPasswordClass = 'PasswordChangeForm';
    public $modelEmailClass = 'EmailChangeForm';

    public $modelPassword = null;
    public $modelEmail = null;

    public function getRbacOperation()
    {
        return 'Profile change';
    }

    protected function beforeAction($action)
    {
        $this->viewPath = Yii::getPathOfAlias($this->viewsPath);
        Yii::loadExtConfig($this, __FILE__, __CLASS__ . ".php");
        Yii::applyAssets($this, __FILE__);
        $this->usersComponent->onBeforeActions(new CEvent($this, compact('action')));

        $this->modelPassword = new $this->modelPasswordClass;
        $this->modelPassword->user = Yii::app()->user->model;
        $this->modelPassword->scenario = 'profile';

        $this->modelEmail = new $this->modelEmailClass;
        $this->modelEmail->user = Yii::app()->user->model;
        $this->modelEmail->scenario = 'profile';

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $this->render($this->views['index']);
    }

    public function actionEmail()
    {
        $success = false;
        if (isset($_POST[$this->modelEmailClass])) {
            $this->modelEmail->setAttributes($_POST[$this->modelEmailClass]);

            // AJAX-валидация
            if (Yii::app()->request->isAjaxRequest) {
                echo CActiveForm::validate($this->modelEmail);
                Yii::app()->end();
            }

            if ($this->modelEmail->save()) {
                // отправка email с кодом подтверждения на смену е-мейла
                $success = true;
            }

        }
        $this->render($this->views['email'], compact('success'));
    }

    public function actionPassword()
    {
        $success = false;
        if (isset($_POST[$this->modelPasswordClass])) {
            $this->modelPassword->setAttributes($_POST[$this->modelPasswordClass]);

            // AJAX-валидация
            if (Yii::app()->request->isAjaxRequest) {
                echo CActiveForm::validate($this->modelPassword);
                Yii::app()->end();
            }

            // сохранение нового пароля
            if ($this->modelPassword->save()) {

                // связанные события - при запросе на восстановление пароля
                $this->usersComponent->raiseEvent(
                    'onPasswordChange', new CEvent($this, array(
                        'formModel' => $this->modelPassword,
                        'userModel' => $this->modelPassword->user
                    ))
                );

                // вход под новым пользователем
                $userIdentity = new $this->usersComponent->userIdentityClass(
                    $this->modelPassword->user->username,
                    $this->modelPassword->user->password
                );
                $userIdentity->usersComponent = $this->usersComponent;
                $this->usersComponent->login($userIdentity, false);

                $success = true;
            }
        }
        $this->render($this->views['password'], compact('success'));
    }
}