<div id="success_message">
	<h1><?= Yii::t('users', 'Password was changed'); ?></h1>
	<p><?= Yii::t('users', 'Your password was changes succesfull.'); ?></p>
	<br/>
	<a class="btn btn-large btn-success" href="/auth/login"><?= Yii::t('users', 'Try to log in'); ?></a>
</div>