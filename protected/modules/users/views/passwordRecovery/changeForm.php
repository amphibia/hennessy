<?

$form = $this->beginWidget('CActiveForm', array(
	'id' => 'password_change_form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
	),
));

?>
<h2 id="prompt"><?= Yii::t('users', 'Enter new password'); ?></h2>
<input type=hidden name="<?= Yii::app()->request->csrfTokenName ?>" value="<?= Yii::app()->request->csrfToken ?>" />
<?

echo $form->passwordField($this->model, 'password', array(
	'placeholder' => $this->model->getAttributeLabel('password'),
	'class' => 'input-block-level'
));
echo $form->error($this->model, 'password');

echo $form->passwordField($this->model, 'retype_password', array(
	'placeholder' => $this->model->getAttributeLabel('retype_password'),
	'class' => 'input-block-level'
));
echo $form->error($this->model, 'retype_password');

?><button id="submit" class="btn btn-large btn-info" type="submit"><?= Yii::t('users', 'Change password') ?></button><?

$this->endWidget();

?>