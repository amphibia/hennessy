<html>
	<head>
	</head>

	<body>
		<h1><?= Yii::t('users', 'Registration success');  ?></h1>

		<p>
			<?= Yii::t('users', 'Thank you for registration at our site!'); ?>
			<?= Yii::t('users',
					'You must go at adress: <a href="{link}">{link}</a> for verify your email.',
					array('{link}' => $verifyUrl)
				);
			?>
		</p>
	</body>

</html>