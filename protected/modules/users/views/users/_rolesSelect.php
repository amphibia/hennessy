
<div class="rolesList">
    <h3><?= Yii::t('users', 'Search users by roles'); ?></h3>
    <?

    $this->widget(Yii::localExtension('mtmselect', 'MTMSelect'), array(
        'relationClassName' => 'AuthRole',
		'titleField' => 'title',
        'model' => $this->model,
        'value' => $this->_rolesFilter,
        'sortable' => false,
        'route' => 'rolesMTMPage',
        'buttonLabel' => Yii::t('users', 'Select roles'),
        'hintTitle' => Yii::t('users', 'Search by roles'),
        'hintText' => Yii::t('users', 'You can view users with one of specified roles.'),
        'noItemsMessage' => Yii::t('mtmselect', '<strong>Cant find operations!</strong><br/>Check your RBAC system.')
    ));

    ?>
</div>