<?php

/**
 * Данные профиля пользователя.
 */
class UserProfile extends ActiveRecord
{
	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_created',
				'updateAttribute' => 'date_changed',
			)
		);
	}

	public function getFullName(){
		$full_name = ($this->first_name);
		$full_name .= ($full_name?' ':'') . $this->last_name;
		return $full_name ? $full_name : (
			$this->user->username?$this->user->username:$this->user->email);
	}

	public function getProfileAvatar(){
		if($this->avatar) return trim(current(explode(',', $this->avatar)));
	}

	public function relations()
	{
		return array('user' => array(self::BELONGS_TO, 'User', 'user_id'));
	}
	
    public function rules()
    {
        $purifier = new CHtmlPurifier();

        return array(
            array('user_id', 'required'),
            array('user_id, date_created, date_changed', 'numerical'),
            array('first_name, last_name', 'filter', 'filter'=>array($purifier, 'purify')),
			array('avatar', Yii::localExtension('multiuploader', 'UploadValidator'), 'max'=>16),

            array('socid_vk, socid_facebook, socid_twitter, socid_google', 'match', 'pattern'=>'\w+'),
            array('data', 'type', 'type'=>'array'),
        );
    }

    public function tableName() {
        return "{{auth_profile}}";
    }
}

?>
