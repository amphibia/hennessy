<?php

/**
 * Модель формы входа в систему.
 */
class AuthOptionsForm extends ExtCoreConfigModel
{
    // данные формы
	public $from_email;
	public $return_path;
	public $from_title;
	public $subject_verify;
	public $subject_forgot_password;
	public $type;
	public $username;
	public $host;
	public $port;
	public $password;
	public $bcc;


    public function rules()
    {
        return array
        (
            array(
				'from_email, from_title, subject_verify, subject_forgot_password, type',
				'required'
			),

			array('from_email, return_path', 'email'),
			array('port', 'numerical', 'integerOnly'=>'true', 'min'=>1, 'max'=>'1024'),
			array('type', 'in', 'range'=>array('mail', 'sendmail', 'smtp')),
			array('host, username, password', 'safe'),
        );
    }

	public function attributeLabels() {
		return array(
			'from_email' => Yii::t('attributes', 'Email in "from"'),
			'from_title' => Yii::t('attributes', 'Title in "from"'),
			'subject_verify' => Yii::t('attributes', 'Subject of verify email'),
			'subject_forgot_password' => Yii::t('attributes', 'Subject of password recovery'),
			'type' => Yii::t('attributes', 'Email type'),
			'username' => Yii::t('attributes', 'Username'),
			'host' => Yii::t('attributes', 'Host'),
			'password' => Yii::t('attributes', 'User password'),
			'bcc' => Yii::t('attributes', 'Hidden copies reciever'),
		);
	}

	/**
	 * Для POP3 и SMTP требуются авторизационные данные.
	 * @return boolean
	 */
	protected function beforeValidate()
	{
		$attrubutes = $this->getAttributes();
		if(isset($attrubutes['password']) && !$attrubutes['password'])
			unset($attrubutes['password']);

		$this->setAttributes(
			array_merge(require $this->_configFilename, $attrubutes));

		if($this->type != 'mail')
		{
			if(!$this->host) $this->addError('host', Yii::t('users', 'Hostname required'));
			if(!$this->port) $this->addError('port', Yii::t('users', 'Port requiered'));
			if(!$this->username) $this->addError('username', Yii::t('users', 'Username required'));
			if(!$this->password) $this->addError('password', Yii::t('users', 'Password required'));
		}

		return parent::beforeValidate();
	}
}



?>
