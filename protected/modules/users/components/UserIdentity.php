<?php

/**
 * Идентификационная карточка пользователя при авторизации.
 * Создается компонентом @see UsersComponent и использует его настройки при работе.
 * 
 * Использует модельную инфраструктуру модуля (по умолчанию, классы User, UserProfile
 * AuthRole, AuthOperation) для сопоставления данных пользователя с данными карточки.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Компонент-инициатор авторизациия.
	 * @var UsersComponent
	 */
    public $usersComponent = NULL;

	/**
	 * Модель с данными пользователя.
	 * @var CModel
	 */
	public $model = NULL;


    public function __construct($username, $password){
		$this->username = $username;
		$this->password = $password;
	}

	/**
	 * Проверка авторизационных данных.
	 * В случае неудачи, оставляет код и развернутое сообщение об ошибке.
	 * @return boolean результат проерки
	 */
	public function authenticate()
	{
		$userModelClass = $this->usersComponent->modelClass;
		if($this->model = $userModelClass::findUser($this->username, $this->password))
		{	
			// запись о выполненном входе
			$loginRecord = new $this->usersComponent->loginModelClass;
			$loginRecord->setAttributes(array(
				'user_id' => $this->model->id,
				'provider' => 'Users module UserIdentity',
				'ip' => $_SERVER['REMOTE_ADDR'],
				'user_agent' => $_SERVER['HTTP_USER_AGENT'],
				'referrer' => $_SERVER['HTTP_REFERER'],
			));
			$loginRecord->save();

			$this->password = $this->model->hashedPassword($this->password);
			return true;
		}
		else
		{
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			$this->errorMessage = Yii::t('users',
				'Can`t find user with this authentificate data!');

			return false;
		}
	}

    /**
     * Вместо username, возвращается порядковый индекс.
     * @return integer 
     */
    public function getId(){        
        return $this->model ? $this->model->id : 0;
    }

}