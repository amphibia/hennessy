<?php

/**
 * Центральный компонент, реализующий RBAC-систему.
 * Хранит настройки и события, которые могут быть переопределены в конфиге.
 * Зависимые компоненты, также используют эти настройки.
 * 
 * Для отправки email (при регистрации и восстановлении пароля) использует
 * компонент MailerComponent.
 *
 * @title Users component
 * @description { Central component of RBAC-system in 'users' module. }
 * @preload yes
 * @vendor koshevy
 * @package users
 * @requirements{component:mailer }
 */
class UsersComponent extends CApplicationComponent
{
	/**
	 * Модель пользователя, с которой работает компонент.
	 * @var string
	 */
	public $modelClass = NULL;

	/**
	 * Класс модели роли пользователя.
	 * @var string
	 */
	public $roleModelClass = NULL;

	/**
	 * Класс модели операции, требующей доступа.
	 * @var string
	 */
	public $operationModelClass = NULL;

	/**
	 * Класс модели записи о входе.
	 * @var string
	 */
	public $loginModelClass = NULL;
	
	/**
	 * Класс модели записи о входе.
	 * @var string
	 */
	public $profileModelClass = NULL;

	/**
	 * Класс нового компонента "user", которым подменяется родной компонент Yii.
	 * @var string
	 */
	public $userComponentClass = NULL;

	/**
	 * Класс модели формы авторизации.
	 * @var string
	 */
	public $formModelClass = NULL;

	/**
	 * Класс модели формы авторизации.
	 * @var string
	 */
	public $registrationModelClass = NULL;

	/**
	 * Класс модели формы запроса на восстановление пароля.
	 * @var string
	 */
	public $recoveryModelClass = NULL;

	/**
	 * Класс модели формы смены пароля.
	 * @var string
	 */
	public $changeModelClass = NULL;

	/**
	 * CUserIdentity - элемент, используемый для входа.
	 * @var string
	 */
	public $userIdentityClass = NULL;

	/**
	 * Требуется подтверждение зарегистрированного пользователя,
	 * без которого он вспринимается как гость.
	 *
	 * @var boolean
	 */
	public $needVerification = true;
	
	/**
	 * RBAC - операция по умолчанию.
	 * Проименяется для контроллеров, к которым не указана RBAC - операция.
	 * @var string
	 */
	public $rbacDefaultOperation = NULL;

	/**
	 * URL формы входа. Значение будет указано в одноименное свойство компонента
	 * 'user'. Здесь указано, чтобы было удобней конфигурировать из одного места.
	 * @var string
	 */
	public $loginUrl = NULL;
	
	/**
	 * Адрес выхода.
	 * @var string
	 */
	public $logoutUrl = NULL;

	/**
	 * Длительность сессии входа. Будет передано аругентом в метод
	 * CWebUser::login(IUserIdentity $identity, integer $duration=0) 
	 * @var int
	 */
	public $loginDuration = NULL;

	/**
	 * Сообщение, что требуется авторизация.
	 * @var string
	 */
	public $loginRequiredAjaxResponse = NULL;

	/**
	 * Аккаунт для MailerComponent.
	 * @var array 
	 */
	public $mailerAccount = array();

	/**
	 * Событие при входе.
	 * @param CEvent $event
	 */
	public function onLogin($event){ $this->raiseEvent('onLogin', $event); }

	/**
	 * Событие при выходе.
	 * @param CEvent $event
	 */
	public function onLogout($event){ $this->raiseEvent('onLogout', $event); }

	/**
	 * Событие при неудачной попытке входа.
	 * @param CEvent $event
	 */
	public function onFail($event){ $this->raiseEvent('onFail', $event); }

	
	/**
	 * Событие при регистрации.
	 * В $params отправляются:
	 *		User userModel - созданный при регистрации пользователь
	 *		RegistrationForm formModel - заполненна форма регистрации
	 * @param CEvent $event
	 */
	public function onRegistration($event){ $this->raiseEvent('onRegistration', $event); }

	/**
	 * Событие при запросе на восстановлении пароля.
	 * @param CEvent $event
	 */
	public function onPasswordRecovery($event){ $this->raiseEvent('onPasswordRecovery', $event); }

	/**
	 * Событие при смене пароля (при восстановлении).
	 * @param CEvent $event
	 */
	public function onPasswordChange($event){ $this->raiseEvent('onPasswordChange', $event); }

	/**
	 * Собыите, возникающее перед действиями контроллеров, привязанных к этому
	 * компоненту (UsersController и AuthController).
	 * 
	 * @param CEvent $event
	 */
	public function onBeforeActions($event){ $this->raiseEvent('onBeforeActions', $event); }



	public function init()
	{
		Yii::loadExtConfig($this, __FILE__);

		Yii::import('application.modules.users.components.*');
		Yii::import('application.modules.users.services.*');
		Yii::import('application.modules.users.models.*');
		$this->_registerServices();

		// перебивка компонента 'user'
		Yii::app()->setComponent('user', new $this->userComponentClass);
		Yii::app()->user->usersComponent = $usersComponent = &$this;

		Yii::app()->user->loginRequiredAjaxResponse = $this->loginRequiredAjaxResponse;
		Yii::app()->onActionRun = function() use($usersComponent){
			$usersComponent->checkRoleAccess(
				Yii::app()->controller,
				Yii::app()->controller->lastActionID
			);
		};


		// контроллер для входа
		$curDir = basename(__DIR__);
		Yii::app()->controllerMap['auth'] = array(
            'class' => "application.modules.users.controllers.AuthController",
            'usersComponent' => &$this
        );

		// контроллер для регистрации / управления профилем
		$curDir = basename(__DIR__);
		Yii::app()->controllerMap['users'] = array(
            'class' => "application.modules.users.controllers.UsersController",
            'usersComponent' => &$this
        );


		Yii::app()->user->loginUrl = $this->loginUrl;
		Yii::app()->user->initAutorization();
	}

	public function _registerServices()
	{
        // сервис управления пользователями
		Yii::app()->registerService('users', 'UsersAdminService', 'admin',
            array('title' => 'Users management', 'menuGroup' => 'users'));

        // сервис управления ролями
		Yii::app()->registerService('roles', 'UsersRolesAdminService', 'admin',
            array('title' => 'Roles management', 'menuGroup' => 'users') );
		
        // сервис управления настройками регистрации
		Yii::app()->registerService('authOptions', 'UsersOptionsAdminService', 'admin',
            array('title' => 'Registration/authorization options', 'menuGroup' => 'users',
				'dictionary'=>array('actions' => array('form'=>'Email notifications setup'))
			)
		);
	}

    /**
     * Проверка права доступа к контроллеру/действию.
     * Вынесена из Controller в компонент, чтобы предоставить возможность
     * переопределять метод с помощью бихэверов.
     */
    public function checkRoleAccess($controller, $actionID)
    {
		// операция RBAC для этого действия
		$operation = isset($controller->rbacOperation)
			? $controller->rbacOperation : NULL;
		if(!$operation) return true;

		// может быть использовано замыкание
        if(is_callable($operation))
            $operation = call_user_func($actionID, $operation);

		// может быть указан массив операций для каждого действия
		if(is_array($operation))
		{
			// операция по для этого действия или по умолчанию для этого контроллера
			if(isset($operation[$actionID]))
				 $operation = $operation[$actionID];
			else if(isset($operation['default']))
				 $operation = $operation['default'];
			else $operation = $this->rbacDefaultOperation;
		}

		// проверка разрешения
		if(!Yii::app()->user->checkAccess($operation))
		{
			if(Yii::app()->user->isGuest)
			{
				// Сообщение о необходимости выполнить вход. Выводится как обычный текст
				// в обычных случаях, и в ExtCore-JSON протоколе, если используется
				// соответствующая библиотека.
				if(isset($_POST['client_system']) && $_POST['client_system'])
				{
					ExtCoreJson::auth_error();
					ExtCoreJson::response();
				}
				else Yii::app()->user->loginRequired();

			}
			else
				$this->accessDenied();
        }
    }

	/**
	 * Вход с помощью идентификационной карточки.
	 * Этот метод автоматизирует работу, беря на себя перенаправления на соответствующие
	 * страницы  и вызывая события 'onLogin' и 'onFail'.
	 *
	 * @param UserIdentity $userIdentity
	 * @param booelan $redirect требуется ли редирект по завершении
	 * @return boolean результат авторизации
	 */
    public function login(UserIdentity $userIdentity, $redirect = true)
	{
		if($userIdentity->authenticate())
		{
			Yii::app()->user->model = $userIdentity->model;
			Yii::app()->user->login($userIdentity, $this->loginDuration);

			$this->raiseEvent('onLogin', new CEvent($this, $userIdentity));
			if($redirect) Yii::app()->controller->redirect(Yii::app()->user->returnUrl);
			return true;
		}
		else
		{
			$this->raiseEvent('onFail', new CEvent($this, $userIdentity));
			return false;
		}
    }

	/**
	 * Действие по умолчанию при удачной регистрации пользователя.
	 * Привязано к событию onLogin.
	 *
	 * Если потребуется изменить логику, можно переопределить эту функцию,
	 * либо указать другой метод в onLogin.
	 * 
	 * В качестве параметра, получает модель контроллера и модель формы регистрации.
	 * 
	 * @param CEvent $event
	 * @throws CException
	 */
	public function _onRegistration(CEvent $event)
	{
		// отправка email с кодом подтверждения
		$this->sendVerifyEmail($event->params['userModel']);

		// вход под новым пользователем
		$userIdentity = new $this->userIdentityClass(
			$event->params['formModel']->username,
			$event->params['formModel']->password
		);
		$userIdentity->usersComponent = $this;
		$this->login($userIdentity, false);
	}

	/**
	 * Действие по умолчанию при запросе на смену пароля.
	 * Привязано к событию onPasswordRecovery.
	 *
	 * Если потребуется изменить логику, можно переопределить эту функцию,
	 * либо указать другой метод в onPasswordRecovery.
	 * 
	 * В качестве параметра, получает модель контроллера формы.
	 * 
	 * @param CEvent $event
	 * @throws CException
	 */
	public function _onPasswordRecovery(CEvent $event)
	{
		$this->sendRecoveryEmail($event->params['userModel']);
	}

	/**
	 * Отправка письма подтверждения для пользователя.
	 * @param User $user пользователь, для которого требуется подтверждение.
	 */
	public function sendVerifyEmail($user)
	{
		if(!$user->status == 'verified') return false;

		// достаются настройки email-отправки
		$emailOptions = new AuthOptionsForm;
		if(!$emailOptions->validate()){
			throw new CException(Yii::t('users',
				'You must configure email notification at {href}.',
				array('{href}'=>Yii::app()->getService('authOptions')->createUrl('form'))
			));
		}

		$timestamp = time();
		$verifyCode = $user->createVerifyCode($timestamp);
		$verifyUrl = Yii::app()->controller->createAbsoluteUrl('verify',
			array(
				'verifyCode'=>$verifyCode,
				'timestamp'=>$timestamp,
				'username' => $user->username,
				'email' => $user->email
			)
		);

		$mailerAccount = &$this->mailerAccount;
		if(!isset($mailerAccount['options'])) $mailerAccount['options'] = array();

		// свойства из формы
		$mailerAccount['options']['MessageID'] = "VERIFY_EMAIL_$user->id";

		$mailerAccount['options']['From'] = $emailOptions->from_email;
		$mailerAccount['options']['FromName'] = $emailOptions->from_title;
		$mailerAccount['options']['ReturnPath'] = $emailOptions->return_path;
		$mailerAccount['options']['Subject'] = $emailOptions->subject_verify;
		$mailerAccount['options']['Host'] = $emailOptions->host;
		$mailerAccount['options']['Port'] = $emailOptions->port;
		$mailerAccount['options']['Username'] = $emailOptions->username;
		$mailerAccount['options']['Password'] = $emailOptions->password;

		$mailerAccount['view'] = Yii::app()->controller->views['emailVerify'];

		return Yii::app()->mailer->sendMessage(array(
				'to' => $user->email,
				'content' => compact('model', 'verifyUrl'),
			),

			NULL, $mailerAccount
		);
	}

	/**
	 * Отправка письма с ссылкой для восстановления пароля.
	 * @param User $user
	 */
	public function sendRecoveryEmail($user)
	{
		// достаются настройки email-отправки
		$emailOptions = new AuthOptionsForm;
		if(!$emailOptions->validate()){
			throw new CException(Yii::t('users',
				'You must configure email notification at {href}.',
				array('{href}'=>Yii::app()->getService('authOptions')->createUrl('form'))
			));
		}

		$timestamp = time();
		$code = $user->createVerifyCode($timestamp, 'RECOVERY_EMAIL');
		$actionUrl = Yii::app()->controller->createAbsoluteUrl('passwordChange', // действие со сменой пароля
			array(
				'verifyCode'=>$code,
				'timestamp'=>$timestamp,
				'username' => $user->username,
				'email' => $user->email
			)
		);

		$mailerAccount = &$this->mailerAccount;
		if(!isset($mailerAccount['options'])) $mailerAccount['options'] = array();

		$mailerAccount['options']['MessageID'] = "RECOVERY_EMAIL_{$user->id}__".microtime();
		$mailerAccount['options']['From'] = $emailOptions->from_email;
		$mailerAccount['options']['FromName'] = $emailOptions->from_title;
		$mailerAccount['options']['ReturnPath'] = $emailOptions->return_path;
		$mailerAccount['options']['Subject'] = $emailOptions->subject_forgot_password;
		$mailerAccount['options']['Host'] = $emailOptions->host;
		$mailerAccount['options']['Port'] = $emailOptions->port;
		$mailerAccount['options']['Username'] = $emailOptions->username;
		$mailerAccount['options']['Password'] = $emailOptions->password;

		$mailerAccount['view'] = Yii::app()->controller->views['emailRecovery'];
	
		return Yii::app()->mailer->sendMessage(array(
				'to' => $user->email,
				'content' => compact('model', 'actionUrl'),
			),
			NULL, $mailerAccount
		);
	}

	/**
	 * Выход из авторизованного режима.
	 * Перекидывает на соответствующую страницу.
	 */
	public function logout()
	{
		$returnUrl = (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'])
			? $_SERVER['HTTP_REFERER'] : '/';

		Yii::app()->user->logout();
		$this->raiseEvent('onLogout', new CEvent($this));

		Yii::app()->request->redirect($returnUrl ? $returnUrl : '/');
	}
	
	/**
	 * Ошибка доступа (для аутентифицированных пользователей, не имеющих прав
	 * доступа).
	 */
	public function accessDenied() {
		throw new CHttpException( 403,
			Yii::t( 'auth', 'You don\'t have permissions to access this page' ) );
	}

	public function redirectToDefaultPage() {
		Yii::app()->request->redirect("/");
		exit;
	}

}
