<?php
echo CHtml::tag('h1', array(), Yii::t('translate', 'Translation management'));
?>
<div class="tabbable tabs-left" id="translate">
    <ul class="nav nav-tabs">
        <?php
        foreach ($categories as $category) {
            $htmlOptions = array();
            if ($categoryCurrent == $category) {
                $htmlOptions['class'] = 'active';
            }
            echo CHtml::tag('li', $htmlOptions, CHtml::link(Yii::t('messages_categories', $category), $this->createUrl('index', array($category)), array('data-category' => $category)));
        }
        ?>
    </ul>
    <div class="tab-content">
        <?= CHtml::tag('h3', array(), Yii::t('translate', Yii::t('messages_categories', $categoryCurrent))); ?>
        <?php
        if ($this->saved) {
            ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?= Yii::t('app', 'Changes successfully saved'); ?>
            </div>
        <?php
        }
        ?>
        <form class="form-horizontal" method="post">
            <div class="alert alert-warning rowDeleted to_clone hide">
                <?= Yii::t('app', 'Element deleted.'); ?>
                <a href="javascript:;" class="cancel"><?= Yii::t('app', 'Cancel'); ?></a>
            </div>
            <div class="control-group to_clone hide">
                <label class="control-label" for=""></label>

                <div class="controls">
                    <?php
                    $htmlOptions = array(
                        'class' => 'input-xxlarge',
                    );
                    echo CHtml::textArea('values[]', '', $htmlOptions);
                    echo "&nbsp;" . CHtml::htmlButton('<i class="icon-minus icon-white"></i>', array('class' => 'btn btn-danger btn-mini delete', 'rel' => 'tooltip', 'title' => Yii::t('app', 'Remove')));
                    echo CHtml::hiddenField('names[]', htmlspecialchars(''));
                    ?>
                </div>
            </div>
            <?php
            $i = 0;
            foreach ($messages as $message => $translate) {
                ?>
                <div class="control-group translateElement" data-i="<?= $i ?>">
                    <label class="control-label" for="<?= $message ?>"><?= htmlspecialchars($message, ENT_QUOTES) ?></label>

                    <div class="controls">
                        <?php
                        $htmlOptions = array(
                            'class' => 'input-xxlarge',
                        );
                        if ($message == $translate) {
                            $htmlOptions['class'] .= ' need_to_translate';
                        }
                        echo CHtml::textArea('values[' . $i . ']', htmlspecialchars_decode($translate, ENT_QUOTES), $htmlOptions);
                        echo "&nbsp;" . CHtml::htmlButton('<i class="icon-minus icon-white"></i>', array('class' => 'btn btn-danger btn-mini delete', 'rel' => 'tooltip', 'title' => Yii::t('app', 'Remove')));
                        echo CHtml::hiddenField('names[' . $i . ']', htmlspecialchars($message, ENT_QUOTES));
                        ?>
                    </div>
                </div>
                <?php
                $i++;
            }
            ?>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary btn-large"><?= Yii::t('app', 'Save') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>