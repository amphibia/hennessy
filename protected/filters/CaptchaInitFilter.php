<?php

class CaptchaInitFilter extends CFilter
{
    protected function preFilter($filterChain)
    {
        // код, выполняемый до выполнения действия
        if (isset($_GET[0]) && $_GET[0]==1) {
            $value = current($_GET);
            if ($value) $_GET['refresh'] = $value;
        }

        $filterChain->run();
    }

    protected function postFilter($filterChain)
    {
    // код, выполняемый после выполнения действия
    }
}