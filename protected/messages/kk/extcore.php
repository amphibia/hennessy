<?php

return array(
	'Item manipulations'			=> 'Управление элементом',
	'Cant find action (service:{service}, action:{actionID})'			=> 'Cant find action (service:{service}, action:{actionID})',
	'Delete items'			=> 'Delete items',
	'Delete item(s)'			=> 'Delete item(s)',
	'Cancel'			=> 'Cancel',
	'Not been sent the required arguments ({arguments}).'			=> 'Not been sent the required arguments ({arguments}).',
	'Too many arguments'			=> 'Too many arguments',
	'The action "{action}" was unnecessary argument "{argument}".'			=> 'The action "{action}" was unnecessary argument "{argument}".',
	'The action "{action}" was unnecessary arguments.'			=> 'The action "{action}" was unnecessary arguments.',
);