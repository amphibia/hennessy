<?php

return array(
	'Translation management'			=> 'Переводы интерфейсов',
	'ActiveTable'			=> 'Виджет "ActiveTable"',
	'users'			=> 'Пользовательская система',
	'AdminTable'			=> 'Административная таблица',
	'UploadsFormElement'			=> 'Виджет "UploadsFormElement"',
	'translate'			=> 'переводы',
	'actions'			=> 'действия контроллеров',
	'admin'			=> 'административная система',
	'app'			=> 'приложение (глобальные лексемы)',
	'attributes'			=> 'атрибуты',
	'mtmselect'			=> 'mtmselect',
	'models'			=> 'models',
	'struct'			=> 'struct',
	'custom'			=> 'custom',
	'forms'			=> 'forms',
	'comment'			=> 'comment',
	'comments'			=> 'comments',
);