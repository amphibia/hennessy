<?php

return array(
	'Source file not found.'			=> 'Источник изображения не найден.',
	'Upload started'			=> 'Закачка началась',
	'Height is too short (must be 10% at least)'			=> 'Height is too short (must be 10% at least)',
	'Horizontal position is out of image area.'			=> 'Horizontal position is out of image area.',
	'Vertical position is out of image area.'			=> 'Vertical position is out of image area.',
	'Element deleted.'			=> 'Element deleted.',
	'Cancel'			=> 'Cancel',
	'Width is too long.'			=> 'Width is too long.',
	'Width is too short (must be 10% at least).'			=> 'Width is too short (must be 10% at least).',
	'Height is too short (must be 10% at least).'			=> 'Height is too short (must be 10% at least).',
	'Upload failed'			=> 'Upload failed',
	'You can`t use more than {number} images'			=> 'You can`t use more than {number} images',
);