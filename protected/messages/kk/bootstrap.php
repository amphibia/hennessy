<?php

return array(
	'Next' => 'Вперед',
	'Previous' => 'Назад',
	'First' => 'В начало',
	'Last' => 'В конец',
);