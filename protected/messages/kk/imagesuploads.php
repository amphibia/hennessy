<?php

return array(
	'Error occured'			=> 'Возникла ошибка',
	'You can`t attach more images for this field'			=> 'К этому полю не может быть прикреплено больше изображений',
	'Attribute for this field must have "{validator}" validator!'			=> 'Attribute for this field must have "{validator}" validator!',
);