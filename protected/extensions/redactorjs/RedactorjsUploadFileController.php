<?php

/**
 * Загрузчик изображений
 *
 * @title {ImageUpload }
 * @desc {Используется в виджете Redactor}
 */

class RedactorjsUploadFileController extends Controller
{
	/**
	 * Название операции, которую использует RBAC авторизация.
	 *
	 * @return mixed Cтрока с названием операции, или массив пар ключ => значение,
	 * в котором ключ соответсвует действию, значение название операции. Если
	 * указанно NULL, контроль RBAC авторизации не применяется.
	 *
	 * @example
	 * return array
	 * (
	 *     "edit"       => "editPost",
	 *     "delete"     => "deletePost",
	 *     "default"    => "controllerAction",
	 *     "error"      => NULL,
	 * );
	 */
	public function getRbacOperation(){
		return "File upload";
	}

	public function actionIndex()
	{
		//echo "<pre>".print_r($_FILES, true)."</pre>";

		if(isSet($_FILES['file']['type'])){
			$_FILES['file']['type'] = strtolower($_FILES['file']['type']);


			if ($_FILES['file']['type'] == 'image/png'
				|| $_FILES['file']['type'] == 'image/jpg'
				|| $_FILES['file']['type'] == 'image/gif'
				|| $_FILES['file']['type'] == 'image/jpeg'
				|| $_FILES['file']['type'] == 'image/pjpeg')
			{
				$dir = 'Yii::app()->basePath/../uploads/images/';
				$type = explode('.', $_FILES['file']['name']);

				// setting file's mysterious name
				//$filename = md5(date('YmdHis')).'.'.$type[count($type)-1];
				$filename = md5(date('YmdHis')).'.'.Yii::app()->redactorjsUploadFile->getType($_FILES['file']['type']);
				$file = $dir.$filename;

				// copying
				move_uploaded_file($_FILES['file']['tmp_name'], $file);

				/*
                // displaying file
                $array = array(
                    'url' => 'uploads/'.$filename,
                    'id' => 123
                );

                echo stripslashes(json_encode($array));
                */

				echo "<img src='/uploads/images/".$filename."'/>";
			}
			elseif($_FILES['file']['type'] == 'application/zip'
				|| $_FILES['file']['type'] == 'application/x-rar-compressed'
				|| $_FILES['file']['type'] == 'application/octet-stream'
				|| $_FILES['file']['type'] == 'application/pdf'
				|| $_FILES['file']['type'] == 'image/vnd.adobe.photoshop'
				|| $_FILES['file']['type'] == 'application/msword'
				|| $_FILES['file']['type'] == 'application/rtf'
			){
				$dir = 'Yii::app()->basePath/../uploads/files/';
				$type = explode('.', $_FILES['file']['name']);

				// setting file's mysterious name
				$filename = md5(date('YmdHis')).'.'.Yii::app()->redactorjsUploadFile->getType($_FILES['file']['type']);
				$file = $dir.$filename;

				// copying
				move_uploaded_file($_FILES['file']['tmp_name'], $file);

				echo "<a href='/uploads/files/".$filename."'><img src='/uploads/download-icon.png'/> ".Yii::t('custom', 'Скачать')."</a>";
			}
		}

	}


	

}

?>
