/**
 * Клиентское обслуживание виджета InlineCalendar.
 * Реализует AJAX-перелистывание календаря.
 * Вызывает следующие события:
 *	- onAfterCalandarAjax (привязывается к document)
 */
$(document).ready(function()
{
	// листание месяцев
	$('.inlineCalendar .monthChange[href][data-ajax-url]').live('click', function(){
		$(this).closest('.inlineCalendar')
			.request($(this).attr('data-ajax-url'), {}, null,
			function(){
				$(document).trigger({
					type:"onAfterCalandarAjax",
					url:$(this).attr('data-ajax-url'),
					oldElement: this
				});		
			}
		);
		return false;
	});

	// смена месяца/года в списках выбора
	$('.inlineCalendar .calendarDropdown').live('change', function(){
		$(this).closest('.inlineCalendar').request($(this).val(), {}, null,
			function(){
				$(document).trigger({
					type:"onAfterCalandarAjax",
					url:$(this).attr('data-ajax-url'),
					oldElement: this
				});		
			}
		);
	});
});