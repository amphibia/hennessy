<?php

/**
 * @author koshevy
 *
 * Виджет встроенного календаря, с возможностью указывать используемые года,
 * месяцы, дни.
 * 
 * Для образовывания URL может идти двумя путями: добавление параметров к
 * "route" в естественном порядке массива $params, либо использование createUrl()
 * у контроллера $workController с использованием $params.
 *
 */
class InlineCalendarWidget extends CWidget
{
	/**
	 * @var ID виджета
	 */
	public $id = NULL;
	
	/**
	 * Указанный год (по умолчанию - текущий)
	 * @var integer
	 */
	public $year = NULL;

	/**
	 * Указанный месяц (по умолчанию - текущий).
	 * @var integer
	 */
	public $month = NULL;

	/**
	 * Указанный день (по умолчанию - текущий)
	 * @var integer
	 */
	public $day  = NULL;

	/**
	 * Заголовки таблицы календаря.
	 * @var array
	 */
	public $headings = array();

	/**
	 * Подпись к ссылке "назад".
	 * @var string
	 */
	public $prevLabel = NULL;

	/**
	 * Подпись к ссылке "вперед".
	 * @var string
	 */
	public $nextLabel = NULL;

	/**
	 * Активные дни.
	 * @var array
	 */
	public $acceptedDays = NULL;

	/**
	 * Активные месяцы.
	 * @var array
	 */
	public $acceptedMonths = NULL;

	/**
	 * Активные года.
	 * @var array
	 */
	public $acceptedYears = NULL;


	/**
	 * Контроллер, с которым работает виджет.
	 * @var Controller 
	 */
	public $workController = NULL;

	/**
	 * Роут для формирования URL в календаре.
	 * @var string
	 */
	public $route = NULL;

	/**
	 * Роут для обращения по AJAX.
	 * @var string
	 */
	public $ajaxRoute;

	/**
	 * Параметры, с которыми формируется URL.
	 * @var array
	 */
	public $params = array();

	/**
	 * HTML-настройки, соответствующие различным элементам календаря:
	 * 'calendarHeader' - блок с заголовками календаря
	 * 'prevMonth' - "кнопка предыдущий месяц"
	 * 'nextMonth' - "кнопка предыдущий месяц"
	 * 
	 * 'monthSelect' - элемент выбора месяца
	 * 'yearSelect' - элемент выбора года
	 * @var array
	 */
	public $elementsHtmlOptions = array();

	/**
	 * @var array
	 */
	public $assets = array(
		'css' => array('calendar.css'),
		'js' => array('calendar.js')
	);

	/**
	 * Предыдущий месяц.
	 * @var integer
	 */
	protected $_prevMonth;

	/**
	 * Следующий месяц.
	 * @var integer
	 */
	protected $_nextMonth;

	/**
	 * Год предыдущего месяца.
	 * @var integer
	 */
	protected $_prevYear;

	/**
	 * Год следующего месяца.
	 * @var integer
	 */
	protected $_nextYear;

	/**
	 * Шаблон ссылок  на день (day, month, year).
	 * @var array
	 */
	protected $_urlTemlates = array();

	/**
	 * UNIX-TIME текущего дня.
	 * @var integer
	 */
	protected $_currentDayTmestamp = NULL;



	function init()
	{
		if(!$this->month)
			$this->month = ($this->acceptedMonths && sizeof($this->acceptedMonths))
				? current($this->acceptedMonths) : date('m');

		//if(!$this->day)
		//	$this->day = ($this->acceptedDays && sizeof($this->acceptedDays))
		//		? current($this->acceptedDays) : date('d');

		$this->year = intval($this->year ? $this->year : date("Y"));
		$this->month = intval($this->month);
		$this->day = intval($this->day);

		if(!$this->acceptedDays) $this->acceptedDays = array($this->day);
		if(!$this->acceptedMonths) $this->acceptedMonths = array($this->month);

		if($this->acceptedYears){ sort($this->acceptedYears); $this->acceptedYears = array_values($this->acceptedYears); }
		if($this->acceptedMonths){ sort($this->acceptedMonths); $this->acceptedMonths = array_values($this->acceptedMonths); }
		//if($this->acceptedDays){ sort($this->acceptedDays); $this->acceptedDays = array_values($this->acceptedDays); }

		static $objectsCount = 0;
		if(!$this->id) $this->id = get_class($this)."_".($objectsCount++);

		// UNIX TIME на начало сегоднящнего дня
		$this->_currentDayTmestamp = mktime(0, 0, 0);

		// загрузка конфига по умолчанию
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

		$this->_nextMonth = ($this->month != 12 ? $this->month + 1 : 1);
		$this->_prevMonth = ($this->month != 1 ? $this->month - 1 : 12);
		$this->_prevYear = ($this->month != 1 ? $this->year : $this->year - 1);
		$this->_nextYear = ($this->month != 12 ? $this->year : $this->year + 1);

		// роут по умолчанию
		if(!$this->route) $this->route = $this->_defaultRoute();

		$this->_initTemplates();

		return parent::init();
	}

	public function run(){ $this->render("main"); }


	/**
	 * Инцициализация шаблонов для формирования URL.
	 */
	protected function _initTemplates()
	{
		$params = $this->params;

		$params['day'] = '{day}';
		$this->_urlTemlates['day'] = $this->_createUrl($params);
		$this->_urlTemlates['ajaxDay'] = $this->ajaxRoute
			? $this->_createUrl($params, $this->ajaxRoute) : NULL;
	
		unset($params['day']); $params['month'] = '{month}';
		$this->_urlTemlates['month'] = $this->_createUrl($params);
		$this->_urlTemlates['ajaxMonth'] = $this->ajaxRoute
			? $this->_createUrl($params, $this->ajaxRoute) : NULL;

		unset($params['month']); $params['year'] = '{year}';
		$this->_urlTemlates['year'] = $this->_createUrl($params);
		$this->_urlTemlates['ajaxYear'] = $this->ajaxRoute
			? $this->_createUrl($params, $this->ajaxRoute) : NULL;
	}

	/**
	 * Создание URL с учетом настроек виджета.
	 * @param array $params парамеры
	 * @param string $route роут (если указан $this->workController) или начало URL
	 * @return string
	 */
	protected function _createUrl($params, $route = NULL)
	{
		foreach(array('day', 'month', 'year') as $key){
			if(isset($params[$key]) && $params[$key] && is_numeric($params[$key]))
				$params[$key] = sprintf("%02d", $params[$key]);
		}

		if(!$route) $route = $this->route;
		if($this->workController)
			 return $this->workController->createUrl($route, $params);
		else return $route.'/'.implode('/', $params);
	}

	/**
	 * Роут по умолчанию для формирования ссылок.
	 * @return string
	 */
	protected function _defaultRoute()
	{
		// использование компонента STRUCT
		if(Yii::app()->hasComponent('struct'))
		{
			if($page = Yii::app()->struct->currentPage)
				return $page->createUrl();
			else if($category = Yii::app()->struct->currentCategory)
				return $category->createUrl();
		}

		// испольузется активный контроллер
		if(Yii::app()->controller){
			$this->workController = Yii::app()->controller;
			return Yii::app()->controller->lastActionID;
		}
		else return NULL;
	}

	/**
	 * Обращение к настройкам элемента
	 * @param string $key
	 * @return array
	 */
	protected function _elementHtmlOptions($key){
		if(isset($this->elementsHtmlOptions[$key]))
			 $options = $this->elementsHtmlOptions[$key];
		else $options = array();

		return $options;
	}

	/**
	 * Формирование ссылки на предыдущий месяц.
	 * @return string
	 */
	protected function _prevMonthLink()
	{
		$year = $this->year;
		$month = $this->month;

		if(($monthPosition = array_search($month, $this->acceptedMonths)) === 0)
		{
			$year--;
			$params = compact('year');
		}
		else
		{
			$monthPosition--;
			$month = $this->acceptedMonths[$monthPosition];
			$params = compact('month', 'year');
		}

		return $this->_dateLink(
			$this->prevLabel, $params,
			$this->_elementHtmlOptions('prevMonth')
		);
	}

	/**
	 * Формирование ссылки на следующий месяц.
	 * @return string
	 */
	protected function _nextMonthLink()
	{
		$year = $this->year;
		$month = $this->month;

		if(($monthPosition = array_search($month, $this->acceptedMonths)) == (sizeof($this->acceptedMonths)-1))
		{
			$year++;
			$params = compact('year');
		}
		else
		{
			$monthPosition++;
			$month = $this->acceptedMonths[$monthPosition];
			$params = compact('month', 'year');
		}

		return $this->_dateLink(
			$this->nextLabel, $params,
			$this->_elementHtmlOptions('nextMonth')
		);
	}

	/**
	 * Вывод списка выбора месяца.
	 */
	protected function _monthsList()
	{
		if(!$this->acceptedMonths) return NULL;

		$items = array();
		$selectValue = NULL;
		for($month = 1; $month <= 12; $month++)
		{
			// этого месяца нет в списке используемых месяцев
			if($this->acceptedMonths && !in_array($month, $this->acceptedMonths))
				continue;

			$monthName = date('F', mktime(0, 0, 0, $month));
			$monthUrl = str_replace(
				"{month}", sprintf("%02d", $month),
				$this->_urlTemlates['ajaxMonth']
					? $this->_urlTemlates['ajaxMonth'] : $this->_urlTemlates['month']
			);

			$items[$monthUrl] = Yii::t('calendar', $monthName);
			if($month == $this->month) $selectValue = $monthUrl;
		}

		$htmlOptions = $this->_elementHtmlOptions('monthSelect');
		if(sizeof($items) <= 1) $htmlOptions['disabled'] = 'disabled';
		return CHtml::dropDownList('monthSelect', $selectValue, $items, $htmlOptions);
	}

	/**
	 * Вывод списка выбора года.
	 */
	protected function _yearsList()
	{
		if(!$this->acceptedYears) return NULL;

		$items = array();
		$selectValue = NULL;
		foreach($this->acceptedYears as $year)
		{
			$yearUrl = str_replace(
				"{year}", sprintf("%02d", $year),
				$this->_urlTemlates['ajaxYear']
					? $this->_urlTemlates['ajaxYear'] : $this->_urlTemlates['year']
			);
			
			$items[$yearUrl] = $year;
			if($year == $this->year) $selectValue = $yearUrl;
		}

		$htmlOptions = $this->_elementHtmlOptions('yearSelect');
		if(sizeof($items) <= 1) $htmlOptions['disabled'] = 'disabled';
		return CHtml::dropDownList('yearSelect', $selectValue, $items, $htmlOptions);
	}

	/**
	 * Формирование ссылки с датой.
	 * Подставляет значение к готовому шаблону URL (чтобы не тратиться
	 * на затратный createUrl() и проверяет, есть ли эта дата в списке используемых
	 * дат, если нет - делает ссылку не активной).
	 * 
	 * @param string $title
	 * @param array $params Параметры для замены в шаблоне (год, [месяц], [день])
	 * @param array $htmlOptions
	 * 
	 * @return string
	 */
	protected function _dateLink($title, $params = array(), $htmlOptions = array())
	{
		if(!isset($htmlOptions['class'])) $htmlOptions['class'] = NULL;
		$dateTimestamp = mktime(0, 0, 0,
			isset($params['month']) ? $params['month'] : 0,
			isset($params['day']) ? $params['day'] : 0,
			$params['year']
		);

		if($dateTimestamp == $this->_currentDayTmestamp)
			$actualityClass = 'now';
		elseif($dateTimestamp < $this->_currentDayTmestamp)
			$actualityClass = 'past';
		elseif($dateTimestamp > $this->_currentDayTmestamp)
			$actualityClass = 'future';

		$htmlOptions['class'] .= " $actualityClass";

		// проверка, потребуется ли ссылка вообще
		if(	(isset($params['year']) && $this->acceptedYears && !in_array($params['year'], $this->acceptedYears)) ||
			(isset($params['month']) && $this->acceptedMonths && !in_array($params['month'], $this->acceptedMonths)) ||
			(isset($params['day']) && $this->acceptedDays && !in_array($params['day'], $this->acceptedDays)) )
			{
				$urlTemplate = false;
				$htmlOptions['class'] .= ' disabled';
			}

		// если ссылка потребовалась
		if(!isset($urlTemplate))
		{
			if(isset($params['day']))
			{
				$urlTemplate = $this->_urlTemlates['day'];
				$ajaxTemplate = $this->_urlTemlates['ajaxDay'];
			}
			elseif(isset($params['month']))
			{
				$urlTemplate = $this->_urlTemlates['month'];
				$ajaxTemplate = $this->_urlTemlates['ajaxMonth'];
			}
			else
			{
				$urlTemplate = $this->_urlTemlates['year'];
				$ajaxTemplate = $this->_urlTemlates['ajaxYear'];
			}
		}

		// подстановка параметров
		foreach($params as $key => $value){
			$urlTemplate = str_replace("{".$key."}", sprintf("%02d", $value), $urlTemplate);
			if(isset($ajaxTemplate) && $ajaxTemplate)
				$ajaxTemplate = str_replace("{".$key."}", sprintf("%02d", $value), $ajaxTemplate);;
		}

		if(isset($ajaxTemplate) && $ajaxTemplate)
			$htmlOptions['data-ajax-url'] = $ajaxTemplate;

		return CHtml::link($title, $urlTemplate?$urlTemplate:NULL, $htmlOptions);
	}

}
?>
