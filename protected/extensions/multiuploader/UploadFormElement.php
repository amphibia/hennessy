<?php

/**
 * @package imagesuploader
 * @author koshevy
 * 
 * @desc
 * Виджет множественной закачки.
 * Использует компонент @see ImagesUploadComponent. Для связи с сервером, выводимый
 * элементу управления использует контроллер @see ImagesUploadController.
 * 
 * Также, в комплекте с виджетом идёт вадидатор @see ImageValidator, который проверяет
 * корректность отправляемых файлов, перемещает файлы из временной директории в
 * постоянную после успешного сохранения модели.
 * 
 * К виджету прикреплён JQuery-плагин (@see iploadsFormElement.js), реализующий
 * логику на стороне клиента.
 * 
 * @example
 * Чтобы использовать виджет, для начала подключите компонент @see ImagesUploadComponent:
 * .
 * .
 * 'components' => array(
 *      'imagesupload'=>Yii::localExtension('imagesupload', 'ImagesUploadComponent')
 * ),
 * .
 * .
 * Если Вы собираетесь использовать другой ID для компонента, измените свойство
 * UploadFormElement::$uploadsComponentID соответствующим образом.
 * 
 * При инициализации, компонент сам подключит необходимый для работы контроллер.
 * Далее, нужно привязать виджет к конкретному полю в форме:
 * 
 * .
 * .
 * 'elements' => array(
 *      'title'=>array('type'=>'default'),
 *      'images' => array('type' => 'ext.imagesuploader.UploadFormElement'),
 *      ['uploadsComponentID'=><ID компонента>] // по умолчанию: "imagesupload"
 *
 * ),
 * .
 * .
 * 
 * После этого, привяжите валидатор @see ImageValidator к этому полю.
 * В валидаторе можно указать свойства 'max' и 'min', которые будут ограничивать
 * и/или наоборот запрашивать необходимое количество закачанных файлов:
 * .
 * .
 * public function rules(){
 *      return array(
 *          array('images', Yii::localExtension('imagesuploader', 'ImageValidator'), 'min'=>1, 'max'=>3),
 *      )
 * }
 * .
 * .
 * 
 * Клиентская часть будет прятать кнопку "закачать", как только количество закачанных
 * файлов в форме достигнет ImageValidator::$max.
 * 
 * Для получения информации о настройках, смотрите файлы конфигурации в расширениях
 * storage и imagesuploader.
 * 
 */
class UploadFormElement extends CInputWidget
{

    /**
     * @var ActiveRecord Модель, с которой завязан этот элемент управления.
     */
	public $model;

    /**
     * @var string Поле в используемой модели, к которому привязан элемент управления. 
     */
    public $attribute;

     /**
     * @var array Финальное значение, которое используется в представлении. 
     */
    public $value;
 
    /**
     * @var array представления, используемые виджетом.
     */
    public $views = array();

    public $assets = array(
        'js' => array('jquery.Jcrop.min.js', 'uploadsFormElement.js', 'uploadRoutine.js'),
        'css' => array('jquery.Jcrop.min.css', 'uploadsFormElement.css')
    );
    
    /**
     * ID компонента управления закачками.
     * @var string
     */
    public $uploadsComponentID = 'upload';

    /**
     * Класс валидатора, к которому идёт привязка из списка валидаторов для
     * аттрибута модели.
     * @var string
     */
    public $validatorClass = 'UploadValidator';
    
    /**
     * Валидатор, к котороому привязывается виджет (берёт информацию о максимальном/
     * минимальном количестве изображений и т.п.)
     * @var UploadValidator
     */
    public $validator = NULL;

	/**
	 * Требуется ли использовать окно с указанием позиции обрезки.
	 * 
	 * @todo В реальности, свойство useCroper не используется. Инструменты
	 * обрезки выводятся в любом случае.
	 * 
	 * @var boolean
	 */
	public $useCroper = true;

    /**
     * Компонент - хранилище изображений.
     * @var StorageComponent
     */
    protected $_storage = NULL;

	/**
	 * Тип файлов, разрешенных для элемента формы.
	 * Определяется в зависимости от настроек валидатора UploadVaidator,
	 * прикрепленного к тому же свойству модели, что и этот элемент формы.
	 *
	 * @var string
	 */
	protected $_fileType = NULL;

	/**
	 * Другая надпись на кнопке загрузки.
	 * @var string
	 */
	public $customUploadLabel;


	/**
	 * Типы файлов, разрешенные
	 * @var array
	 */
	protected $_acceptedMimeTypes = array();

	

	public function init()
	{
        $modelClass = get_class($this->model);
		$this->id = "uploadFile_".$this->attribute;

        Yii::applyAssets($this, __FILE__);
		$this->htmlOptions['id'] = $this->id;
        $this->htmlOptions['type'] = 'file';
        $this->htmlOptions['multiple'] = 'multiple';
        $this->htmlOptions['attribute'] = "{$modelClass}[{$this->attribute}]";
        $this->value = $this->model->{$this->attribute} or $this->value = 'none';

		$this->_storage = Yii::app()->{$this->uploadsComponentID}->storage;
        if(!is_array($this->value)) $this->value = $this->_storage->parseFieldData($this->value);

        $this->views = array_merge(
            Yii::app()->{$this->uploadsComponentID}->informersViews,
            $this->views
        );

        // вывод настроек для клиентской части - AJAX-интерфейсы закачки
        // и вывода информеров
        $controllerID = Yii::app()->{$this->uploadsComponentID}->controllerID;        
        Yii::app()->clientScript->registerScript(get_class($this),
        "jQuery.fn.extend({" .
        "uploadFormElementMessages:{" .
            "error: '".Yii::t('upload', 'Error occured')."'," .
            "limitExceded: '".Yii::t('upload', 'You can`t attach more images for this field')."'," .
            "statusErrors:{".
                "400:'".Yii::t('upload', 'Bad Request')."',".
                "403:'".Yii::t('upload', 'Forbidden')."',".
                "404:'".Yii::t('upload', 'Not Found')."',".
                "405:'".Yii::t('upload', 'Method Not Allowed')."',".
                "406:'".Yii::t('upload', 'Not Acceptable')."',".
                "408:'".Yii::t('upload', 'Request Timeout')."',".
                "410:'".Yii::t('upload', 'Gone')."',".
                "412:'".Yii::t('upload', 'Precondition Failed')."',".
                "413:'".Yii::t('upload', 'Request Entity Too Large. Max size: {size}', array('{size}' => ini_get('upload_max_filesize')))."',".
                "414:'".Yii::t('upload', 'Request-URI Too Large')."',".
                "415:'".Yii::t('upload', 'Unsupported Media Type')."',".
                "423:'".Yii::t('upload', 'Locked')."',".
                "429:'".Yii::t('upload', 'Too Many Requests')."',".
                "431:'".Yii::t('upload', 'Request Header Fields Too Large')."',".
                "434:'".Yii::t('upload', 'Requested host unavailable')."',".
                "456:'".Yii::t('upload', 'Unrecoverable Error')."',".
                "500:'".Yii::t('upload', 'Internal Server Error')."',".
                "503:'".Yii::t('upload', 'Service Unavailable')."',".
                "505:'".Yii::t('upload', 'HTTP Version Not Supported')."',".
                "507:'".Yii::t('upload', 'Insufficient Storage')."',".
                "508:'".Yii::t('upload', 'Loop Detected')."',".
                "509:'".Yii::t('upload', 'Bandwidth Limit Exceeded')."',".
            "}".
        "}," .
        "uploadServerIntefaces:{".
            "upload: '/{$controllerID}',".
            "informerUploading: '/{$controllerID}/informers/start',".
            "informerSuccess: '/{$controllerID}/informers/success',".
            "informerFail: '/{$controllerID}/informers/fail'".
        "}});", CClientScript::POS_READY);

        return true;
	}

    public function _filesList()
    {
        $fileList = NULL;
        if(is_array($this->value))
        foreach($this->value as $fileData)
        {
			$storageDir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR .
				$this->_storage->storageDir;

            try 
            {
				$extractedFileName = $this->_storage->parseOptionsFileName($fileData['fileName']);

                if(!$extractedFileName || $extractedFileName=='none') continue;
                $fileList .= $this->render($this->views["success"],
                    array
					(
						"title" => $fileData['title'],
						"description" => $fileData['description'],
						"compressedFileName" => $extractedFileName,
                        "storage" => $this->_storage,
                        "newFileName" => $extractedFileName,
                        "fileId" => 0,
                        "fileName" => 'http://'.$_SERVER['HTTP_HOST'].$this->_storage->createUrl($extractedFileName, 'original'),
                        "fileSize" => NULL,
                        "attribute" => get_class($this->model)."[{$this->attribute}]"
                    ),
                    true
                );
                $fileList .= "\n\n";
            }
            catch(Exception $err){ continue; }
        }
        
        return $fileList;
    }

	public function run()
	{
        // поиск валидатора
        foreach($this->model->getValidators($this->attribute) as $validator)
            if(get_class($validator) == $this->validatorClass){
                $this->validator = $validator; break; }

		if($this->validator)
		{
			$this->htmlOptions['min'] = $this->validator->min;
			$this->htmlOptions['max'] = $this->validator->max;
			$this->htmlOptions['differentFileTypes'] = $this->validator->differentFileTypes;

			$this->_fileType = $this->validator->fileType;

			// определение разрещенных-MIME по типу закачиваемых файлов
			switch($this->_fileType){
				case UploadValidator::TYPE_DOCUMENT:
					$this->_acceptedMimeTypes = Yii::app()->{$this->uploadsComponentID}
						->allowedDocumentMime;
					break;
				case UploadValidator::TYPE_IMAGE:
					$this->_acceptedMimeTypes = Yii::app()->{$this->uploadsComponentID}
						->allowedImageMime;
					break;
				
				default:

					$this->_acceptedMimeTypes = array_merge(
						Yii::app()->{$this->uploadsComponentID}->allowedDocumentMime,
						Yii::app()->{$this->uploadsComponentID}->allowedImageMime
					);
			}
		}

		// виджет может работать только в паре с валидатором, который выступает
		// в качестве преконвертора
		else
		{
			$errorMessage = Yii::t('upload',
				'Attribute for this field must have "{validator}" validator!',
				array("{validator}" => $this->validatorClass)
			);
			
			echo CHtml::tag('div', array('class'=>'alert error'), $errorMessage);

			return;
		}

		// Передача параметра (MIME - типы, разрешенные для закачки через этот элемент)
		// на сторону сервера.
		Yii::app()->clientScript->registerScript("{$this->id}_MIME_TYPES",
			"$('#{$this->id}').data('MIME_TYPES', ".json_encode($this->_acceptedMimeTypes).");" .
			"$('#{$this->id}').data('MIME_TYPE_ERROR', ".json_encode(Yii::app()->{$this->uploadsComponentID}->mimeErrorMessage).");" .
			"$('#{$this->id}').data('DUPLICATE_TYPE_ERROR', ".json_encode(Yii::t('upload', 'The specified file type is already loaded.')).");",
			CClientScript::POS_READY
		);

        if(Yii::app()->hasComponent($this->uploadsComponentID))
             $this->render($this->views['main'], array("fileList" => $this->_filesList()));
        else $this->render($this->views['disabled']);
	}

}
?>