<?php

/**
 * @package imagesuploader
 * @author koshevy
 * 
 * Валидатор для проверки полей модели, к которым привязывается UploadFormElement.
 * Необходимо использовать их в паре.
 * 
 * Помимо самих изображений, валидатор обрабатывает настройки изображения
 * (кадрирование), которые присылаются в $_POST['UploadsFormElement'][md5(FILENAME)].
 */
class UploadValidator extends CValidator
{
	const TYPE_FILE = 0;
	const TYPE_IMAGE = 1;
	const TYPE_DOCUMENT = 2;

    public $skipOnError = true;

	/**
	 * Свойство, указывающее, что все файлы должны быть разных типов.
	 * Применяется, если поле должно хранить альернативные версии одного файла.
	 * @var boolean
	 */
	public $differentFileTypes = false;

	/**
	 * Тип файлов (UploadValidator::TYPE_DOCUMENT, UploadValidator::TYPE_IMAGE
	 * или UploadValidator::TYPE_FILE) - т.е. изображения, документы или и то и
	 * другое. По умолчанию - UploadValidator::TYPE_FILE.
	 * @var integer
	 */
	public $fileType = UploadValidator::TYPE_FILE;
	
    /**
     * ID компонента управления закачками (на случай, если компонент будет
     * добавлен под другим ID).
     * @var type 
     */
    public $uploadsComponentID = 'upload';

    /**
     * Минимальное количество изображений.
     * @var int
     */
    public $min = NULL;

    /**
     * Максимальное количество изображений.
     * @var int
     */
    public $max = NULL;

    
    /**
     * @todo: В дальнейшем, могут быть добавлены фильтры ширины и/или высоты,
     * уникальности и т.п.
     */


    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object, $attribute)
    {
        $validator = &$this;

		// минимальное/максимальное количество изображений
		if($validator->min && (sizeof($object->$attribute)<$validator->min))
			$object->addError($attribute, Yii::t('upload',
				'The attachment at least {number} images required',
					array('{number}'=>$validator->min)));

		if($validator->max && (sizeof($object->$attribute)>$validator->max))
			$object->addError($attribute, Yii::t('upload',
				'You can`t use more than {number} images',
					array('{number}'=>$validator->max)));

        // После успешного сохранения, файлы будут перемещены
        // в постоянное хранилище.
		// Чтобы не копировать файл до того, как будет закончена валидация по
		// остальным атрибутам, проверка этогопарамтера откладывается на самый
		// конец - на время окончания остальных проверок.
        $object->onAfterValidate = function(CEvent $event)
        use($object, $attribute, $validator)
        {
			$uploadComponent = Yii::app()->{$validator->uploadsComponentID};

			// Чтобы не было проблемы "невозможно удалить единственный файл".
			if(isset($_POST[get_class($object)]) && !isset($_POST[get_class($object)][$attribute]))
				$object->$attribute = array();

			// Получение массива данных с общим форматом.
			$object->$attribute = $uploadComponent->storage->parseFieldData($object->$attribute);

			if(!sizeof($object->errors))
			{
				$usedExtensions = array();
				$value = array();
				$attributeValue = $object->$attribute; // рабочая копия

				foreach($object->$attribute as $index => $fileData)
				{
					$fileUri = $uploadComponent->storage->parseOptionsFileName($fileData['fileName']);
					if($validator->differentFileTypes)
					{
						$pathinfo = pathinfo($fileUri);
						$extension = $pathinfo['extension'];
						if(in_array($extension, $usedExtensions)){
							$object->addError($attribute, Yii::t('upload',
								'File types must be different!'));
						}

						else $usedExtensions[] = $extension;
					}

					$optionsKey = md5($fileUri);

                    // Проверка URI изображения и перемещение из временной
                    // директории, если требуется.
					try
					{
						/**
						 * @todo: Если не получилось скопировать, или фала нет,
						 * выводить сообщение, но не стирать значение!
						 */

						// Попытка скопировать файл из временной директории.
						// Если файл находится уже в постоянной директории,
						// возвращвется исходное значение.
						if($fileUri = $uploadComponent->_moveFromTemp($fileUri))
							 $attributeValue[$index]['fileName'] = $fileUri;
						
						/** @todo:
						 * Реализовать повторную проверку MIME при сохранении.
						 */

						if(isset($_POST['UploadsFormElement'])
						&& isset($_POST['UploadsFormElement'][$optionsKey]))
						{							
							$options = $_POST['UploadsFormElement'][$optionsKey];
							// проверка свойств, которые приходят c изображениями
							$attributeValue[$index] = $validator->_imageOptionsCheck(
								$optionsKey, $object, $options);

							$attributeValue[$index] = $uploadComponent->storage
								->encodeFileData($fileUri, $options);
						}
					}
					catch(Exception $err){
						$object->addError($attribute, $err->getMessage());
                    }
                }

				$object->$attribute = $attributeValue;
            }

            if(is_array($object->$attribute))
                $object->$attribute = json_encode($object->$attribute);
        };
    }

	/**
	 * Проверка свойств изображения. Не вызывайте для этого метода - для
	 * внутреннего пользования.
	 *
	 * @param string $optionsKey ключ в массиве с настройками
	 * @param CModel $object
	 * @param array $options настройки для изображения
	 */
	public function _imageOptionsCheck($optionsKey, &$object, &$options)
	{
		foreach($options as $optKey => $optVal){
			if(in_array($optKey, array('title', 'description')))
				continue;

			$options[$optKey] = intval($optVal);
		}

		// Проверка присланных с изображением координат.
		if(!isset($options['x']) || !$options['x']) $options['x'] = 0;
		if(!isset($options['y']) || !$options['y']) $options['y'] = 0;
		if(!isset($options['width']) || !$options['width']) $options['width'] = 100;
		if(!isset($options['height']) || !$options['height']) $options['height'] = 100;

		// проверки допустимости параметров
		if($options['width'] < 10)
			$object->addError($attribute, Yii::t('upload',
				'Width is too short (must be 10% at least).'));
		if($options['height'] < 10)
			$object->addError($attribute, Yii::t('upload',
				'Height is too short (must be 10% at least).'));

		// максимальная ширина и высота
		if($options['width'] > 1000)
			$object->addError($attribute, Yii::t('upload', 'Width is too long.'));
		if($options['height'] > 1000)
			$object->addError($attribute, Yii::t('upload', 'Height is too long.'));

		if($options['x'] > 90)
			$object->addError($attribute, Yii::t('upload',
				'Horizontal position is out of image area.'));
		if($options['y'] > 90)
			$object->addError($attribute, Yii::t('upload',
				'Vertical position is out of image area.'));

		if(($options['x'] + $options['width']) < 10)
			$object->addError($attribute, Yii::t('upload',
				'Horizontal position is out of image area.'));
		if(($options['y'] + $options['height']) < 10)
			$object->addError($attribute, Yii::t('upload',
				'Vertical position is out of image area.'));
	}

}
