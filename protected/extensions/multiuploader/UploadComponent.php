<?php

Yii::import('application.extensions.image.Image');

/**
 * @title Multiupload package component
 * @description { Component for images upload (add upload controllers to system,
 * allow using UploadFormElement widgets). Component has functions for single
 * file upload (multi-files upload implements by meny single AJAX-requests). }
 * @preload yes
 * @vendor koshevy
 * @package multiuploader
 * @requirements{component:image, component:storage}
 * 
 * Компонент, содержащий настройки и функционал, связанные с закачкой на сервер.
 * Используется виджетом @see UploadsFormElement и контроллером @see ImagesUploadController.
 * 
 * Конфиг по умолчанию находится в @see StorageOptions.
 * 
 * Напрямую зависим от компонента storage.
 * 
 * Если потребуется вынести хранилище на другой хост (или на несколько других
 * хостов) - переопределите методы ImagesUploadComponent::_copyFileXHR(),
 * ImagesUploadComponent::_selectStorage() и ImagesUploadComponent::_moveFromTemp().
 * 
 */
class UploadComponent extends ExtCoreApplicationComponent
{
    /**
     * Разрешённые MIME-типы документов (используется, если включено расширение
	 * "Fileinfo").
     * @var array
     */
    public $allowedDocumentMime = array();

    /**
     * Разрешённые MIME-типы изображений (используется, если включено расширение
	 * "Fileinfo").
     * @var array
     */
    public $allowedImageMime = array();
	
    /**
     * Максимальная длина закачиваемого файла.
     * @var integer
     */
    public $maxFileNameLentgh = NULL;

    /**
     * Представления, используемые для создания информеров закачки.
     * @var array
     */
    public $views = array();

    /**
     * Атрибуты INPUT[FILE]-элемента, используемого виджетом закачки.
     * @var array
     */
    public $formElementAttributes = array();

    /**
     * ID контроллера, обрабатывающего закачки.
     * @var string
     */
    public $controllerID = NULL;

    /**
     * ID компонента - хранилища.
     * @var string
     */
    public $storageComponentID = NULL;

    /**
     * Компонент-хранилище, в которое происходит закачка.
     * @var type 
     */
    public $storage = NULL;
    
    /**
     * Функция переименовывания файла при загрузке.
     * @var callback($fileName)
     * @return string новое имя 
     */
    public $fileRenameFunction = NULL;

    /**
     * @var array Представления для информеров.
     */
    public $informersViews = array();

    
    /**
     * Массив с перечислениями хостов, разрешённых для использования при закачке;
     * также, хост в $_POST['filename'] (для информера "success" - после закачки)
     * должен находится в этом массиве.
     * @var array
     */
    public $acceptedHosts = array();

    /**
     * Информация об имени исходного файла.
     * @var array
     */
    public $srcFileInfo = NULL;

    /**
     * Новое имя файла.
     * @var string
     */
    public $newFileName = NULL;
    
    /**
     * Директория, в которую будет положен файл.
     * @var string
     */
    public $fileDirecory = NULL;

    /**
     * Путь к скопированному файлу.
     * @var type 
     */
    public $filePath = NULL;

    /**
     * Адрес используемого в данном случае хранилища изображений.
     * @var string
     */
    public $storageHost = NULL;

    /**
     * Корневая директория хранилища, использованного для данной закачки. 
     * @return string
     */
    public $storageRoot = NULL;

	/**
	 * Сообщение об ошибке MIME. Будет использоваться и на клиентской стороне.
	 * @var string
	 */
	public $mimeErrorMessage = NULL;



    public function init()
	{
		Yii::loadExtConfig($this, __FILE__, 'UploadOptions.php');
		Yii::app()->controllerMap[$this->controllerID] = array(
			'class' => Yii::localExtension(basename(__DIR__), 'UploadController'),
			'uploadComponent' => &$this
        );

        // ссылка на хранилище
        $this->storage = &Yii::app()->{$this->storageComponentID};

        return true;
    }
 

    /**
     * Принятие файла, отправленного XMLHttpRequest.
     * @return boolean
     */
    public function loadFromXHR()
    {
        // получение и проверка информации о файле
        if(!$this->_parseFileNameXHR()) return false;
        $this->_fileBeforeFilter();

        $this->fileDirecory = "{$this->storage->storageDir}" . DIRECTORY_SEPARATOR .
            "_temp" . DIRECTORY_SEPARATOR . $this->storage->context;

        // создание директории, если требуется
        if(!file_exists("{$this->storageRoot}{$this->fileDirecory}")){
            try{ if(!mkdir("{$this->storageRoot}{$this->fileDirecory}", 0777, true)) throw new Exception; }
            catch(Exception $err){
                throw new CHttpException(507, Yii::t('upload', 'Cant create storage directory.'));
            }
        }

        $this->_copyFileXHR();
        $this->_fileAfterFilter();

        return true;
    }

    /**
     * Сохранение файла, полученного XHR-запросом.
     * Если требуется разбивка файлового хранилища на разные хосты,
     * эта функция должна быть переопределена - в реализации класса
     * ImagesUploadComponent она работает только с локальным
     * хранилищем.
     */
    public function _copyFileXHR()
    {
        // полный путь до файла
        $this->newFileName = $this->_sysname($this->srcFileInfo['basename']);
        $this->filePath = $this->fileDirecory . DIRECTORY_SEPARATOR . $this->newFileName;

        // проверка сущестования такого файла
        if(file_exists("{$this->storageRoot}{$this->filePath}"))
            throw new CHttpException(409, Yii::t('upload', Yii::t('upload', 'Same file already exists.')));

        $fileInput = fopen("php://input", "r");
        $fileDest = fopen("{$this->storageRoot}{$this->filePath}", "w");
        $completeSize = stream_copy_to_stream($fileInput, $fileDest);
        fclose($fileInput);
        fclose($fileDest);

        // файл скопирован до конца?
        if($completeSize != $_SERVER["CONTENT_LENGTH"])
            throw new CHttpException(507, Yii::t('upload', "File copying is not complete."));
    }

    /**
     * Перемещение файла из временной директории в основную.
     * Если требуется разбивка хранилища на разные хосты, эту функцию нужно
     * переопределить.
     * @param type $fileUri
     * @return type 
     */
    public function _moveFromTemp($fileUri)
    {
        // извлечение параметров
        foreach($this->storage->urlParseRules as $rule)
		{
            $rule = str_replace('STORAGE_DIR', $this->storage->storageDir, $rule);
            $rule = str_replace('/', "\\".DIRECTORY_SEPARATOR, $rule);

            if(preg_match($rule, $fileUri, $matches))
			{
				$tmpDir = $matches['tmpDir'] ? "/{$matches['tmpDir']}" : NULL;
				if(!$tmpDir) return $fileUri; // происходит только для временных файлов
				$actualContext = $this->storage->getContext();

				if($matches['host'] && !in_array($matches['host'], array_merge($this->acceptedHosts, $this->storage->hostAliases)))
					throw new ExtCoreFishyError(400, Yii::t('upload', "Sended file from unknown host."));

				// создание директории, если требуется
				if(!file_exists("{$_SERVER['DOCUMENT_ROOT']}{$matches['storageDir']}/{$actualContext}")){
					try{ if(!mkdir("{$_SERVER['DOCUMENT_ROOT']}{$matches['storageDir']}/{$actualContext}", 0777, true))
						throw new Exception;
					}
					catch(Exception $err){
						throw new CHttpException(507, Yii::t('upload', 'Cant create storage directory.'));
					}
				}

                $oldPath = "{$_SERVER['DOCUMENT_ROOT']}{$matches['storageDir']}{$tmpDir}/{$matches['context']}/{$matches['filename']}";
                $newPath = "{$_SERVER['DOCUMENT_ROOT']}{$matches['storageDir']}/{$actualContext}/{$matches['filename']}";

                if(!is_file($oldPath)) return $oldPath;
                if(!rename($oldPath, $newPath)) throw new CException(
                        Yii::t('imagesuploader', 'Sorry, we have some problems with save file. May be disk is full. Try again later.'));

                return "{$matches['host']}{$matches['storageDir']}/{$actualContext}/{$matches['filename']}";
            }
        }

        return $fileUri;
    }

    /**
     * Приёмка файла, отправленного со скрытых форм.
     * @return boolean
     */
    public function loadFromForm(){
        /** @todo: сделать приёмку файлов с форм (для opera и IE) */
        return false;
    }


    /**
     * Выбор хранилища (например, если используется многосерверная система).
     * По умолчанию - функция пустышка. Требуется переопределение.
     */
    protected function _selectStorage(){
    }
    
    /**
     * Транслитерация имени файла (действие переименовывания реализуется в
     * отложенном вызове - @see $fileRenameFunction).
     * @param string $fileName исходное имя файла
     * @return string новое имя файла
     */
    protected function _sysname($fileName){
        if($this->fileRenameFunction)
            return call_user_func($this->fileRenameFunction, $fileName);
        else return $fileName;
    }

    /**
     * Обработка имени файла. Оно должно отправляться с заголовками в HTTP_X_FILE_NAME.
     * Если такой заголовок не был отправлен, возникает 400 ошибка.
     * 
     * После выполнения функции, в контроллере становится доступной информация
     * об имени и типе файла.
     * @return boolean были ли извлечены данные из XHR
     */
    protected function _parseFileNameXHR(){
        // имя файла отправляется с заголовками
        if(array_key_exists("HTTP_X_FILE_NAME", $_SERVER)){
            $this->srcFileInfo = pathinfo($_SERVER["HTTP_X_FILE_NAME"]);
            return true;
        }
        else return false;
    }

    /**
     * Проверка файла перед сохранением (MIME ещё невозможно проверить, т.к.
     * файл по факту ещё никуда не скопирован - проверяется расширение и длина файла.
     * Вызывает исключения, если что-то не так.
     * @return void
     */
    protected function _fileBeforeFilter()
    {
		// Проводится предварительная проверка закачанных файлов - файл
		// должен быть в списке дуопустимых документов или изображений.

        // Расширяем стандартный список Mime-Type
        $magicFile = Yii::getPathOfAlias('application.config.mimeTypes').'.php';
        if(!is_file($magicFile)){
            $magicFile = null;
        }

        // Если включено расширение FileInfo, метод _fileAfterFilter() повторно
        // производит проверку уже скопированного файла по MIME.
        $mimeType = CFileHelper::getMimeTypeByExtension($this->srcFileInfo['basename'], $magicFile);

		if(!in_array($mimeType, array_merge($this->allowedDocumentMime, $this->allowedImageMime)))
			throw new ExtCoreFishyError(400, $this->mimeErrorMessage.$mimeType);

		if(mb_strlen($this->srcFileInfo['basename'], 'utf8') > $this->maxFileNameLentgh)
			throw new ExtCoreFishyError(400, Yii::t('upload', "File name too long."));
    }

    /**
     * Проверка файла после сохранения (MIME).
     * @return void
     */
    protected function _fileAfterFilter()
	{
		$this->srcFileInfo['mimeType'] = CFileHelper::getMimeType(
			"{$this->storageRoot}{$this->filePath}");

		// Проверка, схожая с проводимой в методе _fileBeforeFilter(),
		// но уже исходя из данных файла, а не из расширения
		if(!in_array($this->srcFileInfo['mimeType'],
			array_merge($this->allowedDocumentMime, $this->allowedImageMime))){
			unlink("{$this->storageRoot}{$this->filePath}");
			throw new ExtCoreFishyError(400, $this->mimeErrorMessage);
		}	
    }

	/**
	 * Определяет, является ли файл изображением.
	 * @param string $fileUri
	 * @return boolean
	 */
	public function isImage($fileUri){
		$mimeType = CFileHelper::getMimeTypeByExtension($fileUri);
		if(in_array($mimeType, $this->allowedImageMime))
			 return true;
		else return false;
	}

    
}

?>
