<div class="controlElement imagesUploads" id="<?= $this->id ?>_container">

	<?php
	// заготовка информера "удалено"
	$this->render($this->views['deleted']);
	
	// заготовка окна с настройками (обрезка изображения)
	if(isset($this->useCroper) && $this->useCroper){
		$this->render($this->views['options']);
	}
	?>

    <div class="resultsArea clientContent clientContentAppend"><?= $fileList ?></div>

    <?php
	$uploadLabel = $this->customUploadLabel or $uploadLabel = Yii::t('app', 'Upload');
    $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
        array('buttonType'=>'button', 'type'=>'success', 'icon'=>'icon-upload icon-white', 'label'=>$uploadLabel,
            'htmlOptions'=>array('class'=>'uploadNew'))
    );
    ?>

    <input class="imagesUpload" <?= CHtml::renderAttributes($this->htmlOptions); ?> />

</div>