<div class="alert alert-warning uploadInformer uploadInformerDeleted billet">
	<?= Yii::t('upload', 'Element deleted.'); ?>
	<a href="javascript:;" class="cancel"><?= Yii::t('upload', 'Cancel'); ?></a>
</div>