<?

/**
 * @author koshevy
 *
 * Универсальная форма для обработки заявок с Лендинг-страниц.
 * Обрабатывает все возможные поля для форм заявок. Конкретный список полей указывается в свойстве $requiredFields.
 */
class LandingForm extends CFormModel
{
    /**
     * Заголовок формы
     * @var string
     */
    public $title = NULL;

    /**
     * Описание формы
     * @var string
     */
    public $description = NULL;

    /**
     * Включение режима полей по умолчанию.
     * @var boolean
     */
    public $defaultFields = NULL;

    /**
     * @var string
     */
    public $buttonTitle = NULL;

    /**
     * Эл. адреса, на которые происходит отправка
     * @var string
     */
    public $email = NULL;


    /**
     * Поля для этой формы.
     * @var array
     */
    public $fields = array();


    /**
     * Варианты возможных видов полей.
     * @var array
     */
    public $rulePresets = array();


    /**
     * Применить настройки формы для данной модели.
     * @param $formData
     */
    public function applyFormData($formData)
    {
        unset($formData['theme']);

        foreach($formData as $k => $v){
            try{ $this->$k = $v; }
            catch(CException $err){ continue; }
        }
    }


    public function rules()
    {
        $rules = array();
        foreach($this->fields as $fieldName => $fieldOptions)
        {
            $preset = $this->rulePresets[$fieldOptions['preset']];
            if(isset($preset['validators']))
                foreach($preset['validators'] as $validator)
                {
                    $validatorOptions = array($fieldName, $validator);

                    if( isset($preset['params']) &&
                        isset($preset['params'][$validator]))
                        $rules[] = array_merge(
                            $validatorOptions,
                            $preset['params'][$validator]
                        );

                    if( isset($fieldOptions['required']) &&
                        $fieldOptions['required'])
                    {
                        $rOptions = array($fieldName, 'required');
                        if(isset($fieldOptions['requiredMessage']) && $fieldOptions['requiredMessage'])
                            $rOptions['message'] = $fieldOptions['requiredMessage'];
                        else if(isset($preset['requiredMessage']))
                            $rOptions['message'] = $preset['requiredMessage'];

                        $rules[] = $rOptions;
                    }
                }
        }

        return $rules;
    }

    protected function afterConstruct(){
        parent::afterConstruct();
        Yii::loadExtConfig($this, __FILE__);
    }

    public function relations()
    {
        return array();
    }

    /**
     * @param $attribute
     */
    public function simplePurify($attribute){
        $this->$attribute = htmlspecialchars(
            strip_tags($this->$attribute)
        );
    }


    protected $_fieldsValues = array();

    public function __get($fieldName){
        if(isset($this->fields[$fieldName]))
            return isset($this->_fieldsValues[$fieldName])
                ? $this->_fieldsValues[$fieldName]
                : NULL;

        else throw new CException(Yii::t('custom', 'Модель не содержит данного свойства.'));
    }

    public function __set($fieldName, $fieldValue){
        if(isset($this->fields[$fieldName]))
            $this->_fieldsValues[$fieldName] = $fieldValue;

        else throw new CException(Yii::t('custom', 'Модель не содержит данного свойства.'));
    }

    public function attributeLabels(){
        static $result = NULL;
        if(!$result)
        {
            $result = array();
            foreach($this->fields as $name => $field)
                $result[$name] = $field['title'];
        }

        return $result;
    }

    /**
     * Обращение к готовым данным.
     * @return array|NULL
     */
    public function getCompleteData(){
        if(!$this->validate()) return NULL;
        $attributes = array();
        foreach($this->fields as $name => $field)
            $attributes[$field['title']] = $this->$name;

        return $attributes;
    }

    public function attributeNames()
    {
        return array_keys($this->fields);
    }

}
