<?php

/**
 * @author koshevy
 *
 * Базовый класс для контроллеров, предназначенных для привязки
 * к StructCategory, выполняющих задачи лендинг-страниц.
 */
class LandingBaseController extends ExtController
{
    /**
     * Текущая категория.
     * @var StructCategory
     */
    public $category = NULL;

    /**
     * Поле, в котором хранятся настройки формы.
     * @var null
     */
    public $customField = NULL;

    /**
     * Класс модели формы заявок.
     * @var string
     */
    public $formModelClass = 'LandingForm';

    /**
     * Максимальное время жизни формы в секундах.
     * Нужно, чтобы раз в период менялось значение подписей при отправки формы.
     * @var string
     */
    public $maxFormLiveTime = 86400;    // 24 часа

    /**
     * Пометка, что была произведена отправка формы.
     * @var bool
     */
    public $resultOfSend = NULL;

    /**
     * Сообщение об отправке формы.
     * @var string
     */
    public $message = NULL;

    /**
     * ID формы, отправка которой была произведена.
     * На случай, если на странице находится несколько форм, чтобы однозначно идентифицировать их.
     * @var null
     */
    public $formID = NULL;

    /**
     * Цвет капчи
     * @var integer
     */
    public $foreColorCaptcha = 0x666666;

    /**
     * Модель формы.
     * Используется как универсальная модель-заготовка для отображения форм.
     * При обработке запроса, используется _sentFormModel.
     * @var CModel
     */
    protected $_formModel = NULL;

    /**
     * Модель формы, которая использовалась при обработке заявки.
     * @var CModel
     */
    protected $_sentFormModel = NULL;

    /**
     * Категория/материал, к которым привязана отправленная форма.
     * Используется только при обработке заявки.
     * @var StructCategory
     */
    protected $_source = NULL;


    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'foreColor' => $this->foreColorCaptcha,
                'testLimit' => 1
            ),
        );
    }
    public function filters()
    {
        return array(
            array(
                'application.filters.CaptchaInitFilter + captcha', // Совместимость капчи.
            )
        );
    }

    protected function beforeAction($action)
    {
        // импорт используемых классов
        Yii::import(Yii::localExtension('structlandings', '*'));
        Yii::import(Yii::localExtension('structlandings', 'models.*'));
        Yii::import(Yii::localExtension('structlandings', 'lib.*'));

        // назначение директории расширения в качестве рабочей
        $this->setViewPath(Yii::getPathOfAlias(Yii::localExtension('structlandings', 'views')));

        Yii::loadExtConfig($this, __FILE__);
        $className = $this->formModelClass;
        $this->_formModel = new $className;
        $this->category = Yii::app()->struct->currentCategory;

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        if(Yii::app()->struct->currentCategory){
            //$this->layout = '//layouts/structLayout';
            $this->render(
                //Yii::app()->struct->structController->views['category'],
                '/category',
                array('category'=>Yii::app()->struct->currentCategory)
            );
        }
        else $this->render('/mainPage');
    }

    /**
     * @param $signature
     * @param $field
     * @param $timeStamp
     * @param $sourceID
     * @param $formID
     * @throws CHttpException
     */
    public function actionOrder($signature, $field, $timeStamp, $sourceID, $formID = NULL)
    {
        // информация о том, какая именно форма была отправлена
        $this->formID = $formID;

        if(isset($_POST[$this->formModelClass]))
        {
            if(preg_match('/^(c|p):(\d{1,5})$/', $sourceID, $matches))
            {
                $this->_source = ($matches[1] == 'c')
                    ? StructCategory::cachedInstance($matches[2])
                    : StructPage::cachedInstance($matches[2]);
            }
            else throw new CHttpException(404);;

            // привязка к категории
            if  (!$this->_source ||
                ($signature != FormEditorHelper::createAliasHash($sourceID, $field, $timeStamp)))
                throw new CHttpException(404); //защита от подделки запроса

            // проверка, чтобы одна и та же форма не была отправлена
            // несколько раз
            Yii::app()->session->open();
            $prevForms = Yii::app()->session->get('prevForms');
            $sessionKey = "{$signature}_{$field}"; // ключ формы в списке отправленных форм в сессии
            if (!$prevForms) $prevForms = array();
            if (false && in_array($sessionKey, $prevForms)) { // отключена проверка на единственную отправку формы
                $this->resultOfSend = true;
                $this->message = Yii::t('custom', 'Спасибо, Ваша заявка уже была отправлена');
            } else
            {
                $prevForms[] = $sessionKey;
                Yii::app()->session['prevForms'] = $prevForms;

                $className = $this->formModelClass;
                $this->_sentFormModel = new $className;

                // применение настроек формы
                $formOptions = $this->_source->{$field};
                if($this->_source->issetCustomField($field))
                    $this->_sentFormModel->applyFormData($formOptions);
                else CException(Yii::t('structlandings',
                    'Не удается найти расширенное поле с настройками формы.'
                ));

                // Проверка, что форма не устарела (чтобы время от времени
                // менялась подпись для интерфейса приемки форм).
                if(($timeStamp - time()) > $this->maxFormLiveTime)
                    $this->message = Yii::t('structlandings',
                        'Форма устарела и не была отправлена. Просто отправьте сейчас заявку еще раз.'
                    );

                else
                {
                    // отправка сообщения
                    $this->_sentFormModel->setAttributes($_POST[$this->formModelClass]);

                    //добавление атачментов
                    $attachments=array();
                    foreach($this->_sentFormModel->fields as $attribute => $valueAttribute)
                    {
                        if($valueAttribute['preset']=='file')
                        {
                            $this->_sentFormModel->{$attribute}=CUploadedFile::getInstance($this->_sentFormModel, $attribute);
                            if($this->_sentFormModel->{$attribute}) {
                                $attachments[$this->_sentFormModel->{$attribute}->name] = $this->_sentFormModel->{$attribute}->tempName;
                            }
                        }
                    }

                    if ($data = $this->_sentFormModel->getCompleteData())
                    {
                        //Подмена селект и радио значений
                        foreach($this->_sentFormModel->fields as $attribute => $valueAttribute) {
                            if ($valueAttribute['preset'] == 'select' || $valueAttribute['preset'] == 'radio') {
                                $index = $data[$valueAttribute['title']];

                                $arguments = str_replace("\r", '', $valueAttribute['arguments']);
                                $arguments =  explode("\n", $arguments);
                                $data[$valueAttribute['title']] = $arguments[$index];
                            }
                        }

                        $data = $this->additionalDataMessage($data, $this->formID);

                        $theme = $formOptions['theme']
                            ?   $formOptions['theme']
                            :   Yii::t('custom', 'Заявка с формы "{categoryTitle}"',
                                array('{categoryTitle}' => $this->_source->title)
                            );

                        $to_email = $formOptions['email']
                            ?   $formOptions['email']
                            :   FeedbackOptionsForm::instance()->to_email;

                        Yii::app()->mailer->sendMessage
                            (
                                array
                                (
                                    'to' => $to_email,
                                    'bcc' => FeedbackOptionsForm::instance()->bcc,
                                    'content' => $data,
                                    'subject' => $theme,
                                    'attachments' => $attachments
                                ),
                                'common'
                            );

                        $this->resultOfSend = true;
                        $this->message = Yii::t('custom', 'Спасибо, Ваша заявка отправлена!');
                    }
                    else
                    {
                        $this->resultOfSend = false;
                        $this->message = Yii::t('custom', 'Форма заполнена неправильно!');
                    }
                }

                // JSON-ответ или обычный
                if (Yii::app()->request->isAjaxRequest) {
                    if ($this->resultOfSend)
                        ExtCoreJson::message($this->message);
                    else ExtCoreJson::error($this->message);
                    ExtCoreJson::response();
                } else $this->forward('index');
            }

        }
        else throw new CHttpException(404);
    }

    //Заглушка
    //Добавляет дополнительные данные в MagicForm
    protected function additionalDataMessage($data, $formID){
        return $data;
    }
}