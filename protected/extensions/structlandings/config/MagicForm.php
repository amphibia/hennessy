<?php

/**
 * @package structlandings
 * @author koshevy
 *
 * Автоматическая форма, разворачиываемая на основании данных от редактора форм.
 */
return array
(

    /**
     * Источник данных - категория или материал.
     * Может быть указана произвольная категория, либо, если свойство
     * не указано, берется активный материал/категория.
     *
     * Если к категории прикреплен контроллер, производный от
     * LandingBaseController, отправка происходит на него, если нет —
     * используется свойство "controller".
     *
     * @var StructCategory|StructPage|NULL
     */
    'source' => Yii::app()->struct->getMainPageCategory(),

    /**
     * Контроллер, производный от LandingBaseController,
     * на который отправляется запрос с формой.
     *
     * @var LandingBaseController
     */
    'controller' => 'site',

    /**
     * Поле категории/материала, в котором хранятся данные для формы.
     * @var string
     */
    'customField' => 'form_data',


    'assets' => array(
        'js' => array(
            'jquery.maskedinput.min.js',
            'formRoutine.js'
        )
    ),

    // представления для формы
    'views' => array(
        'formHeader' => 'frontend/formHeader',
        'formBody' => 'frontend/formBody',
        'formFooter' => 'frontend/formFooter',
    ),

);
?>