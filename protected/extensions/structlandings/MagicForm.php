<?php

/**
 * @package structlandings
 * @author koshevy
 *
 * Автоматическая форма, разворачиываемая на основании данных от редактора форм.
 */
class MagicForm extends CActiveForm
{
    /**
     * Модель, с которой будет происходить работа.
     * @var LandingForm
     */
    public $model = NULL;

    /**
     * Источник данных - категория или материал.
     * Может быть указана произвольная категория, либо, если свойство
     * не указано, берется активный материал/категория.
     *
     * Если к категории прикреплен контроллер, производный от
     * LandingBaseController, отправка происходит на него, если нет —
     * используется свойство "controller".
     *
     * @var StructCategory|StructPage|NULL
     */
    public $source = NULL;

    /**
     * Контроллер, производный от LandingBaseController,
     * на который отправляется запрос с формой.
     *
     * @var LandingBaseController
     */
    public $controller = NULL;

    /**
     * Поле категории/материала, в котором хранятся данные для формы.
     * @var string
     */
    public $customField = NULL;

    /**
     * Отправка формы через AJAX.
     * @var boolean
     */
    public $ajaxSend = NULL;


    public $assets = array();

    /**
     * Данные для этой формы.
     * @var array
     */
    protected $_formData = NULL;

    public $views = array();

    /**
     * Вывод label
     * @var bool
     */
    public $show_labels = false;

    /**
     * Шаблон формы. Позволяет делать HTML вставки в начало и в конец ячейки.form-group
     * Константы ячеек: top_external, bottom_external, top_internal, bottom_internal
     * @var array
     * 'template' => array(
     *      0 => array('top_external' => '<div class="external">'),
     *      2 => array(
     *        'bottom_external' => '</div><div class="external">',
     *        'top_internal' => '<div class="internal">',
     *        'bottom_internal' => '</div>',
     *      ),
     *      4 => array('bottom_external' => '</div>'),
     *      'button' => array('bottom_external' => '<div></div>'),
     * ),
     */
    public $template = array();

    /**
     * Initializes the widget.
     * This renders the form open tag.
     */
    public function init()
    {
        Yii::import(Yii::localExtension('structlandings', 'lib.*'));
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

        $this->_initParams();

        // должна идти после инициализации
        // параметров виджета
        parent::init();

        // шапка формы (после нее еще может быть вставлен контент)
        $this->render($this->views['formHeader']);
    }

    /**
     * Runs the widget.
     * This registers the necessary javascript code and renders the form close tag.
     */
    public function run()
    {
        //die("<pre>".print_r($this->views, true)."</pre>");

        // тело формы (сразу после пользовательского контента)
        $this->render($this->views['formBody']);

        // подвал формы
        $this->render($this->views['formFooter']);
        parent::run();
    }

    /**
     * Инициализация параметров для формы.
     */
    protected function _initParams()
    {
        // По-умолчанию, источник — текщуий материал,
        // или категория, если материал отсутствует.

        // Если категория не указана, то используется категория
        // главной страницы.
        if(!$this->source)
        {
            $this->source = Yii::app()->struct->currentPage
            or
            $this->source = Yii::app()->struct->currentCategory
            or
            $this->source = Yii::app()->struct->getMainPageCategory();
        }

        $this->customField = preg_replace('/\W/', '', $this->customField);
        if(!$this->customField)
            throw new CException(Yii::t('structlandings',
                'Не указано поле, используемое для хранения параметров формы.'
            ));

        // Формирование URL для свойства parent::$action (наследство от CActiveForm),
        // туда будет отправляться форма.
        if($this->source)
        {
            // выяснение категории — источника
            Yii::import('application.controllers.*');
            if($this->source instanceof StructCategory){
                $sourceID = "c:{$this->source->id}"; // кодовое обозначение объекта-источника
                $destCategory = $this->source;
            }
            else if($this->source instanceof StructPage){
                $sourceID = "p:{$this->source->id}";
                $destCategory = $this->source->getCategory();
            }
            else throw new CException(Yii::t('structlandings',
                'Источником для формы могут быть только категории или материалы.'
            ));

            $timestamp = time();
            $signature = FormEditorHelper::createAliasHash($sourceID, $this->customField, $timestamp); // создание подписи

            // по-умолчанию, отправка будет производиться на контроллер,
            // прикрепленный к категории-источнику, или категории материала-источника
            if( ($destCategory->type == 'controller') ){
                $className = ucfirst($destCategory->handler).'Controller';
                if(is_subclass_of($className, 'LandingBaseController'))
                    $this->action = $destCategory->createUrl()
                        . "/order/{$signature}/{$this->customField}/{$timestamp}/{$sourceID}/{$this->id}";
            }

            // если, категория-источник не является LandingBaseController,
            // отправка идет на указанный контроллер
            if(!$this->action)
            {
                if($this->controller){
                    if($this->controller instanceof LandingBaseController)
                        $cID = $this->controller->id;
                    else if(is_string($this->controller))
                        $cID = $this->controller;
                    else CException(Yii::t('structlandings',
                        'MagicForm::$controller должен быть контроллером-потомком LandingBaseController, или его ID.'
                    ));
                }
                else CException(Yii::t('structlandings',
                    'Форма должна использоваться в категории, к которой привязан контроллер-потомок LandingBaseController. '
                    . 'Либо необходимо явно указывать свойство MagicForm::$controller.'
                ));

                $this->action = "/{$cID}/order/{$signature}/{$this->customField}/{$timestamp}/{$sourceID}/{$this->id}";
            }
        }
        else throw new CException(Yii::t('structlandings',
            'Не указаны материал или категория, ассоциируемые с формой.'
        ));

        // нормализация данных для виджета
        $this->_formData = $this->source->{$this->customField};
        FormEditorHelper::normalizeData($this->_formData);

        // подготовка модели, с которой работает форма
        if(!$this->model) $this->model = new LandingForm();
        $this->model->applyFormData($this->_formData);

        // форма предполагает собственные настройки для AJAX
        if($this->ajaxSend)
        {
            $this->enableAjaxValidation = false;
            $this->enableClientValidation = true;

            $this->clientOptions = array(
                'validateOnChange' => true,
                'validateOnSubmit' => true,
                'afterValidate' => 'js:beforeYiiValidate',
            );
        }
    }

}
?>