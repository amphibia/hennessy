/**
 * JS-плагин для работы с компонентом редактирования форм.
 **/
jQuery.fn.extend({

    /**
     * Инициализация объекта.
     **/
    structFormEmailField_init: function(){

        var formEditor = this,
            $teaser = $('.struct-form-teaser[rel="'+$(formEditor).attr('id')+'"]');

        // Разворачивает автоматически-добавляемые поля (например, для email)
        // на основании заготовки.
        $(this).find('.repeat-field-blank').each(function(){
            var $newObject = $(this).clone();
            $newObject.removeClass('repeat-field-blank').addClass('repeat-field');
            $newObject.appendTo($(this).parent());
        });

        // Обработка автоматически повторяющихся полей (например, для
        // введения нескольких email-ов)
        $(this).find('.repeat-field input').live('change', function(){

            // подчищает все пустые поля
            $(this).structFormEmailField_removeEmptyFields();
        });

        // Переход на следующий эл. адрес по нажатию Enter
        $(this).find('.repeat-field input').live('keydown', function(evt){
            // Ввод - предполагается введение нового адреса в новой строке
            if((evt.keyCode == 13)){
                $(this).structFormEmailField_addAfter();
                $(this).closest('.repeat-field').next()
                    .find('input').focus();
                return false;
            }
        });

        // автодобавление полей
        $(this).find('.repeat-field input').live('keyup', function(evt){

            // добавляет поле в конце
            $(this).structFormEmailField_addAfter();

            // Удаление с пустой строки - возврат к предыдущей строке
            if(evt.keyCode == 8){
                if(!$(this).val()){
                    var $controlGroup = $(this).closest('.control-group');
                    var $prevItem = $(this).closest('.repeat-field')
                        .prev('.repeat-field')
                        .find('input').focus();

                    if($prevItem.length) $prevItem.focus();
                    else{
                        $controlGroup
                            .find('.repeat-field:last-child input')
                            .focus();
                    }

                    $(this).structFormEmailField_removeEmptyFields();

                    return false;
                }
            }
        });

        // проверка введенных эл. адресов / их отображение в тизере
        $(this).find('[data-attr="email"]').live('change', function()
        {
            // отображение в тизере изменений по эл. адресам
            var emails = [];
            $(formEditor).find('.repeat-field input').each(function(){
                if($(this).val()) emails.push($(this).val());
            });

            if(emails.length){
                $teaser.find('[data-attr="email"]')
                    .text(emails.join(', '));
            }
            else{
                var $apprCaption = $teaser.find('[data-attr="'+$(this).attr('data-attr')+'"]'),
                    value = $(this).val();

                value = value ? value : $apprCaption.attr('data-empty-value');
                value = $("<div/>").text(value).html().replace(/([^>])\n/g, '$1<br/>');
                $apprCaption.html(value);
            }

            // проверка правильности эл. адреса
            var reg = /^([\w\d\-_]+\.?)+@([\w\d\-_]+\.?)+(\.\w{2,10})?$/;
            if(!reg.test($(this).val()))
                $(this).addClass('error');
            else $(this).removeClass('error');
        });

        // переключения формы / тизера
        $('.struct-form-teaser[rel="'+$(this).attr('id')+'"] .more a').click(function(){
            $(this).closest('.struct-form-teaser').fadeOut(500, function(){
                $(formEditor).addClass('open');
            });
            return false;
        });

        $(this).find('.less a').click(function(){
            $(formEditor).removeClass('open');
            $teaser.fadeIn(500)
        })

        // изменения в настройках форм, которые должны сразу отображаться в тизере
        $(this).find('[data-attr="title"], [data-attr="theme"], [data-attr="description"]')
            .change(function(){
                var $apprCaption = $teaser.find('[data-attr="'+$(this).attr('data-attr')+'"]'),
                    value = $(this).val();

                value = value ? value : $apprCaption.attr('data-empty-value');
                value = $("<div/>").text(value).html().replace(/([^>])\n/g, '$1<br/>');
                $apprCaption.html(value);
            });

        $(this).find(".fields-editor ul").sortable({
            placeholder: "fields-placeholder well",
            handle: ".handle",
            cancel: ".deleted, .unmovable"
        });

        // переключатель "обязательное поле"
        $(this).find('.required-control').live('click', function(){
            $(this).toggleClass('active');
            $(this).closest('li').find('.required-input').val($(this).hasClass('active') ? 1 : 0);
        });

        // включение расширенных настроек
        $(this).find('.extended-control').live('click', function(){
            $(this).toggleClass('active');
            $(this).closest('li').find('.extended-options').toggleClass('open');
            $(this).closest('li').toggleClass('unmovable')
        });

        // удаление поля
        $(this).find('.delete-control').live('click', function(){
            var $replacement = $(this).closest('li');

            // новые незаполненные элементы просто удаляются,
            // для старых элементов остается плейсхолдер, позволяющий вернуть элемент
            if( $replacement.hasClass('new') &&
                !$replacement.find('.title-input').val() &&
                !$replacement.find('.required-message').val() &&
                !$replacement.find('.extended-arguments').val()){

                // просто удалаяются
                $replacement.remove();
            }

            // старые поля, и поля, для которых были указаны
            // название, или другие настройки
            else{

                var $deletedPlaceholder = $(this).closest('ul').find('.deleted.bill').clone();
                $('.tooltip.in').remove(); // костыль для глюка, когда подсказка кнопки "удалить" остается висеть после восстановления
                $deletedPlaceholder.removeClass('bill')
                    .find('.restore').click(function(){
                        $(this).closest('li').replaceWith($replacement);
                        $(formEditor).structFormEmailField_checkFieldsCount();
                        return false;
                    });

                $replacement.replaceWith($deletedPlaceholder);
            }

            // проверка количества полей
            $(formEditor).structFormEmailField_checkFieldsCount();

            return false;
        });

        // добавление нового поля
        $(this).find('.add-field-controll').click(function(){
            var $elementsList = $(formEditor).find('ul'),
                $newFieldElement = $elementsList.find('.new.bill').clone();

            $newFieldElement.removeClass('bill')
                .find('input, select, textarea').removeAttr('disabled');
            $elementsList.append($newFieldElement);

            // проверка количества полей
            $(formEditor).structFormEmailField_checkFieldsCount();

            return false;
        });

        // изменение конфигурации полей по-умолчанию
        $(this).find('.edit-default-fields').click(function(){
            $(formEditor).find('.default-fields-state').val(0);
            $(formEditor).attr('default-fields', 'false');
        });

        // сброс настроек полей к конфигурации по-умолчанию
        $(this).find('.default-fields-controll').click(function(){
            $(formEditor).find('.default-fields-state').val(1);
            $(formEditor).attr('default-fields', 'true');
        });
    },

    /**
     * Добавляет поле еще одно поле введения эл. адреса
     * при заполнении предыдущего.
     */
    structFormEmailField_addAfter: function(){
        var $repeatField = $(this).closest('.repeat-field');
        if($(this).val() && !$repeatField.next().length){
            var $newObject = $repeatField.closest('.control-group')
                .find('.repeat-field-blank').clone();
            $newObject.removeClass('repeat-field-blank').addClass('repeat-field');
            $newObject.insertAfter($repeatField);
        }
    },

    /**
     * Удаление неиспользуемых полей ввода Эл. алресов.
     */
    structFormEmailField_removeEmptyFields: function(){
        $(this).closest('.control-group')
            .find('.repeat-field').each(function(){
                if(!$(this).find('input').val() &&
                    $(this).next().length)
                    $(this).remove();
            });

        // проверка количества полей
        $(this).closest('.struct-form-editor')
            .structFormEmailField_checkFieldsCount();
    },

    /**
     * Проверка количества полей: максимальное количество.
     */
    structFormEmailField_checkFieldsCount: function(){
        var fieldsCount = parseInt($(this).find('.fields-editor .field-element:not(.bill)').length),
            maxFieldsCount = parseInt($(this).attr('data-max-fields')),
            $toggledItems = $(this).find('.add-field-controll, .deleted .restore'),
            $noFieldsWarning = $(this).find('.no-fields-in-list');

        if(fieldsCount >= maxFieldsCount)
            $toggledItems.fadeOut(500);
        else $toggledItems.fadeIn(500);

        if(fieldsCount == 0)
            $noFieldsWarning.fadeIn(500);
        else $noFieldsWarning.fadeOut(500);
    },

    /**
     * Проверка данных перед отправкой.
     * @param className
     * @param attribute
     */
    structFormEmailField_validate: function(className, attribute){
        /** @todo: Валидатор не реализован */
    }

});