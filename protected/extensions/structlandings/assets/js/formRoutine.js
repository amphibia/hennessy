// инициализация объекта
$(document).ready(function(){

    $('form').each(function(){
        if(!$(this).hasClass('initialized-form')){
            $(this).addClass('initialized-form');

            var $phone = $(this).find('input[data-preset="phone"]'),
                $time = $(this).find('input[data-preset="time"]'),
                $date = $(this).find('input[data-preset="date"]');

            if($phone.length) $phone.mask("+7 (999) 999-99-99");
            if($time.length) $time.mask("99:99");
            if($date.length) $date.mask("99.99.9999");
        }
    });

});