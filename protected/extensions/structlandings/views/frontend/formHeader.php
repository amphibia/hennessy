<!-- Открывается внутренняя часть формы -->
    <?php if($this->model->title): ?>
        <h2><?= html_entity_decode($this->model->title) ?></h2>
    <?php endif ?>

    <?php if($this->model->description): ?>
        <div class="description">
            <?= nl2br(html_entity_decode($this->model->description)) ?>
        </div>
    <?php endif ?>

    <div class="fields">