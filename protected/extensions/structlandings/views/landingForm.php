<?
if(!Yii::app()->clientScript->isScriptRegistered('beforeYiiValidate', CClientScript::POS_BEGIN))
{
    $beforeYiiValidate = <<<'EOT'
function beforeYiiValidate($form, errors, hasError)
{
    if(!hasError && !$form.hasClass('clientBusy')){
        var data = $form.getFields();

        $form.request($form.attr('action'), data,
            function(result){
                if(result && result.__commands && result.__commands.message){
                    var messages = result.__commands.message
                    $form.html('');
                    for(var i in messages)
                        $form.append('<p class="'+(parseInt(data.result)?'success':'error')+'">'+messages[i]+'</p>');
                }
            }
        );
    }

    return false;
}
EOT;

    Yii::app()->clientScript->registerScript(
        'beforeYiiValidate',
        $beforeYiiValidate,
        CClientScript::POS_BEGIN
    );
}

$form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,

    'action' => $this->createOrderUrl($categoryOrPage, $subject),

    'clientOptions' => array(
        'validateOnChange' => true,
        'validateOnSubmit' => true,
        'afterValidate' => 'js:beforeYiiValidate',
    ),

    'htmlOptions' => array(
        'class' => 'block-form'
    )
));
?>

<div class="sub">

    <? if(!$this->_resultOfSend || ($this->_orderCategory->id != $category->id)): ?>
        <h2><?= $formTitle ?></h2>
        <div class="fields">
            <? foreach($model->fields as $fieldName => $fieldOptions): ?>
                <? $preset = $model->rulePresets[$fieldOptions['preset']]; ?>
                <div class="row" data-element-type="<?= $preset['element'] ?>">
                    <?
                    echo CHtml::openTag('div', array(
                        'class' => 'elementWrapper'
                    ));

                    switch($preset['element'])
                    {
                        case 'text':
                            echo $form->textField($model, $fieldName, array(
                                'placeholder' => $model->getAttributeLabel($fieldName)
                            ));
                            break;

                        case 'textarea':
                            echo $form->textArea($model, $fieldName, array(
                                'placeholder' => $model->getAttributeLabel($fieldName)
                            ));
                            break;

                        case 'select':
                            if(isset($fieldOptions['values']))
                                 $values = $fieldOptions['values'];
                            else $values = $preset['values']['in'];

                            echo $form->dropDownList(
                                $model,
                                $fieldName,
                                $values,
                                array('placeholder' => $model->getAttributeLabel($fieldName))
                            );
                            break;
                    }

                    echo CHtml::closeTag('div');
                    echo CHtml::tag('div',
                        array('class' => 'errorWrapper'),
                        $form->error($model, $fieldName)
                    );
                    ?>
                </div>
            <? endforeach; ?>
        </div>
        <div class="row button">
            <button class="btn btn-default">
                <?= $formButtonTitle ?>
            </button>
        </div>
    <? else: ?>
        <p class="success"><?= $this->_message ?></p>
    <? endif ?>

    <div class="clearfix"></div>
</div>

<? $this->endWidget(); ?>
<div class="clearfix"></div>