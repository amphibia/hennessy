<?php

return array
(
    'collapsed' => true,
    
    /**
     * Включена ли DRAG-N-DROP - сортировка элементов.
     * @var boolean
     */
    'isSortable' => true,

    /**
     * Роут для формирования ссылки на действие, перемещающее запись внутри дерева.
     * Передаёт следующие аргументы: $id, $beforeID = NULL, $afterID = NULL, $appendTo = NULL.
     * 
     * Аргументы передаются в POST. Если действие-приёмщк находится в сервисе,
     * можно использовать аргументы действия вида:
     * actionMoveItem($id, $beforeID = NULL, $afterID = NULL, $appendTo = NULL).
     * @var string
     */
    'itemMoveAction' => 'moveitem',

    /**
     * Условие и параметры отсортировки.
     * Осно
     * По умолчанию - выборка некорневых элементов (только для Nested-моделей).
     * @var array
     */
    'conditions' => array("t1.{$component->model->levelAttribute}>1"),

    /**
     * Рабочая модель.
     * @var CModel
     */
    'model' => NULL,

    /**
     * ID записи-предка (не имеет значения для Nested-записей).
     * @var string
     */
    'parentIdField' => 'parent_id',

    /**
     * Используемые поля.
     * @var string
     */
    'fields' => array
    (
        'text'=>'title',
        'alt'=>false,
        'id_parent'=>false,
        'task'=>false,
        'icon'=>false,
        'tooltip'=>false,
        'url'=>false,
        'options'=>false
    ),

    /**
     * Шаблон формирования надписи.
     * @var string
     */
    'template' => '{text}',

    /**
     * Свойство, помещающее, что при обновлении структуры дерева требуется
     * перерисовать его.
     * 
     * При перерисовке отправляеся запрос по текущему URL, при этом в POST
     * отправляется 'MTreeViewRepaint'. Если используется динамичная перерисовка,
     * в месте использования виджета необходимо вставит проверку и, в случае
     * обнаружения параметра $_POST['MTreeViewRepaint'], выводить только виджет.
     * @var boolean
     */
    'repaintOnChanges' => false,

    /**
     * Скорость анимации (fast/slow).
     * @var mixed
     */
    'animated' => 'fast',


    'assets' => array
    (
        'core' => array('jquery'),
        'js' => array('TreeViewSortable.js'),
        'css' => array('TreeViewSortable.css')
    )
);