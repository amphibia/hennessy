<?php

/**
 * @package mtmselect
 * @author koshevy
 * 
 * Валидатор для проверки атрибутов - MANY_TO_MANY отношений. Обычно применяется
 * в связке с эдементом MTMSelect, однако может использоваться и отдельно.
 */
class ManyToManyValidator extends CValidator
{
    /**
     * Минимальное количество привязанных элементов.
     * @var int
     */
    public $min = NULL;

    /**
     * Максимальное количество привязанных элементов (по умолчанию 32).
     * @var int
     */
    public $max = 32;


    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object, $attribute)
    {
        $validator = &$this;

        // чтобы не было проблемы "невозможно удалить единственный элемент",
        if(isset($_POST[get_class($object)]) && !isset($_POST[get_class($object)][$attribute]))
            $object->$attribute = array();

        if(!is_null($this->min) && (sizeof($object->$attribute) < $this->min))
            $object->addError($attribute, Yii::t('mtmselect',
                'At least {number} related items required',
                    array('{number}'=>$this->min)));

        if($this->max && (sizeof($object->$attribute)>$this->max))
            $object->addError($attribute, Yii::t('mtmselect',
                'You can`t use more than {number} related items',
                    array('{number}'=>$validator->max)));
    }

}
