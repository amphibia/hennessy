<?php
class CheckBoxListValidator extends CValidator{
    public $items = array();

    // этот метод вызвается непосредственно при валидации
    protected function validateAttribute($object, $attribute) {

        if(is_array($object->$attribute))
        {
            foreach($object->$attribute as $value) {
                if (!in_array($value, $this->items)) {
                    $this->addError($object, $attribute, Yii::t('error', 'Не верное значение'));
                }
             }
         }
        else {
            $this->addError($object, $attribute, Yii::t('error', 'Переданы не верные данные'));
        }
    }
}