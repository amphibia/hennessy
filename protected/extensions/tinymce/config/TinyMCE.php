<?php

return array
(
    /**
     * Конфигурация для TinyMCE - плагина.
     * @var array
     */
    'editorConfig' => array
    (
        // General options
        'theme' => 'advanced',
        'plugins' => "safari,autolink,lists,style,layer,table,save,advhr,advimage,advlink,inlinepopups,insertdatetime,media,searchreplace,print,contextmenu,paste,noneditable,nonbreaking,xhtmlxtras,template,advlist",
        // Theme options
        'theme_advanced_buttons1' => "formatselect,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
        'theme_advanced_buttons2' => "undo,redo,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,forecolor,backcolor,|,link,unlink,image,media,charmap,|,cleanup",//,help
        'theme_advanced_buttons3' => "tablecontrols,visualaid,|,hr,removeformat,styleprops,|,sub,sup,|,code",
        //'theme_advanced_buttons4' => "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs",
        'theme_advanced_toolbar_location' => "top",
        'theme_advanced_toolbar_align' => "left",
        'theme_advanced_statusbar_location' => "top",
        'theme_advanced_resizing_use_cookie' => false,
        'theme_advanced_resizing_min_width' => 200,
        'theme_advanced_resizing' => false,

        //'convert_newlines_to_brs' => true,
        //'force_p_newlines' => false,
        //'force_br_newlines'=>true,
        'width' => '100%',
        'theme_advanced_path' => true,
        
        'handle_event_callback' => 'TinyMCE_eventHandler',
        'init_instance_callback' => 'TinyMCE_onInit',
		'onchange_callback' => 'TinyMCE_onChangeHandler',
        'object_resizing' => true,
        'apply_source_formatting' => true,

        'relative_urls' => false,
        'convert_urls'=>false,

        'valid_elements' => "@[*]",
        'extended_valid_elements' => '*[*]',

        /**
         * 'file_browser_callback' => 'TinyMCE_browse',
         * 
         * Если свойство не задано, но в системе используется компонент KCFinderComponent,
         * он автоматически добавляет метод из его ассетсов в TinyMCE::$editorConfig['file_browser_callback']
         **/
    ),

    /**
     * Стили внешней части, подключаемые для предпросмотра результата.
     * Устанавливает свойство 'content_css'.
     * @var array
     */
    'faceCssFiles' => array(),

    /**
     * Свой стиль для редактора.
     * @var string
     */
    'editorCss' => NULL,

    /**
     * Свой стиль для содержимого всплывающих окон.
     * @var string
     */
	'popupCss' => 'popup.css',

    /**
     * Метод прямого сохранения текста (в активном контроллере).
     *
     * По умолчанию:
     * actionSetAttributes($id, Array $values, $returnMessage = true, Array $customMessage = NULL).
     * Порядок работы сохраняется.
     * @var string
     */
    'forceSaveAction' => 'setAttributes',

    /**
     *  Сообщение, возвращаемое при прямом сохранении.
     * @var string
     */
    'forceSaveMessage' => Yii::t('tinymce', '<strong>Text was saved directly.</strong><br/>Other attributes was not saved.'),

    /**
     * Заголовок окна подгрузки файлов.
     * 
     * ПРИМЕЧАНИЕ: если используется несколько элементов TinyMCE в одной форме,
     * то на заголовок, заданный для последнего из них будет применятся для
     * всех - на стороне клиента значение задаётся только один раз.
     * @var string
     */
    'filesBrowseTitle' => Yii::t('tinymce', 'File browser'),

    /**
     * ID компонента приложения, который обеспечивает загрузку файлов из TinyMCE.
     * Компонент должен иметь свойство $jsBrowseFunction (JS-функция, реализующей
     * поддержку загрузки) - именно оно используется при конфигурированиии виджета.
     * @var string 
     */
    'fileUploaderID' => 'kcfinder',

    /**
     * Свойство, используемое клиентской частью: селектор элемента, являющегося
     * корневым контейнером для редактора, на всю площадь которого будет развёрнут
     * редактор.
     * 
     * ПРИМЕЧАНИЕ: CSS-свойство элемента 'position' будет установлено в 'relative',
     * и если промежуточный родитель тоже будет задан как 'relative', возникнет
     * сбой при разворачивании. Если же селектор не будет указан, либо будет
     * указан неверно, редактор развернётся на всю страницу.
     * @var string
     */
    'rootContainerSelector' => '.container.root-container',

    /**
     * Свойство, используемое клиентской частью: селектор элемента, являющегося
     * корневым контейнером для редактора, на всю площадь которого будет развёрнут
     * редактор в обычном положении (для режима full-screen смотрите $rootContainerSelector).
     * 
     * ПРИМЕЧАНИЕ: CSS-свойство элемента 'position' будет установлено в 'relative',
     * и если промежуточный родитель тоже будет задан как 'relative', возникнет
     * сбой при разворачивании. Если же селектор не будет указан, либо будет
     * указан неверно, редактор развернётся на всю страницу.
     * @var string
     */
    'ownContainerSelector' => '.controls',

    /**
     * @var array
     */
    'assets' => array(
        'core' => array('jquery', 'cookie'),
        'js' => array('routine.js', 'jquery.autosize.js'),
		'pure' => array('tiny_mce_src.js'),
        'css' => array('routine.css')
    ),

    /**
     * @var array
     */
    'views' => array('main'=>'main')

);
