<?php

/**
 * @author koshevy
 * @package tinymce
 *
 * Контроллер, предоставляющий функционал KCFinder.
 * Контроль доступа осуществляется в стандартном порядке.
 * 
 * Использует функционал плагина KCFinder (в lib/kcfinder) для серверной части.
 * Клиентская часть отделена и вынесена в assets (привязаны к этому контроллеру).
 * 
 * Основные свойства выделены в компонент, с которым контроллер имеет жесткую
 * связь (@see KCFinderComponent, указан в KCFinderController::$ownerComponent).
 * 
 * Компонент KCFinderComponent должен быть 'preload' - он добавляет контроллер
 * в controllerMap с инициализацией аргументов.
 */
class KCFinderController extends ExtController
{
     /**
     * Загрузка файлов будет отслеживаться отдельно.
     */
    public function getRbacOperation(){
        return 'File upload';
    }
    
    
    /**
     * Компонент, запустивший этот контроллер.
     * Указывается из компонента KCFinderComponent при инициализации.
     * Не нужно указывать в конфиге.
     * @var CApplicationComponent
     */
    public $ownerComponent = NULL;

    /**
     * Заголовок окна.
     * @var string
     */
    public $title = NULL;
    
    /**
     * @var array
     */
    public $assets = array();


    /**
     * Отключает ExtCore - обработку аргументов (чтобы не затирался родной $_GET).
     * @var boolean
     */
    protected $_useAutoArguments = false;
    


    public function init()
    {
        // загрузка конфига по умолчанию
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

        if(!$this->ownerComponent) throw new CHttpException(500,
            Yii::t('kcfinder', 'Can`t run without owner component'));

        if(!$this->ownerComponent->imagesDir) throw new CHttpException(500,
            Yii::t('kcfinder', 'Upload directory not setted'));

        // инициализация системы динамической подгрузки классов в KCFinder
        $this->_kcfinderAutoload();
    }
    
    /**
     * Импорт библиотеки KCFinder.
     */
    protected function _kcfinderAutoload()
    {
        // GD EXTENSION CHECK
        if(!function_exists("imagecopyresampled"))
            throw new CHttpException(500, Yii::t('kcfinder',
                "The GD PHP extension is not available! It's required to run KCFinder."));

        // SAFE MODE CHECK
        if(ini_get("safe_mode"))
            throw new CHttpException(500, Yii::t('kcfinder',
                'The \"safe_mode\" PHP ini setting is turned on! You cannot run KCFinder in safe mode.'));
        
        $extensionPath = Yii::localExtension(basename(__DIR__));
        Yii::import("$extensionPath.lib.kcfinder.*");
        Yii::import("$extensionPath.lib.kcfinder.lang.*");
        Yii::import("$extensionPath.lib.kcfinder.tpl.*");
        Yii::import("$extensionPath.lib.kcfinder.core.*");
        Yii::import("$extensionPath.lib.kcfinder.core.types.*");
        Yii::import("$extensionPath.lib.kcfinder.lib.*");
    }

    /**
     * Интерфейс загрузки файлов. 
     */
    public function actionUpload()
	{
        $uploader = new uploader();
        $uploader->upload();
    }

    /**
     * Интерфейс браузера KCFinder. 
     */
    public function actionBrowse()
	{
		$browser = new browser();
        $browser->action();
    }

    /**
     * Сжиматель JS-файлов для KCFinder (если использовать - умерите их из ассетсов)
     * @return  void
     */
    public function actionJoiner()
    {
        return;
        $files = dir::content("js/browser", array(
            'types' => "file",
            'pattern' => '/^.*\.js$/'
        ));

        foreach ($files as $file) {
            $fmtime = filemtime($file);
            if (!isset($mtime) || ($fmtime > $mtime))
                $mtime = $fmtime;
        }

        httpCache::checkMTime($mtime);
        header("Content-Type: text/javascript");
        foreach ($files as $file)
            require $file;
    }
}

?>
