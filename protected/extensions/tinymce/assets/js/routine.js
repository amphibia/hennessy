/**
 * @package tinymce
 * @author koshevy
 * Клентская часть компонента TinyMCE.
 * При необходимости, все функции быть переопределены в конфиге TinyMce. 
 */

/**
 * Деактивирование всех редакторов.
 **/
/*
function TinyMCE_blurAll()
{
	//$('.activeExpandPlace').css('height', 'auto');
	//$('.mceEditor').find('iframe, .mceIframeContainer').css('height', 'auto').css('display', 'auto');
	$('.mceActive').removeClass('mceActive').removeClass('mceStarted');
}
*/

/**
 * Перехватчик событий, цель которого развернуть форму на весь документ при
 * клике на тело редактора.
 * @return void
 **/
function TinyMCE_eventHandler(evt)
{
    // по элементу "body" будет определяться редактор, к которому идёт обращение
    var $body = $(evt.target).closest('body');
    if(!$body.length) $body = $(evt.target).find('body');
    if(!$body.length) return;

    var id = $body.attr('id'),
        containerId = id+'_parent',
        $editor = $(".mceEditor#"+containerId),
		$mceLayout = $editor.find('.mceLayout'),
		$mceIframeContainer = $editor.find('iframe, .mceIframeContainer'),
        height = $body.closest('html').innerHeight();


    // У Google Chrome есть одна неприятная особенность: он высчитывает высоту
    // body не по высоте содержимого, а по высоте содержащего его iframe
	// 
	// ПРИМЕЧАНИЕ: в последних версиях FF наблюдается такая же ситуация.
	//if($.browser.webkit)
	//{

        var sumHeight = parseInt($body.css('margin-top')) + parseInt($body.css('margin-bottom'));
		if(!sumHeight) sumHeight = 0;

        $body.children('*').each(function(){
            var top;
            if((top = this.offsetTop || $(this).position().top)>sumHeight)
                sumHeight = top;
            sumHeight += this.offsetHeight + parseInt($(this).css('margin-bottom'));
        });    
        height = sumHeight+64;
	//}


	//if(!$editor.hasClass('mceActive') && !evt.justInit){
	//	TinyMCE_blurAll();
		$editor.addClass('mceActive');
	//}

	$editor.find('iframe, .mceIframeContainer').css('overflow', 'hidden');
	$editor.find('iframe, .mceIframeContainer').css('height', '')
		.attr('height', height)
		.css('height', height+'px');

	$editor.find('.mceLayout').css('height', 'auto');
	//$mceIframeContainer.css('bottom', '0px');
	//$editor.closest('.activeExpandPlace').css('height', ($mceLayout.outerHeight())+'px');
}

function TinyMCE_onChangeHandler(evt)
{
	TinyMCE_eventHandler(evt);
}

/**
 * Вызывается после разворачивания редактора (когда готовы все элементы инфраструктуры
 * TinyMCE). Подготавливает для своих доработок.
 * @return void
 **/
function TinyMCE_onInit(inst)
{
    var $container = $('#'+inst.editorContainer),
        rootContainerSelector = $("#"+inst.id).data('tinymce_rootContainerSelector'),
        $rootContainer = $container.closest(rootContainerSelector),
        ownContainerSelector = $("#"+inst.id).data('tinymce_ownContainerSelector'),
        $ownContainer = $container.closest(ownContainerSelector);

    //$rootContainer.addClass('mceRootContainer')
    //$ownContainer.addClass('mceOwnContainer');
	//TinyMCE_setExpandPlace($container, false);

	$rootContainer.css('position', 'relative').css('overflow', 'hidden');
	$('#'+inst.editorContainer).find('iframe').attr('scrolling', 'no');

    $('a[title]').attr('rel', 'tooltip');
    $('[rel="popover"]').popover();
    $('[rel="tooltip"]').tooltip();

	// наблюдатель за скроллом, который следит, чтобы панель управления 
	// активных редаткоров переключалась в fixed-режим
	var toolbarScrollAgent = function(){
		var scrollTop = $(document).scrollTop() || $(document.body).scrollTop();
		if(!$('.mceEditor.mceActive').length) return;
		var mceEditor = $($('.mceEditor.mceActive')[0]),
			mceEditorHeight = mceEditor.find('.mceLayout').height(),
			mceEditorTop = mceEditor.hasClass('fixedToolbar')
				? mceEditor.attr('data-original-top') : mceEditor.offset().top;

		var $toolbarPanel = mceEditor.find('.mceToolbar [role="group"]'),
			toolbarHeight = $toolbarPanel.height();

		$toolbarPanel.css('height', toolbarHeight+'px');

		if((scrollTop >= mceEditorTop-60) /** @todo Костыльное решение! **/
			&& (mceEditorHeight>$(window).height())
			&& (scrollTop < (parseInt(mceEditorTop)+parseInt(mceEditorHeight)-(toolbarHeight*2)))){

			if(!mceEditor.hasClass('fixedToolbar')){
				mceEditor.addClass('fixedToolbar');
				mceEditor.attr('data-original-top', mceEditorTop);
			}
		}
		else mceEditor.removeClass('fixedToolbar');
	}

	//$container.find('iframe, .mceIframeContainer').css('height', 'auto');

	if(!$(document.body).hasClass('toolbarScrollAgent')){
		$(document.body).addClass('toolbarScrollAgent')
		$(window).scroll(toolbarScrollAgent);
		$(window).resize(toolbarScrollAgent);
	}

	inst.windowManager.onOpen.add(function(){
		var offsetTop = $('html').scrollTop();
		$('html').css('overflow', 'hidden');
		$('html').scrollTop(offsetTop);
	});
	inst.windowManager.onClose.add(function(evt){
		var offsetTop = $('html').scrollTop();
		$('html').css('overflow', 'scroll');
		$('html').scrollTop(offsetTop);

		evt.target = inst.contentDocument.body;

		TinyMCE_eventHandler(evt);
	});

	// загрузка завершена
	var $mceServWrapper = $container.closest('.mceServWrapper.mceLoading');
	$mceServWrapper.fadeOut(300, function(){
		$mceServWrapper.removeClass('mceLoading')
			.fadeIn(500, function(){
				TinyMCE_eventHandler({target:inst.contentDocument.body, justInit:true});
			});
	});
	
	//inst.contentDocument.body.onload = function(){
	//	TinyMCE_eventHandler({target:inst.contentDocument.body, justInit:true});
	//}
}

/**
 * Подключение закачки и файдера (по умолчанию KCFinder).
 * Можно подключить другой - для этого в конфиге TinyMCE нужно изменить свойство
 * editorConfig['file_browser_callback'].
 * @return boolean
 **/
function TinyMCE_browse(field_name, url, type, win) {
    tinyMCE.activeEditor.windowManager.open({
        file: '/kcfinder/browse?Lang=ru&opener=tinymce&type=' + type,
        title: $(document).data('TinyMCE_filesBrowseTitle'),
        width: 700,
        height: 500,
        resizable: "no",
        inline: true,
        close_previous: "no",
        popup_css: false
    },{
        window: win,
        input: field_name
    });
    return false;
}

/**
 * Перехват команд, в частности сохранения страницы (чтобы сохранить содержимое
 * скрыто, без переотправки формы )
 * @return booelan была ли перехвачена требуемая комманда
 **/
function TinyMCE_execHandle(editor_id, elm, command, user_interface, value)
{
    switch(command)
    {

    /**
     * Реализуется отображение HTML-прямо в редакторе (а не во всплывающем окне,
     * как по умолчанию).
     *
     * @todo Новый встроенный HTML-редактор мог наделать багов с вычислением
     * высоты области и установке высоты при переключении HTML/WISYWUG.
     **/
    case 'mceCodeEditor':
        var $body = $(elm).closest('body'),
            $container = $('#'+editor_id+'_parent'),
            $toolbarButton = $container.find('.mceButton.mce_code'),
            $mceIframeContainer = $container.find('.mceIframeContainer'),
            onChange = function(){ $body.html($(this).val()); };

        // селектор корневого контейнера редактора (на всю площадь которого он
        // разворачивается, становясь активным)
        var $rootContainer = $container.closest('.activeExpandPlace');

		// прячет HTML-редактор
        if($toolbarButton.hasClass('mceButtonActive'))
        {
            $toolbarButton.removeClass('mceButtonActive');
            $mceIframeContainer.find("#mceHtmlSource_back, #mceHtmlSource").remove();
            $mceIframeContainer.height($mceIframeContainer.find('iframe').height())
            $rootContainer.css('height', ($container.find('.mceLayout').height()+64)+'px');
            
        }

		// отображение редактора
        else
        {
            $toolbarButton.addClass('mceButtonActive');

            // отображает HTML-редактор
            $mceIframeContainer.css('position', 'relative');
            $mceIframeContainer.append(
                '<div id="mceHtmlSource_back" style="' +
                'position:absolute;'+
                'top:0px; left:0px; right:0px; bottom:0px;'+
                '"></div>');
            $mceIframeContainer.append(
                '<textarea autofocus id="mceHtmlSource" wrap="soft" style="' +
                'position:absolute; width:100%;'+
                'top:0px; left:0px; right:0px; resize:none;'+
                '"></textarea>');
            var $textArea = $mceIframeContainer.find("#mceHtmlSource"),
				changeSizeEvent = function(){
					TinyMCE_eventHandler({target:$body});
                    if($.browser.webkit) onChange.apply(this);
                    $mceIframeContainer.height($(this).height()+64);
                    $rootContainer.height(
                        $container.find('.mceLayout').height());					
				};

			$textArea.val($(elm).closest('body').html())
				.autosize()
				.change(onChange)
				.keydown(changeSizeEvent)
				.mousedown(changeSizeEvent);

			TinyMCE_eventHandler({target:$body});
            $mceIframeContainer.height($textArea.height()+64);
            $rootContainer.height($container.find('.mceLayout').height()); 
        }

        return true;

    /**
     * Реализует прямо сохрение из редактора напрямую при нажатии на "сохранить"
     * и CTRL+S (без пересохранения других свойств).
     **/
    case 'mceSave':

        var $tagetTextarea = $($('textarea#'+editor_id)[0]),
            setAttributeAction = $tagetTextarea.data('tinymce_forceAction'),
            message = $tagetTextarea.data('tinymce_forceActionMessage');

            if(!setAttributeAction) return false;

        $tagetTextarea.val($(elm).closest('body').html());

        var args = {id:$tagetTextarea.attr('_id'), customMessage:{message:message}, values:{}};
        args.values[$tagetTextarea.attr('_attribute')] = $tagetTextarea.val();
        $(this).request(setAttributeAction, args);

        return true;


    /**
     * Редактирование на всю страницу - своя реализация.
     * Переключает режим редактирование - в рамках корневого конейнера,
     * либо на месте.
     **/
	/*
    case 'mceFullScreen':

        var $editor = $('#'+editor_id+'_parent'),
            $toolbarButton = $editor.find('.mceButton.mce_fullscreen');

        if($toolbarButton.hasClass('mceButtonActive')){
            TinyMCE_setExpandPlace($editor, false);
        }
        else{
            TinyMCE_setExpandPlace($editor, true);
			$editor.closest('html').scrollTop(0);
        }
        
        $('.tooltip.fade').remove();
        $editor.find('iframe')[0].contentDocument.body.click();

        return true;
	*/

    }

	TinyMCE_eventHandler({target:$body, justInit:true});
    return false;
}

/**
 * Установка активного контейнера, на всю площадь которого будет развёрнут
 * конкретный TinyMCE-редактор.
 * @param element $eitor - .tinymce - контейнер редактора
 * @value boolean fullScreen - на весь экрани или нет
 **/
/*
function TinyMCE_setExpandPlace($editor, fullScreen)
{
    var $rootContainer = $editor.closest('.mceRootContainer'),
        $ownContainer = $editor.closest('.mceOwnContainer'),
        $toolbarButton = $editor.find('.mceButton.mce_fullscreen');

	//$rootContainer.css('height', 'auto');
	//$ownContainer.css('height', 'auto');

	//$ownContainer.css('height', 'auto');

	// переключение активной области
    if(fullScreen){
        $rootContainer.addClass('activeExpandPlace');
        $ownContainer.removeClass('activeExpandPlace');
        $toolbarButton.addClass('mceButtonActive');
    }else{
		$rootContainer.removeClass('activeExpandPlace');
		$ownContainer.addClass('activeExpandPlace');
		$toolbarButton.removeClass('mceButtonActive');
	}
}

/**
 * Переключает редактор в неактивное состояние при нажатии мышкой вне его пределов.
 */
/*
$(document).click(function(evt){
    // нажатие не по толбару TinyMCE
    if(!$(evt.target).closest(
		'.mceIframeContainer, #mceHtmlSource, header,.mceToolbar,.mceStatusbar, .mceStatusbar,'+
		'.mceListBoxMenu, .mceDropDown, .mceWrapper, .mceModalBlocker')
		.length){
		TinyMCE_blurAll();
    }
});
*/

/**
 * Если редактор активен, спрашивает подтверждение на закрытие.
 * Особенно это важно, учитывая что у польователя может возникнуть желание
 * нажать браузерное "назад", чтобы вернуться к форме. При этом, для наглядности,
 * редктор закрывается, возвращая польователя к форме и спрашивает разрешение на
 * уход со страницы.
 */
/*
window.onbeforeunload = function(){
	if($('.mceActive').length){
		TinyMCE_blurAll();
		return 'Do you want leave form?';
	}
}
*/
