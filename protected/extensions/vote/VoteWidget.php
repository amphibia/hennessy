<?php

class VoteWidget extends CWidget
{

    /**
     * Компонент
     * @var VoteComponent
     */
    public $voteComponent = NULL;

    /**
     * Категория, к которой привязан виджет
     * @var StructCategory
     */
    public $category = null;

    /**
     * Название поля, к которому прикреплено голосование
     * @var string
     */
    public $field = null;

    /**
     * Заголовок виджета
     * @var string
     */
    public $title = null;

    public $htmlOptions = array('class' => 'voteWidget');

    /**
     * @var array
     */
    public $assets = array(
        'js' => array('voteWidget.js'),
    );

    public function init()
    {
        if (!$this->category || !$this->category instanceof StructCategory) throw new CException(
            Yii::t('custom', get_class($this) . ' must work with StructCategory!'));
        $this->htmlOptions['data-category-id'] = $this->category->id;
        Yii::applyAssets($this, __FILE__);
    }

    public function run()
    {
        $this->render('main');
    }
}