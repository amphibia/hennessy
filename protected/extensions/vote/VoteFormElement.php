<?php

/**
 * @package vote
 * @author slaik
 *
 * @desc
 * Виджет голосования
 *
 */
class VoteFormElement extends CInputWidget
{

    /**
     * @var ActiveRecord Модель, с которой завязан этот элемент управления.
     */
    public $model;

    /**
     * @var string Поле в используемой модели, к которому привязан элемент управления.
     */
    public $attribute;

    /**
     * @var array Финальное значение, которое используется в представлении.
     */
    public $value;

    /**
     * @var array представления, используемые виджетом.
     */
    public $views = array();

    public $assets = array(
        'js' => array('voteFormElement.js'),
        'css' => array('votingFormElement.css')
    );

    /**
     * Класс валидатора, к которому идёт привязка из списка валидаторов для
     * аттрибута модели.
     * @var string
     */
    public $validatorClass = 'VoteValidator';

    /**
     * Валидатор, к котороому привязывается виджет
     * @var UploadValidator
     */
    public $validator = NULL;


    public function init()
    {
        $modelClass = get_class($this->model);
        $this->id = "vote_" . $this->attribute;

        Yii::applyAssets($this, __FILE__);
        $this->htmlOptions['id'] = $this->id;
        $this->htmlOptions['type'] = 'text';
        $this->htmlOptions['attribute'] = "{$modelClass}[{$this->attribute}]";
        $this->value = $this->model->{$this->attribute} or $this->value = 'none';

        return true;
    }

    public function run()
    {
        // поиск валидатора
        foreach ($this->model->getValidators($this->attribute) as $validator)
            if (get_class($validator) == $this->validatorClass) {
                $this->validator = $validator;
                break;
            }

        if ($this->validator) {

        } else {
            $errorMessage = Yii::t('vote',
                'Attribute for this field must have "{validator}" validator!',
                array("{validator}" => $this->validatorClass)
            );

            echo CHtml::tag('div', array('class' => 'alert error'), $errorMessage);

            return;
        }

        $this->render('formElement');

    }

}
