<?php

echo CHtml::beginForm(Yii::app()->createUrl('/vote'), 'post', array(
    'class' => 'voteWidget_vote clientContent',
    'data-vote-id' => $vote->id,
    'data-vote-field' => $this->field,
));
echo CHtml::hiddenField('hash', hash('sha256', $vote->id . $this->field . Yii::app()->vote->salt));
echo CHtml::tag('h3', array('class' => 'voteWidget_answer'), $vote->title);
echo CHtml::openTag('div', array('class' => 'voteWidget_answers'));
echo CHtml::radioButtonList('voteWidget_answer[' . $vote->id . ']', null, array_keys($voteData), array('container' => '', 'separator' => '<div class="voteWidget_sep"></div>'));
echo CHtml::closeTag('div');
echo CHtml::submitButton(Yii::t('vote', 'Vote now'));
echo CHtml::endForm();