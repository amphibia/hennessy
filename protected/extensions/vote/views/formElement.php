<div class="voteFormElement">
    <div class="alert alert-warning voteDeleted to_clone hide">
        <?= Yii::t('app', 'Element deleted.'); ?>
        <a href="javascript:;" class="cancel"><?= Yii::t('app', 'Cancel'); ?></a>
    </div>
    <div class="control-group to_clone hide">
        <?= CHtml::tag('i', array('class' => 'icon-resize-vertical move'), ''); ?>&nbsp;
        <?= CHtml::textField('Vote_answer[]', '', array('disabled' => 'disabled', 'class' => 'span4', 'rel' => 'tooltip', 'title' => Yii::t('vote', 'Answer'))) ?>
        &nbsp;
        <?= CHtml::textField('Vote_votes[]', '', array('disabled' => 'disabled', 'class' => 'span1', 'rel' => 'tooltip', 'title' => Yii::t('vote', 'Number of votes'))) ?>
        &nbsp;
        <?= CHtml::htmlButton('<i class="icon-minus icon-white"></i>', array('class' => 'btn btn-danger btn-small delete', 'rel' => 'tooltip', 'title' => Yii::t('vote', 'Remove answer'))); ?>
    </div>
    <?php
    if (is_array($this->value) && count($this->value)) {
        foreach ($this->value as $answer => $votes) {
            ?>
            <div class="control-group">
                <?= CHtml::tag('i', array('class' => 'icon-resize-vertical move'), ''); ?>&nbsp;
                <?= CHtml::textField('Vote_answer[]', $answer, array('class' => 'span4', 'rel' => 'tooltip', 'title' => Yii::t('vote', 'Answer'))) ?>
                &nbsp;
                <?= CHtml::textField('Vote_votes[]', $votes, array('class' => 'span1', 'rel' => 'tooltip', 'title' => Yii::t('vote', 'Number of votes'))) ?>
                &nbsp;
                <?= CHtml::htmlButton('<i class="icon-minus icon-white"></i>', array('class' => 'btn btn-danger btn-small delete', 'rel' => 'tooltip', 'title' => Yii::t('vote', 'Remove answer'))); ?>
            </div>
        <?php
        }
    }
    ?>

    <?= CHtml::htmlButton('<i class="icon-plus icon-white"></i>', array('class' => 'btn btn-primary btn-small add', 'rel' => 'tooltip', 'title' => Yii::t('vote', 'Add new answer'))); ?>
</div>