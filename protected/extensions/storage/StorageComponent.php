<?php

Yii::import('application.extensions.image.Image');

/**
 * @title File storage (images and documents)
 * @description { Provides operations with storage and output images in different presets. }
 * @preload yes
 * @vendor koshevy
 * @package storage
 * @requirements{component:image}
 *
 * Компонент, обеспечивающий доступ к файлам хранилища.
 *
 * Также обеспечивает доступ к изображениям в разных форматах:
 * при обращении, изображения изменяются под нужны формат (пресет) - меняются
 * размеры, накладываются водяные знаки и т.п.
 *
 * Изменённые изображения кэшириутся. В дальнейшем, при обращении по этой ссылке,
 * происходит прямая переадрессация на файл кэша.
 *
 * Для этого, компонент прикрепляет к приложению контроллер расширения,
 * обрабатывающий обращения вида:
 * /storage/local/common/uploadPreview/2012_26_06__06_47_22__485.jpg
 * /storage/local/common/w250/2012_26_06__06_47_22__485.jpg
 * /storage/local/common/w450/h520/2012_26_06__06_47_22__485.jpg
 * /storage/local/common/x25/y70/w450/h520/2012_26_06__06_47_22__485.jpg
 *
 * Для создания ссылок с учётом пресета (например, uploadPreview) либо с указанием
 * конкретных настроек (например, x25/y70/w450/h520) применяется функция
 * @see storageComponent::createUrl($fileUri, [$preset]).
 * В качестве $fileUri предполагается строка вида:
 * [ХОСТ]/<ОТНОСИТЕЛЬНЫЙ_АДРЕС_ИЗОБРАЖЕНИЯ>
 *
 * Также, компонент предполагает разбитие хранилища по различным папкам, в зависимости
 * от "контекста" - например, изображений конкретного пользователя. Контекст по
 * умолчанию: 'common'.
 *
 *
 * Используемые пресеты хранятся в @see storageComponent::$presets.
 * Они могут быть указаны в родном конфиге или конфиге 'components'.
 * Могут быть добвалены динамически.
 *
 * По умолчанию, при обращении к файлу с использованием разных настроек, проверяется
 * наличие пресета с подобными настройками. Это сделано с целью защиты от "засирания"
 * системы. Чтобы отключит эту проверку, измените свойство @see StorageComponent::$forcePreset
 * на true.
 *
 * @todo: наложение водянных знаков не реализовано;
 * @todo: возможно следует поменять Image-компонент (для работы с изображениями).
 * В данный момент используется адаптация Kohana Image для Yii, с неподным набором
 * функций, которая не поддерживается и не дописыватся.
 *
 */
class StorageComponent extends ExtCoreApplicationComponent
{
    /**
     * Директория - хранилище изображений.
     * @var string
     */
    public $storageDir = NULL;

    /**
     * Ддиректория для кэша.
     * @var string
     */
    public $cacheDir = NULL;

    /**
     * Поддериктория для конкретных задачи/пользователя.
     * @var string
     */
    public $subdir = NULL;

    /**
     * Правило корректного имени файла.
     * @var string
     */
    public $fileNameRule = NULL;

    /**
     * Правила извлечения аргументов из коммандной строки.
     * @var array
     */
    public $rules = array();

    /**
     * Настройки пресета по умолчанию.
     * @var type
     */
    public $blankPreset = array();

    /**
     * Водяной знак (если не нужен - делается FALSE).
     * @var type
     */
    public $watermark = array();

    /**
     * Пресеты (форматы) вывода файлов.
     * @var type
     */
    public $presets = array();

    /**
     * Правила извлечения аргументов из правила.
     * Примечание: 'IMAGES_DIR' в правилах будет заменено на storageComponent::$storageDir,
     * а '/' на DIRECTORY_SEPARATOR.
     * @var array
     */
    public $urlParseRules = array();

    /**
     * ID контроллера для доступа к хранилищу.
     * @return string
     */
    public $controllerID = NULL;

    /**
     * Отложенна функция, определяющая, можно ли обрабатывать данные в соответствии
     * с аргументами вне зависимости от пресетов.
     *
     * Если NULL, то нельзя.
     *
     * @var callback
     */
    public $forcePreset = NULL;

    /**
     * Псевдонимы используемого хоста.
     **/
    public $hostAliases = array();

    /**
     * Соль для формирования подписи.
     * @var string
     */
    public $signatureSalt = NULL;

    /**
     * @var Регульярное выражение для извлечения аргументнов кадрирования из
     * сохраненного имени файла.
     */
    public $attributesParseReg = NULL;

    /**
     * Настройки пользовательского кадрирования
     * @var array
     */
    public $userCropOptions = NULL;

    /**
     * Использование "прямой переброски без правил" на кэш изображения, соответствующий
     * запросу, не дожидаясь выполнения всей цепочки - загрузки остальных компнент,
     * поиску роута и т.п.
     *
     * Операция не является структурно правильной, и может привести к конфликтам
     * с другими компонентами и потери прозрачности прозрачности архитектуры приложения,
     * однако во многих случаях, может использоваться после создания и тестирования приложения для
     * увеличения скорости переброски на изображение (приблзительно в 10 раз).
     *
     * @var boolean
     */
    public $useForceRedirect = true;


    /**
     * "Прямая переброски без правил" на кэш изображения, соответствующий
     * запросу, не дожидаясь выполнения всей цепочки - загрузки остальных компнент,
     * поиску роута и т.п.
     *
     * Операция не является структурно правильной, и может привести к конфликтам
     * с другими компонентами и потери прозрачности архитектуры приложения, однако во многих
     * случаях, может использоваться после создания и тестирования приложения для
     * увеличения скорости переброски на изображение (приблзительно в 10 раз).
     */
    protected function _forceRedirect()
    {
        $uri = urldecode(Yii::app()->request->requestUri);
        if (!preg_match("/({$this->fileNameRule})/", $uri, $matches)) return;
        $fileNameBase = preg_replace("/\/storage\/\w+/", '', $uri);
        $cacheFileName = $_SERVER['DOCUMENT_ROOT'] . "/_cache$fileNameBase";

        // простой поиск - буквальное соответствие
        if (is_file($cacheFileName)) Yii::app()->request->redirect("/_cache$fileNameBase");

        // поиск с учетом настроек
        else {
            $fileName = $matches[1];
            if (!$fileName = $this->parseOptionsFileName($fileName)) return false;

            $hashString = NULL;
            foreach ($_POST['UploadsFormElement'][md5($fileName)] as $key => $value)
                if ($value) $hashString .= $key . $value;
            $hashString = md5($hashString);

            $cacheFileName = preg_replace("/({$this->fileNameRule})/", "$hashString/$fileName", $cacheFileName);
            $fileNameBase = preg_replace("/({$this->fileNameRule})/", "$hashString/$fileName", $fileNameBase);
            if (is_file($cacheFileName)) Yii::app()->request->redirect("/_cache$fileNameBase");
        }
    }

    public function init()
    {
        Yii::loadExtConfig($this, __FILE__, 'StorageOptions.php');
        Yii::app()->controllerMap[$this->controllerID] = array(
            'class' => Yii::localExtension(basename(__DIR__), 'StorageController'),
            'storage' => &$this
        );

        if ($this->useForceRedirect) $this->_forceRedirect();

        // добавление правил перенаправления
        Yii::app()->urlManager->addRules($this->rules, false);

        return true;
    }

    /**
     * Пресеты вывода изображений.
     * @param string $presetName
     * @return array
     */
    public function getImagePreset($presetName)
    {
        if (isset($this->presets[$presetName]))
            return $this->presets[$presetName];
        else return NULL;
    }

    /**
     * Обертка для обратной совместимости
     * @param string $filesData
     * @return array
     */
    public function decodeImages($filesData)
    {
        return $this->decodeFiles($filesData);
    }

    /**
     * Извлечение данных из implode/json - послеовательности.
     * @param string $filesData
     * @return array
     */
    public function decodeFiles($filesData)
    {
        // Обработка пустых значений
        if (!$filesData || $filesData == '[]') {
            return null;
        }

        if (is_array($filesData)) $result = $filesData;
        else if (!$result = json_decode($filesData, true))
            $result = explode(',', $filesData);

        return $result;
    }

    /**
     * Создание ссылки на изображение из вне.
     * @param mixed $file имя файла и относителмьный адрес оригинала (/КОНТЕКСТ/ИМЯ,
     * /_temp/КОНТЕСТ/ИМЯ, либо полный относительный путь), либо массив, в котором
     * есть ключ "fileName"
     * @param mixed $preset пресет (string) или массив с аргументами(array)
     * @return string относительный URL на изображение или NULL, если что-то не так с $fileUri
     */
    public function createUrl($file, $preset = 'default')
    {
        if (is_array($file)) {
            if (isset($file['fileName'])) $fileName = $file['fileName'];
            else return NULL;
        } else $fileName = $file;

        if (is_array($preset)) {
            $arguments = array_merge($this->blankPreset, $preset);

            // проверка существования пресета с запрашиваемыми настройками
            if (!$preset = Yii::app()->storage->presetExists($arguments))
                throw new CHttpException(404);
        }

        if (is_array($this->hostAliases) && sizeof($this->hostAliases))
            $fileName = preg_replace("@(" . implode("|", $this->hostAliases) . ")\/@", $_SERVER['HTTP_HOST'] . "/", $fileName);

        // извлечение параметров и создание нового URL
        foreach ($this->urlParseRules as $rule) {
            $rule = str_replace('STORAGE_DIR', $this->storageDir, $rule);
            $rule = str_replace('/', (DIRECTORY_SEPARATOR == '/') ? '/' : "\\\\", $rule);
            $fileName = str_replace('/', DIRECTORY_SEPARATOR, $fileName);

            $selfHost = str_replace('www.', '', $_SERVER['HTTP_HOST']);


            if (preg_match($rule, $fileName, $matches)) {

                $place = ($matches['host'] == $selfHost) ? 'local'
                    : "remote/{$matches['host']}";
                $tmpDir = $matches['tmpDir'] ? "/{$matches['tmpDir']}" : NULL;

                return "/{$this->controllerID}/{$place}{$tmpDir}/{$matches['context']}/{$preset}/{$matches['filename']}";
            }
        }

        return NULL;
    }


    /**
     * Функция, которая возвращает директорю, с которой работает текущая сессия.
     * @return Поддиректория, с которой работает текущая сессия.
     */
    public function getContext()
    {
        if (Yii::app()->hasComponent('users')) Yii::app()->users;
        return Yii::app()->hasComponent('user')
            ? ((Yii::app()->user->isGuest) ? "common" : 'user_' . Yii::app()->user->model->id)
            : 'common';
    }


    /**
     * Наложение водяного знака согласно настроек в конфиге.
     * @param $image Изображение, на которое требуется наложение.
     * @param $arguments
     * @return bool
     */
    public function addWatermark($image, $arguments)
    {
        if(!isset($arguments['watermarkImage']) || !isset($arguments['watermarkRight']) || !isset($arguments['watermarkBottom'])){
            return false;
        }
        $image->watermark(
            $arguments['watermarkImage'],
            $arguments['watermarkRight'],
            $arguments['watermarkBottom']
        );
    }

    /**
     * Во избежание "засирания" диска запросами создаваемым кэшем,
     * предполагаемые к использованию настройки следует объявлять в конфиге.
     * @param array $arguments
     * @return string имя пресета с соответсвующими настройками.
     */
    public function presetExists($arguments)
    {
        // неучитываемые в пресетах данные
        unset($arguments['request'], $arguments['filename']);

        if (is_callable($this->forcePreset))
            if (call_user_func($this->forcePreset)) return true;

        // поиск пресета
        foreach ($this->presets as $key => $preset) {
            $preset = array_merge($this->blankPreset, $preset);
            if (!sizeof(array_diff_assoc($preset, $arguments)))
                return $key ? $key : true;
        }

        return false;
    }


    /**
     * Изменение файла в соостветствии с указанными настройками и сохранение кэша.
     * @param type $filePathOrginal оригинал файла (относительный адрес)
     * @param type $filePathCache адрес сохранения кэш-файла (относительный)
     * @param type $arguments настройки изменения файла
     * @return boolean результат операции
     */
    public function createCacheLocal($filePathOrginal, $filePathCache, $arguments)
    {
        $filePathOrginal = $_SERVER['DOCUMENT_ROOT'] . "/$filePathOrginal";

        if (!is_file($filePathOrginal))
            throw new CHttpException(404, Yii::t('storage', 'Source file not found.'));

        /** @TODO сейчас нельзя ограничивать изображение по большей стороне,
         * а ресайз делается только по ширине */

        // объект работы с изображениями (используется адаптированный
        // Image-модуль от Kohana)
        $image = Yii::app()->image->load($filePathOrginal);

        $fullCachePath = $_SERVER["DOCUMENT_ROOT"] . $filePathCache;
        if (!file_exists($cahcheDir = dirname($fullCachePath))) mkdir($cahcheDir, 0777, true);

        // перед применением пресета применяются настройки
        // пользовательского кадрирования
        if ($this->userCropOptions) {

            $imageWidth = $image->width;
            $imageHeight = $image->height;

            $userCropWidth = round($imageWidth / 100 * $this->userCropOptions['width']);
            $userCropHeight = round($imageHeight / 100 * $this->userCropOptions['height']);
            $userCropX = round($imageWidth / 100 * $this->userCropOptions['x']);
            $userCropY = round($imageHeight / 100 * $this->userCropOptions['y']);

            //print_r(array($userCropX, $userCropY, $userCropWidth, $userCropHeight)); die();
            $image->crop($userCropWidth, $userCropHeight, $userCropY, $userCropX);
        }

        $image->save($fullCachePath);
        $image = Yii::app()->image->load($fullCachePath);

        extract($arguments);

        // Если используется режим по одной стороне, ограичение происходит
        // по большей стороне, свойства  высота и ширина игрорирутся.
        if ($side) {
            if ($image->height > $image->width)
                $image->resize(NULL, $side);
            else $image->resize($side, NULL);
        } else {
            if ($height) {
                // вписывание в высоту
                if ($width) {
                    if (($image->height / $height) < ($image->width / $width))
                        $image->resize(NULL, $height);
                    else $image->resize($width, NULL);

                    $image->crop($width, $height, $y ? $y : 'center', $x ? $x : 'center');
                } else {
                    $image->resize(NULL, $height);
                }
            } else if ($width) $image->resize($width, NULL);
        }

        // наложение водного знака
        $this->addWatermark($image, $arguments, $preset);

        $image->save($fullCachePath);

        // переадресация на только-что созданный кэш
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: $filePathCache");
        exit;

    }

    /**
     * Извлечение параметров из имени файла.
     * Параметры (если они есть) сохраняются в $_POST['UploadsFormElement'][md5(FILENAME)],
     * возвращается имя файла без параметров.
     *
     * @param string $fileName
     * @param mixed $signature - для возвращения подписи
     * @return string оригинальное имя файла (без параметров)
     */
    public function parseOptionsFileName($fileName, &$signature = NULL)
    {
        if (!isset($_POST['UploadsFormElement'])) $_POST['UploadsFormElement'] = array();

        if (preg_match($this->attributesParseReg, $fileName, $matches)) {
            extract($matches);

            // если подпись правильная, параметры применяются
            if ($signature == $this->createSignature($x, $y, $width, $height))
                $_POST['UploadsFormElement'][md5($matches["fileName"])] = compact(
                    'x', 'y', 'width', 'height'
                );

            return $fileName;
        } else {
            if (!isset($_POST['UploadsFormElement'][md5($fileName)]))
                $_POST['UploadsFormElement'][md5($fileName)] = array(
                    'x' => NULL, 'y' => NULL, 'width' => NULL, 'height' => NULL
                );

            return $fileName;
        }
    }

    /**
     * Создание подписи для подтверждения параметров, включенных в имя файла.
     *
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     */
    public function createSignature($x, $y, $width, $height)
    {
        return md5($this->signatureSalt . $x . $y . $width . $height . "_");
    }

    /**
     * Извлечение данных из поля, хранящего данные о файлах в implode-
     * последовательности или JSON-формате.
     *
     * ПРИМЕЧАНИЕ: имена файлов требуют дополнительной обработки с помощью
     * метода createUrl()
     *
     * @param mixed $fieldValue значение поля, хранящего данные о файлах
     * @return array массив вида [['fileName', 'title', 'description'], ...] или NULL
     */
    public function parseFieldData($fieldValue)
    {
        // превращение значения поля в массив данных
        if (!is_array($fieldValue)) {
            if (!is_array($data = json_decode($fieldValue, true))) {
                $data = explode(',', $fieldValue);
            }
        } else $data = $fieldValue;

        if (!is_array($data)) return NULL;
        $result = array();

        // приведение к одному формату данных
        foreach ($data as $element) {
            if (is_array($element))
                $result[] = $element;
            else $result[] = array(
                'fileName' => $element,
                'title' => NULL,
                'description' => NULL
            );
        }

        return $result;
    }

    /**
     * Подготовка массива с данными для хранения в базе.
     * Часть настроек будет вшита непосредственно в имя файла (настройки
     * кадрирования), а название и описание - будут храниться в отдельных полях.
     *
     * @param type $fileName
     * @param array $options привязанные данные - title, description, x, y, width, height
     */
    public function encodeFileData($fileName, $options)
    {
        // координаты будут храниться непосредственно в имени файла
        // (если обрезка изменяет исходные координаты 0,0,100,100)
        extract($options);
        if (($x <> 0) || ($y <> 0) || ($width <> 100) || ($height <> 100)) {
            $signature = $this->createSignature($x, $y, $width, $height);
            $encodedFileName = "$fileName|$x|$y|$width|$height|$signature";
        } else $encodedFileName = $fileName;

        return array(
            'fileName' => $encodedFileName,
            'title' => isset($options['title']) ? $options['title'] : NULL,
            'description' => isset($options['description']) ? $options['description'] : NULL,
            'x' => $x, 'y' => $y, 'width' => $width, 'height' => $height
        );
    }
}

?>
