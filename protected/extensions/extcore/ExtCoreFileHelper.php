<?php

/**
 *
 * Хелпер для работы с файлами.
 *
 *
 */
class ExtCoreFileHelper extends CComponent
{

    /**
     * Get formated file size
     *
     * @param $filepath Full path to the file on server
     * @return string
     */
    public static function get_filesize($filepath)
    {
        if (!is_file($filepath)) {
            return false;
        }
        $dsize = filesize($filepath);
        if (strlen($dsize) <= 9 && strlen($dsize) >= 7) {
            $dsize = number_format($dsize / 1048576, 1);
            return "$dsize MB";
        } elseif (strlen($dsize) >= 10) {
            $dsize = number_format($dsize / 1073741824, 1);
            return "$dsize GB";
        } else {
            $dsize = number_format($dsize / 1024, 1);
            return "$dsize KB";
        }
    }
}