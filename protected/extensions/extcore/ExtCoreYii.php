<?php

require($yiiBase);

/**
 * Yii is a helper class serving common framework functionalities.
 *
 * It encapsulates {@link YiiBase} which provides the actual implementation.
 * By writing your own Yii class, you can customize some functionalities of YiiBase.
 *
  */
class Yii extends YiiBase
{

	/**
	 * Перевод технической лексемы (название поля, действия и т.п.) фразой
	 * на человоекопонятном языке с переводом на текущий язык. 
	 *
	 * @param string $lexem техническая лексема
	 * @param string $context контекст (attributes, actions и т.п.)
	 * @param boolean $translate требуется ли перевод на текущий язык
	 * @param array $additionalDictionary
	 * return 
	 */
	public static function label($lexem, $context, $translate = true, $additionalDictionary = array())
	{
		$value = NULL;
	
		if(isset($additionalDictionary[$context])){
			if(isset($additionalDictionary[$context][$lexem]))
				$value = $additionalDictionary[$context][$lexem];
		}
		
		if(!$value && isset(Yii::app()->dictionary[$context][$lexem]))
			$value = Yii::app()->dictionary[$context][$lexem];
			
		// "топорный перевод" - если не найдено требуемое значение
		if(!$value) $value = ucfirst(preg_replace("/_/", " ", $lexem));
        if($translate) $value = Yii::t($context, $value);
		
		
		return $value;
	}


    /**
     * Расширенная подгрузка конфига:
     * сначала обращается к данным из локального конфига расширения, затем
     * к данным из Yii::app()->params(), перезаписывая их поверх.
     * 
     * В случае с внутренним конфигом приложения, перезаписываются только те
     * свойства, которые равны NULL или array() - пустой массив; в противном
     * случае окажутся перезаписанными настройки компонент приложения, переданные
     * при инициализации в app.config.components.php. 
     * 
     * @param CComponent $component обект, в который будет считываться конфиг
     * @param string $componentDirectory местоположение расширения (__FILE__)
     * @param string $configFile конкретный файл в директории (по умолчанию файл
     * одноименный с классом кмпонента); также модно указать "*" для чтения из
     * всех файлов директории ./config.
     */
    public static function loadExtConfig(CComponent $component, $componentPath, $configFile = NULL)
    {   
        $className = get_class($component);
        if(!$configFile) $configFile = $className.".php";

        
        $relDir = dirname($componentPath);

        $configFiles = array_merge(
            ($config = glob("{$relDir}/config/{$configFile}")) ? $config : array(),
            ($config = glob("{$relDir}/../config/{$configFile}")) ? $config : array()
        );

        // чтение конфигурационных файлов  в директории расширения
        if($configFiles)
        {   
            foreach($configFiles as $configFileMatch)    
                
                // применение конфига
                foreach(require $configFileMatch as $key => $value) 
                {
                    // только для пустых элементов
                    if( !isset($component->$key) ||
                        is_null($component->$key) ||
                        (is_array($component->$key) && !sizeof($component->$key)))
                        $component->$key = $value;
                    
                    // неоднозначные значения смешиваются
                    else if(is_array($component->$key) && is_array($value))
                        $component->$key = array_merge($value, $component->$key);
                }
        }

        // записывает поверх из конфига приложения
		if(is_file($fileName = Yii::getPathOfAlias('application').'/config/classes/'.$className.'.php')){
			$secondConfig = require $fileName;
		}
		else if(isset(Yii::app()->params[$className]))
			 $secondConfig = Yii::app()->params[$className];
		else $secondConfig = array();

		foreach($secondConfig as $key => $value)
			$component->$key = $value;

    }


    /**
     * Применение ассетсов компонента.
     * Собирает данные из свойства $assts["js", "css", "core"] объекта и публикует их.
     * 
     * @param CComponent $component
     * @param type $filePath местополжение объекта (используйте __FILE__)
     * 
     * @return void
     */
    public static function applyAssets(CComponent $component, $filePath)
    {
		if(isset($component->assets))
		{
			// опубликованные ресурсы компонента
			if(!isset($component->assets["directory"]))
			{
				if(!file_exists($assetsDirectory = dirname($filePath).DIRECTORY_SEPARATOR.'assets')){
					if(!file_exists($assetsDirectory = dirname($filePath).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR.'assets'))
						$assetsDirectory = NULL;
				}

				if($assetsDirectory)
					$component->assets["directory"] = Yii::app()->assetManager->publish($assetsDirectory);
			}

			if(isset($component->assets["css"]))
			foreach($component->assets["css"] as $css)
			{
				// если адрес начинается с "/", значит адрес абсолютный, если нет,
				// относительно ассетсов расширения
				if(!preg_match("/^\/.*$/", $css))
					$css = $component->assets["directory"]."/css/$css";
	
				Yii::app()->clientScript->registerCssFile($css);
			}
	
			if(isset($component->assets["js"]))
			foreach($component->assets["js"] as $js)
			{
				// если адрес начинается с "/", значит адрес абсолютный, если нет,
				// относительно ассетсов расширения
				if(!preg_match("/^\/.*$/", $js))
					$js = $component->assets["directory"]."/js/$js";
	
				Yii::app()->clientScript->registerScriptFile($js);
			}

			if(isset($component->assets["pure"]))
			foreach($component->assets["pure"] as $js)
			{
				// если адрес начинается с "/", значит адрес абсолютный, если нет,
				// относительно ассетсов расширения
				if(!preg_match("/^\/.*$/", $js))
					$js = $component->assets["directory"]."/js/$js";
	
				Yii::app()->clientScript->registerScriptFile($js, ExtCoreClientScript::POS_HEAD_PURE);
			}
			
			if(isset($component->assets["core"]))
			foreach($component->assets["core"] as $core)
				Yii::app()->clientScript->registerCoreScript($core);
		}
    }

    /**
     * Путь до расширения (в контексте модуля или всего приложения).
     * @param string $extensionName псевдоним расширения (должен совпадать с именем директории)
     * @param string $pathPrefix путь к конкретному элементу расширения (обычно, имя класса)
     * @param string $curDirectory текущая директория (указывать __FILE__, если используются сервисы)
	 * @return string
     */
    public static function localExtension($extensionName, $pathPrefix = NULL, $serviceDirectory = NULL)
	{
		// кэш адресов использованных расширений
		static $usedExtensions = array();

		if(!isset($usedExtensions[$extensionName]))
		{
			// расширение по на уровне приложений (наиболее приоритетно)
			if(!file_exists(Yii::getPathOfAlias($localDir = strtolower("application.extensions.{$extensionName}")))){
				// расширение в контексте использоваемого модуля
				if($module = Yii::app()->activeModule)
					$localDir = strtolower("application.modules.{$module->id}.extensions.{$extensionName}");
                // расширение в контексте модуля сервиса
                if($serviceDirectory && !file_exists(Yii::getPathOfAlias($localDir))){
                    $relativePath = str_replace(
                        Yii::getPathOfAlias('application').DIRECTORY_SEPARATOR,
                        '', $serviceDirectory);
                    
                    $pathItems = explode('/', $relativePath);
                    if(($item = current($pathItems)) == 'modules')
                        $item .= ($module=next($pathItems))?".$module":NULL;                    
                    $localDir = strtolower("application".($item?".$item":NULL).".extensions.{$extensionName}");

                }
			}

			$usedExtensions[$extensionName] = $localDir;
		}

		return $usedExtensions[$extensionName]  . ($pathPrefix ? ".$pathPrefix" : NULL);
	}

}