<?php

/**
* Контроллер - посредник при обращении к сервису.
*/
class ExtCoreServicesController extends Controller
{
	protected function beforeAction($action){
		if(!isset($_GET["args"]) || !$_GET["args"]) $_GET["args"] = array();
		else $_GET["args"] = explode('/', trim($_GET["args"],"/"));
		return parent::beforeAction($action);
	}

	/**
	 * Обращение к сервису.
	 * @param $serviceID
	 * @param $actionID
	 * @param $args аргументы, передаваемые сервису
	 * @return void
	 */
	public function actionEnter($serviceID, $actionID, Array $args = array()){
		$service = Yii::app()->getService($serviceID);
		if(!$this->layout){
			if($this->layout) $service->layout = $this->layout; else
			if($module=$this->getModule()) $service->layout = $module->layout;
		}
        Yii::app()->controller = $service;
        Yii::app()->controller->lastActionID = $actionID;
		$this->serviceInint();
        $service->runWithParams($actionID, $args);
	}

    /**
     * Подготовка контроллера перед стартом.
     */
	public function serviceInint(){
	}
}