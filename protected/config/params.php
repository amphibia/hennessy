<?php

// application-level parameters that can be accessed
// using Yii::app()->params['paramName']

return array
(
	// показывать ошибки в режиме отладки не только супер-пользователям
	'showErrorsForAll' => true,

	// сообщения о системных ошибках
	'systemErrors' => array(
		'400' => Yii::t('app', 'Request is not valid.'),
		'403' => Yii::t('app', 'Access is denied.'),
		'404' => Yii::t('app', 'Cant find requested page.'),

		'500' => Yii::t('app', 'Sorry! We have some problems. We will solve this problem in the near future. Please, try again later.'),
		'501' => Yii::t('app', 'Sorry! We have some problems. We will solve this problem in the near future. Please, try again later.'),
		'502' => Yii::t('app', 'Sorry! We have some problems. We will solve this problem in the near future. Please, try again later.'),
	),


    /** @category Настройки постраничного вывода. */

    // GET-параметр, в котором передаётся поле сортировки
    "defaultSortVar" => 'orderBy',

    // аргумент для createUrl, в котором указывается номер страницы;
    // если NULL, используется по умолчанию (для pagination в CActiveDataRecords
    // отличается от стандартного "page")
    "defaultPageVar" => 'page',


    "pagination" => array
    (
        "tinySize"      => 5,
        "smallSize"     => 10,
        "mediumSize"    => 16,
        "largeSize"     => 25,
        "hugeSize"      => 50,
        "boundedSize"   => 100,
    ),

    // количество кнопок  в постраничном выводе
    "paginationButtonCount"    => 13,


    /** @category Расширенные настройки даты и времени в приложении. */
    
    // формат дат, используемый при выводе на сайте
    "dateFormat" => "d F, H:i",
    "dateFormatShort" => "d-m-Y H:i",
    
    // заготовка аргументов для выборки по дате (начальная дата, по сути - 
    // старт периода приложения).
	"blankStartDate" => array
    (
        "year" => 2012, "month" => 1, "day" => 1,
        "hour" => 0, "minute" => 0, "second" => 0
    ),

    // заготовка для конечной даты (день нужно выставлять динамически)
    "blankEndDate" => array
    (
        "year" => date("Y"), "month" => 12, "day" => NULL,
        "hour" => 23, "minute" => 59, "second" => 59
    ),

    
    /** @category
     * 
     *  Автоматическая предобработка полей (реализована в классе ActiveRecord,
     *  потомке СActiveRecord).
     */

    // поля, по которым будет осуществляться LIKE-поиск (метод search()) 
    "partialMatch" => array
    (
        'title', 'alias', 'announce', 'keywords', 'gallery', 'text',
        'status', 'data', 'comment_text', 'username',
    ),

    // Cортировка по умолчанию. Будет найдено первое условие, поле которого
    // присутсвует в модели. Можно указывать не более одного поля (это ж по
    // умолчанию условие).
    //
    // Если не указывать 't', в JOIN-запросах может произойти конфликт полей.
    "defaultScopes" => array
    (
        'username'      => array('order' => 'username'),
        'position'      => array('order' => 'position'),
        'date_created'  => array('order' => 't.date_created DESC'),
        'date'          => array('order' => 't.date DESC'),
        'id'            => array('order' => 'id DESC')
    ),

    /** @category настройки RBAC */
    "rbacDefaultOperation" => "Controllers",


    'storageComponent' => array
    (
        // пресеты изменения файлов
        "presets" => array
        (
            // здесь только стандартные размеры
            // дополнительные размеры указывайте в общем конфиге или динамически -
            // в подключаемых зависимых конпонентов
            'default' => array(),
            'avatarLarge' => array('width' => 42, 'height' => 42),
			'cropPreview' => array('side' => 450),

            'uploadPreview' => array('width' => 96, 'height' => 96),
            '_1to2_small' => array('width' => 96, 'height' => 192),
            '_2to1_small' => array('width' => 192, 'height' => 96),
			'_long_small' => array('width' => 230, 'height' => 96),

			'newsSmall' => array('width' => 302, 'height' => 227),
			'galleryLarge' => array('width' => 940, 'height' => 380),
			'galleryPreview' => array('width' => 98, 'height' => 98),
        ),

		'hostAliases' => array('nissan'),
    ),

	'StructPage' => array('autoAlias' => 'true'),

	
	'LangComponent' => array(
		'languages' => array(
			'ru' => array( 'short' => 'Рус', 'long' => 'Русский' ),
		),
	),

);

