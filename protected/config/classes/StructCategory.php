<?php

return array
(
	'templateRange' => array
	(
		'default' => 'Default content page',
		'mainpage' => 'Main page',
		'about' => 'О проекте',
        'interview' => 'Интервью',
		'tastes' => 'Одиссея вкусов',
		'news' => 'Календарь событий',
	)
	
);
