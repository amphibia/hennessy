<?php
return array(
    'useSubDomains' => false,
    'defaultLanguage' => 'ru',
    'languages' => array(
        'en' => array('short' => 'Eng', 'long' => 'English'),
        'ru' => array('short' => 'Рус', 'long' => 'Русский'),
        //'kk' => array('short' => 'Каз', 'long' => 'Казахский'),
    )
);