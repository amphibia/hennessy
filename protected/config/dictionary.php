<?php

return array
(
	"actions" => array
	(
		"index"		=> "Index",
		"edit"		=> "Edit",
		"update"	=> "Update",
		"delete"	=> "Delete",
		"remove"	=> "Remove",
		"add"		=> "Add",
		"search"	=> "Search",
	),
	
	"attributes" => array
	(
		"id"				=> "#ID",
		"name"				=> "Name",
		"first_name"		=> "Firstname",
		"firstname"			=> "Firstname",
		"last_name"			=> "Lastname",
		"lastname"			=> "Lastname",
		"user"				=> "Username",
		"username"			=> "Username",
		"user_name"			=> "Username",
		"age"				=> "Age",
		"phone"				=> "Phone",
		"tel"				=> "Phone",
		"telephone"			=> "Phone",
		"mob_phone"			=> "Mobile phone",
		"mobile_phone"		=> "Mobile phone",
		"mobile_tel"		=> "Mobile phone",
		"mobile_telephone"	=> "Mobile phone",
		"mob_tel"			=> "Mobile phone",
		"mob_telephone"		=> "Mobile phone",
		"email"				=> "Email",
		"url"				=> "URL",
		"value"				=> "Value",
		"title"				=> "Title",
		"text"				=> "Text",
		"announce"			=> "Announce",
		"filename"			=> "File",
		"file_name"			=> "File",
		"busy"				=> "Busy",
		"published"			=> "Published",
		"copyright"			=> "Copyright text",
		"comments"			=> "Comments",
		"descr"				=> "Description",
		"description"		=> "Description",
		"data"				=> "Compressed data",
		"date_created"		=> "Creating date",
		"date"				=> "Creating date",
		"date_modify"		=> "Modify date",
	),
	
	"component" => array(),
	"module" => array(),
);