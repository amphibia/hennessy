<?php
return array(
	'from_email' => 'test@test.com',
	'to_email' => 'test@test.com',
	'bcc' => '',
	'type' => 'smtp',
	'username' => 'test',
	'host' => 'mail.test.com',
	'port' => '25',
	'password' => '',
	'auth' => '1',
	'secure' => 'none',
	'debug' => '1',
);
?>