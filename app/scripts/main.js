(function() {

	var $slider = $('.slider');

	$slider.each(function () {
		var _self = $(this);

		var slider = new Swiper(_self.find('.swiper-container'))
		_self.find('.btn-slide-next').on('click', function () {
			slider.slideNext();
		})
		_self.find('.btn-slide-prev').on('click', function () {
			slider.slidePrev();
		})
	})
	
	function detectmob() {
		if(window.innerWidth <= 500 && window.innerHeight <= 800) {
		  return true;
		} else {
		  return false;
		}
	 }
	   window.isMobile = detectmob();
	   if(window.isMobile) {
			$('#tastes-video').attr('controls', '');
		}
} )();

(function($) {
	
	$.fn.equalHeights = function() {
		var maxHeight = 0,
			$this = $(this);

		$this.each( function() {
			var height = $(this).innerHeight();

			if ( height > maxHeight ) { maxHeight = height; }
		});

		return $this.css('height', maxHeight);
	};

	// auto-initialize
	$('[data-equal]').each(function(){
		var $this = $(this),
			target = $this.data('equal');
		$this.find(target).equalHeights();
	});

})(jQuery);
