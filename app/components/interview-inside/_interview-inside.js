(function(){
	var interviewPromoHeight = $('.interview-inside-promo').height();
	var figures = $('.interview-animate-figures');
	$(window).on('load', function(){
		$('.interview-inside-promo-desc').addClass('animate-on');
	});
	$(window).on('scroll', function(){
		var scrollTop = $(this).scrollTop();
		if (scrollTop >= interviewPromoHeight) {
			figures.addClass('animate-on');
		}
	});
})();