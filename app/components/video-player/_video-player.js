(function(){
    var video = document.getElementById('video');
    
    $('#modalVideo').on('show.bs.modal', function (e) {
      video.play();
    })

    $('#modalVideo').on('hide.bs.modal', function (e) {
      video.pause();
    })
    
})();