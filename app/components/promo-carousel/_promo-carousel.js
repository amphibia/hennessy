(function () {
    var $promoCarousel = $('.promo-carousel');
    
    $promoCarousel.each(function () {
        var _self = $(this);

        var slider = new Swiper(_self.find('.swiper-container'))
        _self.find('.promo-carousel-next').on('click', function () {
            slider.slideNext();
        })
        _self.find('.promo-carousel-prev').on('click', function () {
            slider.slidePrev();
        })
    })
})();