(function(){
	var currentYear = (new Date()).getFullYear();
	var validAge = 18;
	var validYear = currentYear - validAge;
	var btn = $('.select-success-btn').find('.btn-success');
	var url = btn.data('url');

	$(window).on("load",function(){
    	$(".selectric-scroll").mCustomScrollbar({
    		axis:"yx",
    		theme:"light"
    	});
    });

	$('.index-select-items').find('.lang-select').selectric({
		nativeOnMobile: false,
	 	onChange: function(elem) {
	 		var currentVal = $(elem).val();
	 		btn.attr('data-url', url + '?language=' + currentVal);

	 		$(".selectric-scroll").mCustomScrollbar("destroy");
	 		$('.date-select').removeAttr('disabled').selectric('refresh');
	 		$(".selectric-scroll").mCustomScrollbar({
	    		axis:"yx",
	    		theme:"light"
	    	});
	  	}
	});
	$('.index-select-items').find('.date-select').selectric({
		disabled: true,
		nativeOnMobile: false,
		onInit: function(elem) {
			console.log(elem)
		},
	 	onChange: function(elem) {
	 		var currentDate = $(elem).val();
	 		if (validYear >= currentDate) {
	 			btn.removeAttr('disabled')
	 		} else {
	 			btn.attr('disabled','disabled')
	 		}
	  	}
	});

	btn.on('click', function(e){
		var dataUrl = $(this).attr('data-url');
		document.location.href = dataUrl;
	});

 	


})();

