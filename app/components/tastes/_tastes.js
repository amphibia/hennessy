(function () {
    var tastesCarousel = new Swiper($('.tastes-promo .swiper-container'), {
        loop: true,
        effect: 'fade'
    });
    $('.tastes-carousel-next').on('click', function () {
       	tastesCarousel.slideNext();
    });
    $('.tastes-carousel-prev').on('click', function () {
        tastesCarousel.slidePrev();
    });

    if($(".tastes-promo").length) {
        var video = document.getElementsByTagName('video')[0];
        
        video.play();
    }

    $tastesItem = $('.tastes-items').find('.item-desc');

    $tastesItem.equalHeights();
    
})();