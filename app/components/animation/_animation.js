(function(){
	var promoSlide = $('.promo-slide');
	var peopleSlide = $('.section-info-slides').find('.info-slide');
	
	peopleSlide.addClass("animated-disabled").viewportChecker({
		classToRemove: 'animated-disabled',
        classToAdd: 'animated-active',
        offset: 300,
        // repeat: true,
        invertBottomOffset: true
    });

})();