/**
 * @author koshevy
 *
 * Функционал для упрощения обмена между серверной и клиент - частью.
 *
 * Основная часть - функция $(selector).request(url, post, callbackBefore, callbackAfter),
 * которая производит запрос из контекста элемента, автоматически определяя
 * тип ответа: HTML(DOM), TEXT, JSON или XML.
 *
 * Если приходит HTML или простой текст, элемент может быть заполнен результатом
 * (если имеет класс 'clientContent', что содержимое поля модно менять напрямую).
 *
 * По умолчанию, поле может обрабатывать только XML/JSON-запросы, а любой приход
 * текста подразумевается, как исключение.
 *
 * Также, в дополнении с 'clientContent', можно указывать следующие классы, меняющие
 * метод заполнения:
 *
 * 'onBeforeRequest' - перед отправкой запроса от имени элемента,
 * 'clientContentAppend' - добавление к уже существующему содержимому элемента (в начало);
 * 'clientContentPrepend' - добавление к уже существующему содержимому элемента (в конец);
 * 'clientContentSlideLeft' - меянет содержимое с эффектом перелистывания влево;
 * 'clientContentSlideRight' - меянет содержимое с эффектом перелистывания вправо;
 * 'clientContentReplace' - элемент полностью заменяется новым содержимым.
 *
 *
 * JSON-ответ может содержать стандартные комманды взаимодействия между клиентом
 * и сервером. Команды реализованы в этом плагине, и могут быть расширены в
 * объекте $.client.serverCommands.
 *
 * JSON - комманды приходят в системном свойстве ответа __COMMANDS.
 *
 * $.client.config содержит конфигурацию клиента: параметры запросов, данные,
 * отправляемые с каждым запросом (например, если потребуется отправлять токен).
 *
 * Также, система следит за очередью запросов, при необходимости, блокируя
 * страницу и/или элемент, от имени которого идёт запрос.
 *
 * Если потребуется блокировать страницу, расширьте функции:
 * $.client._lockPage и $.client._unlockPage.
 *
 * Чтобы настроить вывод сообщений, переопределите $.client._message.
 *
 * Для упрощения сбора данных в формах, и не только, используется функция
 * $(form).getFields().
 *
 *
 * @todo: Не реализована работа с XML.
 * @todo: Также, предполагается реализация отслеживания смены хэша (с помощью нового
 * события в HTML5, и его иммитация там, где onhashchange не доступно). На данный
 * момент - начато, но не закончено.
 *
 *
 */
jQuery.extend({

    /**
     * @var центральный объект
     */
    client: {
        /**
         * @var объект конфигурации
         */
        config: {
            // установленное рабочее поле (по умолчанию - #content)
            workPlace: null,

            // регулрное выражение, с помощью коорого обрабатывается URI
            // (по умолчанию исползуется рег #<FIELD_ID>/<URI>
            uriParsingReg: new RegExp('^#([a-zA-Z0-9_]{1,})(\/{1}([a-zA-Z0-9_\.]+\/{1})+)$'),
            uriParsingRegArguments: Array("query", "field_id", "uri"),


            // регулярное выражение, справедливое для имён элементов-массивов, типа ABC[]
            arrayNameReg: new RegExp('^([a-zA-Z0-9_]+)(\[[a-zA-Z0-9_]+\])$'),

            changeUrlOnAjax: true,


            // единные настройки для AJAX - запросов
            ajaxOptions: {
                timeout: 60000,
                type: "POST",
                cache: false,

                data: {use_ajax: true, client_system: true}
            },

            // расширенные настройки (выходящте за рамки аргументов
            // функции JQuery.ajax, которая используется для AJAX-вызовов)
            ajaxAdvancedOptions: {
                callbackBefore: function (request_result, requestOptions) {
                },
                callbackAfter: function (request_result, requestOptions) {
                }
            },


            // настойки отслеживания URI
            handleURI_start: true,
            handleURI_interval: 100,
            handleURI: true,

            // сообщения клиентской системы
            messages: {
                'request_error': "error requesting to server",
                'command_error': 'error command'
            },


            // настройка сбора данных по полям
            fieldsGrabber: {
                // простые типы данных (как есть)
                selectorSimple: "input:text, input:password, input:hidden, input:checkbox, textarea, select",

                // простой выбор из нескольких значений
                selectorOption: "input:radio[name]",

                // обычные данные (не поля формы)
                selectorData: ".clientData[id]"
            },

            // костыли :)
            dogNail: {
                // скорости в эффекте перелистывания
                slideTime: 600
            },

            /**
             * Порядок интерпретирования команд
             * если функция команды возвращает true,
             * исполнение команд идёт дальше, если false,
             * очередь прерывается.
             *
             * Командам требуются следующие данные: значение комманды,
             * оригинал ответа от сервера, список уже выполненных команд,
             * элемент, от имени которого выполнялся запрос (this).
             **/
            serverCommands: {

                confirmation: function (value, requestResult, lastCommands, url) {
                    if (confirm(value)) $(this).request(url, {json_confirm: true});
                    return true;
                },

                refresh: function (value, requestResult, lastCommands, url) {
                    return true;
                },

                // замена элемента присланным содержимым
                replace_element: function (argument, requestResult, lastCommands, url) {
                    for (i in argument)
                        $.client.element(argument[i].element).replaceWith(argument[i].value);
                    return true;
                },

                // заполнить элемент
                fill_element: function (argument, requestResult, lastCommands, url) {
                    $.client.element(argument.element).fill(argument.value);
                    return true;
                },

                error: function (argument, requestResult, lastCommands, url) {
                    for (i in argument)
                        $.client.message("error", argument[i]);
                    return true;
                },

                message: function (argument, requestResult, lastCommands, url) {
                    for (i in argument)
                        $.client.message("info", argument[i]);
                    return true;
                },

                redirect: function (argument, requestResult, lastCommands, url) {
                    return true;
                },

                history_back: function (argument, requestResult, lastCommands, url) {
                    return true;
                },

                history_next: function (argument, requestResult, lastCommands, url) {
                    return true;
                }

            }


        },

        /**
         * инициализация и запуск клиентской части
         */
        run: function () {
            // включение отслеживания смены URI
            //if($.client.config.handleURI_start)
            //	setInterval('$.client._checkForChangeURI()', $.client.config.handleURI_interval);

            if ($.client.config.changeUrlOnAjax) {
                window.addEventListener('popstate', function (evt) {
                    if ($.browser.webkit && !$('body').hasClass('historyPushed')) {
                        $('body').addClass('historyPushed');
                        return;
                    }

                    if (evt.state && evt.url && evt.elementId) {
                        $.client.config.changeUrlOnAjax = false;
                        $('#' + evt.elementId + ", [name='" + evt.elementId + "']")
                            //.addClass('clientContentSlideRight')
                            .request(location.href, {}, null,
                            function () {
                                $.client.config.changeUrlOnAjax = true
                            });
                    }

                }, false);
            }
        },

        // получение ID следующей операции, с возможностью привязать
        // функцию-обработчик на завершение операции (вызывается operationApplied(id))
        nextOperationId: function (handler) {
            var operationId = $(document).data('operationId');
            if (!operationId) operationId = 1;
            $(document).data('operationId', operationId + 1);

            if (handler) $(document).bind('operation_' + operationId, handler);

            return operationId;
        },

        // пометка, что операция была завершена
        operationApplied: function (operationId) {
            $(document).trigger('operation_' + operationId);
        },

        /**
         * упрощённое обращение к элементу со стороны сервера
         * @param string имя элемента, идентификатор или JQuery селектор
         */
        element: function (elementLink) {
            // наиболее приротетная ссылка по имени
            if ($("[name=" + elementLink + "]").length) return $("[name=" + elementLink + "]");

            // если нет элемента с указанным именем,
            // значит требуется ссылка по ID
            if ($("#" + elementLink).length) return $("#" + elementLink);

            // в самом последнем случае - это CSS-селектор
            return $(elementLink);
        },

        /**
         * @var область открытого в данный момент диалога
         */
        _activeDialog: null,


        /**
         * @return рабочая область
         */
        workPlace: function () {
            return $.client.config.workPlace
                ? $.client.config.workPlace : $("#content");
        },

        /**
         * @return активная область страницы - это может быть область с
         * конентом, либо область открытого в данный момент диалога
         */
        activePlace: function () {
            return $.client.config._activeDialog
                ? $.client.config._activeDialog : $.client.config.workPlace();
        },


        /**
         * @var послений изменённый URI
         */
        _lastURI: null,


        /**
         * проверка смены URI
         * выполняется через определённый интервал (в зависимости от настроек в конфиге)
         * и отслеживает изменения URI; если URI поменялся, берёт его и пытается интерпретировать
         */
        _checkForChangeURI: function () {

            // обработка URI выключена
            if (!$.client.config.handleURI) return;

            // проверка  URI по правилу (сможет ли клиент его интерпретировать)
            if (!$.client.config.uriParsingReg.test(location.hash)) return;


            // URI изменился
            if (location.hash != $.client._lastURI) {
                // новый URI
                $.client._lastURI = location.hash;


                var uriRegMatches = $.client.config.uriParsingReg.exec(location.hash);
                var uriParsedArguments = Array();

                // интерпретация аргументов нового URI
                // в соответствии с настройками	в конфиге
                for (var index in uriRegMatches) {
                    // аргумент может быть не предусмотрен
                    if ($.client.config.uriParsingRegArguments[index])
                        var argumentName = $.client.config.uriParsingRegArguments[index];
                    else continue;

                    uriParsedArguments[argumentName] = uriRegMatches[index];
                }

                // обработка нового действия  ставится в очередь,
                // чтобы не мешать исполнению незавершённых
                // процессов клиента
                $(document).queue(function () {
                    $.client._atChangeURI(uriParsedArguments);

                    // следующий
                    $(document).dequeue();
                })
            }
        },


        /**
         * обработка смены URI
         * @param object arguments объект с аргументами,
         * распарсенными из URI функцией _checkForChangeURI,
         * в соответствии с настройками в конфиге
         */
        _atChangeURI: function (arguments) {
            $(document).trigger("onClientChangeURI", arguments);

            /**
             * @TODO: не доделал
             */


        },

        /**
         * @var integer количество незавершённых операций,
         * блокирующих страницу
         */
        _pageLocks: 0,

        /**
         * блокирование страницы на время вызовов;
         * по умолчанию не производится никаких действий,
         * не считая того, что ведётся учёт - сколько операций
         * блокируют страницу в данный момент;
         *
         * чтобы изменить поведение страницы при блокировании/разблокировании,
         * используйте события onClientLock и onClientUnlock
         */
        _lockPage: function (initiator) {
            if ($.client._pageLocks == 0)
                $(document).trigger("onClientLock", [initiator, $.client._pageLocks]);

            // увеличение счётчика активных операций
            $.client._pageLocks++;
        },

        /**
         * разблокирование страницы
         */
        _unlockPage: function (initiator) {
            // уменьшение счётчика активных операций
            if ($.client._pageLocks > 0) $.client._pageLocks--;

            if ($.client._pageLocks == 0)
                $(document).trigger("onClientUnlock", [initiator, $.client._pageLocks]);
        },


        /**
         * вывод сообщения
         * @param string messageClass класс выводимого сообщения (success, fail, error, info и т.д.)
         * @param string message текст сообщения
         */
        message: function (messageClass, message) {
            // чтобы расширить функционал сообщений, перегрузите _message
            $.client._message.apply(this, [messageClass, message]);

            // также, для расширения функционала вывода сообщений,
            // можно использовать событие
            $(document).trigger("onClientMessage", [this, $.client._pageLocks]);

        },

        // пустая функция отправки сообщения
        _message: function (messageClass, message) {
        },


        // сообщения (интернационализация)
        __: function (message_id) {
            // в конфиге есть объект messages,
            // хранящий используемые сообщения
            if ($.client.messages[message_id])
                return $.client.messages[message_id];
            else return message_id;
        }

    }



});


// расширения клиент-сереверной автоматизации
// для объектов JQuery
jQuery.fn.extend({

    /**
     * заполнение отдельного элемента
     **/
    fill: function (value) {
        var element = $(this);

        // для массивов есть другая функция -
        // setFields
        if ((typeof value == 'array') ||
            (typeof value == 'object'))
            return element.setFields(this);

        // для элементов формы указывается val(),
        // для изображений src, для остальных - html()

        if (element.is('input') || element.is('textarea') || element.is('select'))
            element.val(value);

        else if (element.is('img')) element.attr('src', value);
        else {
            if (element.hasClass('clientContentAppend')) {
                element.append(value);
                $(this)._unlockElement();
                $.client._unlockPage(this);
            }

            else if (element.hasClass('clientContentPrepend')) {
                element.prepend(value);
                $(this)._unlockElement();
                $.client._unlockPage(this);
            }

            else if (element.hasClass('clientContentSlideLeft')) {
                var originalWidth = element.outerWidth(),
                    originalHeight = element.outerHeight();

                element.removeClass('clientContentSlideLeft').addClass('clientBusy');

                // Фиксирование элемента в текущих размерах.
                element.css('width', originalWidth + 'px').css('height', originalHeight + 'px')
                    .css('position', 'relative').css('overflow', 'hidden');

                // Страница со старым содержимым.
                element.wrapInner(
                    '<div class="clientSliderPage clientSliderPageOld" ' +
                        'style="position:absolute; width:' + originalWidth + 'px; ' +
                        'height:' + originalHeight + 'px; left:0; top:0;">' +
                        '</div>'
                );

                // Общий контейнер, в который вкладывается старое и новое содержимое,
                // после чего он пролистывается.
                element.wrapInner(
                    '<div class="clientSliderContainer" ' +
                        'style="position:absolute; overflow:visible;  ' +
                        'width:' + originalWidth + 'px; height:' + originalHeight + 'px;">' +
                        '</div>'
                );

                // Страница с новым содержимым.
                element.find('.clientSliderPageOld').after(
                    '<div class="clientSliderPage clientSliderPageNew" ' +
                        'style="position:absolute; width:' + originalWidth + 'px; ' +
                        'left:' + originalWidth + 'px; top:0;">' +
                        '</div>'
                );

                element.find('.clientSliderPageNew').html(value);
                element.css('height', element.find('.clientSliderPageNew').outerHeight() + 'px');
                element.find('.clientSliderContainer').animate(
                    {left: '-' + originalWidth + 'px'},
                    $.client.config.dogNail.slideTime, null, function () {

                        $(this)._unlockElement();
                        $.client._unlockPage(this);

                        element.html(element.find('.clientSliderPageNew').html());
                        $(document).trigger("onPartLoad", [element, null]);

                        element
                            .css('width', 'auto').css('height', 'auto')
                            .removeClass('clientBusy')
                            .css('overflow', 'visible');
                    }
                );
            }

            else if (element.hasClass('clientContentSlideRight')) {
                var originalWidth = element.outerWidth(),
                    originalHeight = element.outerHeight();

                element.removeClass('clientContentSlideRight').addClass('clientBusy');

                // Фиксирование элемента в текущих размерах.
                element.css('width', originalWidth + 'px').css('height', originalHeight + 'px')
                    .css('position', 'relative').css('overflow', 'hidden');

                // Страница со старым содержимым.
                element.wrapInner(
                    '<div class="clientSliderPage clientSliderPageOld" ' +
                        'style="position:absolute; width:' + originalWidth + 'px; ' +
                        'height:' + originalHeight + 'px; left:0; top:0;">' +
                        '</div>'
                );

                // Общий контейнер, в который вкладывается старое и новое содержимое,
                // после чего он пролистывается.
                element.wrapInner(
                    '<div class="clientSliderContainer" ' +
                        'style="position:absolute; overflow:visible;  ' +
                        'width:' + originalWidth + 'px; height:' + originalHeight + 'px;">' +
                        '</div>'
                );

                // Страница с новым содержимым.
                element.find('.clientSliderPageOld').before(
                    '<div class="clientSliderPage clientSliderPageNew" ' +
                        'style="position:absolute; width:' + originalWidth + 'px; ' +
                        'height:' + originalHeight + 'px; left:-' + originalWidth + 'px; top:0;">' +
                        '</div>'
                );

                element.find('.clientSliderPageNew').html(value);
                element.find('.clientSliderContainer').animate(
                    {left: originalWidth + 'px'},
                    $.client.config.dogNail.slideTime, null, function () {

                        $(this)._unlockElement();
                        $.client._unlockPage(this);

                        element.html(element.find('.clientSliderPageNew').html());
                        $(document).trigger("onPartLoad", [element, null]);

                        element
                            .css('width', 'auto').css('height', 'auto')
                            .removeClass('clientBusy')
                            .css('overflow', 'visible');
                    }
                );
            }

            else if (element.hasClass('clientContentReplace')) {
                element.replaceWith(value);
                $(document).trigger("onPartLoad", [element, null]);

                $(this)._unlockElement();
                $.client._unlockPage(this);
            }

            else {
                element.html(value);
                $(document).trigger("onPartLoad", [element, null]);

                $(this)._unlockElement();
                $.client._unlockPage(this);
            }
        }
    },


    /**
     * на время вызовов можно блокировать отдельный элемент
     */
    _lockElement: function () {
        // для всех занятых элементов устанавливается
        // класс "clientBusy" - занято
        $(this).addClass("clientBusy");

        // вызов события для этого - блокировка элемента
        $(this).trigger("onClientElementLock", [this]);
    },

    _unlockElement: function () {
        // вызов события - блокировка элемента
        $(this).removeClass("clientBusy");

        // вызов события - разблокирование элемента
        $(this).trigger("onClientElementUnlock", [this]);
    },


    // сообщение для этого элемента
    _message: function (messageClass, message) {
        // отправляется сообщение через общий интерфейс сообщений
        $.client.message(messageClass, message);

        // внешнее сообщение
        var outMessage = '<div class="clientMessage clientMessage_' + message + '"></div>';


        // сообщение выводится прямо внутри элемента
        if (this.hasClass("clientMessage_inner")) {
            $(this).html(message_tag);
            $(this).addClass(message);
        }

        // или рядом с элементом
        else if (this.hasClass("clientMessage_left")) $(this).before(outMessage)
        else if (this.hasClass("clientMessage_right")) $(this).after(outMessage)
    },


    message: function (messageClass, message) {
        this._message.apply(this, [messageClass, message]);

        // обработка события "сообщение" только для этого элемента
        $(this).trigger("onClientMessage", [this, $.client._pageLocks]);
    },


    /**
     * обработка элементов массивов: если объект иммет свойство NAME,
     * используемое как имя массива - ABC[], значение добавляется к
     * соответствующему полю массива; в противном случае, возвращается FALSE
     * @param object dest массив/объект, к которому будет добавлено значение в ABC[]
     * @paran mixed overactedValue значение может быть утрировано - сведено к логическому
     * (overactedValue или отсутствие всякого значения) - для обработки логических массивов
     */
    _arrayValue: function (dest, overactedValue) {
        /** @TODO кажется в этой функции нет смысла */

        var elementName = $(this).attr('name');


        // функция имеет смысл для тех элементов, у которых есть имя
        if (!elementName) return false;


        // сверка по регекспу
        var name_pattern = $.client.config.arrayNameReg;
        if (!name_pattern.test(elementName))
            return false;

        // выдернутые значения
        var name_matches = name_pattern.exec(elementName);
        var name = name_matches[1];
        var index = name_matches[2];


        // первое вхождение массива, к которому относится элемент
        if (!dest[name]) {
            dest[name] = new Object;
        }


        // утрированное значение
        if (overactedValue) {
            /** @TODO с overactedValue не всё понятно **/

            if ($(this).val())
                if (this.val()) dest[name].push(this.val());
        }

        // обычное значение
        else dest[name][index.replace(/[\[\]]/g, '')] = $(this).val();


        return true;
    },


    /**
     * @return object данные, собранные в элементе
     */
    getFields: function () {

        // место сбора данных
        var dataCollection = new Array();

        // конфигурация для функции
        // (задаётся в конфиге клиента)
        var config = $.extend(new Object, $.client.config.fieldsGrabber);


        // обычные поля ввода (текстовые данные)
        $(config.selectorSimple, this).each(function () {
            // выключенные элементы
            if ($(this).hasClass('clientDisabled') ||
                $(this).attr("disabled"))
                return;


            if (this.name) {
                if (this.type == 'checkbox') {
                    // обработка чек-боксов
                    if (!$(this)._arrayValue(dataCollection)) {
                        if ($(this).attr('checked')) {
                            if (!dataCollection[this.name]) {
                                dataCollection[this.name] = [];
                            }
                            dataCollection[this.name].push($(this).val());
                        }
                    }
                } else {
                    // обработка как массива
                    if (!$(this)._arrayValue(dataCollection))
                        dataCollection[this.name] = $(this).val();
                }
            }

            else if (this.id)
                dataCollection[this.id] = $(this).val();

        });

        // поля простого выбора (как правило, radio button)
        $(config.selectorOption, this).each(function () {
            // выключенные элементы
            if ($(this).hasClass('clientDisabled') ||
                $(this).attr("disabled"))
                return;


            var cur_value = false;

            if ($(this).attr("checked")) cur_value = $(this).val();

            // невыделенные элементы записываются только при первой
            // встрече с элементами их группы
            else if (dataCollection[this.name]) return;


            // записывается только по NAME
            if (this.name) {

                if (!is_array_input(this, result_data, cur_value))
                    result_data[this.name] = cur_value;
            }
        });


        // данные помимо полей формы
        $(config.selectorData, this).each(function () {
            // выключенные элементы
            if ($(this).hasClass('clientDisabled')) return;

            dataCollection[this.id] = $(this).val();
        });

        return dataCollection;
    },

    /**
     * заполняет поля в элементе соответсвующими данными
     */
    setFields: function (data) {
        // проход по всем свойствам проекта
        for (index in data) {
            // селектор элементов, которые могут относиться
            // индексу массива
            var indexSelector = ".clientData#" + index + ", " +
                "input#" + index + ", " +
                "select#" + index + ", " +
                "textarea#" + index + ", " +
                "[name=" + index + "]";


            // могут придти вложенные данные
            if ($.isArray(data[index])) {
                // рекурсивное заполнение (подформа типа)
                if ($(indexSelector).length)
                    $(indexSelector).setFields(data[index]);

                // заполнение элемента-массива
                else {
                    for (var count = 0; count < $(indexSelector).length; count++) {
                        // кончились элементы в массиве
                        if (count >= data[index].length) break;

                        // заполнение элемента
                        $($(indexSelector).get(count)).fill(data[index][count]);
                    }
                }

            }

            // обычное заполнение элементов
            else $(indexSelector).each(function () {
                $(this).fill(data[index]);
            });

        }
    },


    /**
     * обработка результата - текст
     */
    _parseRequestResultType_text: function (requestResult, url) {
        // чтобы текст отображался в элементе, у него должен
        // стоять отдельный класс
        if (!$(this).hasClass("clientContent")) return false;
        $(this).fill(requestResult);
        $(this).trigger("onClientParseRequest_text", [this, requestResult]);

        // смена URI
        if ($.client.config.workPlace
            && (($(this).attr('id') == $.client.config.workPlace)
            || ($(this).attr('name') == $.client.config.workPlace))) {
            if ($.client.config.changeUrlOnAjax) {
                /**
                 * @TODO: сделать интерпретацию URL при неработающем history.pushState
                 */
                if (history.pushState)
                    history.pushState({elementId: $.client.config.workPlace}, null, url);
                else location.hash = 'ajax:' + ($(this).attr('id') ? $(this).attr('id') : $(this).attr('name')) + url;
            }

        }

        return true;
    },


    /**
     * обработка результата - XML
     */
    _parseRequestResultType_xml: function (requestResult, url) {
        /**
         * @TODO: обработка XML данных не реализована;
         * можно воспользоваться событием, описанным ниже,
         * но лучше функцию дописать или расширить
         **/

        $(this)._unlockElement();
        $.client._unlockPage(this);

        // вызов события "получение XML-данных"
        $(this).trigger("onClientParseRequest_xml", [this, requestResult]);
    },

    /**
     * обработка результата - HTML
     */
    _parseRequestResultType_html: function (requestResult, url) {
        // вызов события "получение HTML-данных"
        $(this).trigger("onClientParseRequest_html", [this, requestResult]);

        // и работает как с простым текстом
        return  $(this)._parseRequestResultType_text(requestResult);
    },


    /**
     * в ответе от сервера могут приходитьисполнение
     */
    _parseServerCommands: function (requestResult, url) {
        $.client._unlockPage(this);

        // уже использованные комманды
        var usedCommands = Array();

        // проходит по всем коммандам в том порядке, в котором
        // они расставлены в конфиге
        for (command in $.client.config.serverCommands) {
            // эта комманда  не приходила с сервера
            if (!requestResult.__commands[command]) continue;

            // вызов обработчика
            if (!$.client.config.serverCommands[command].apply(this,
                [requestResult.__commands[command], requestResult, usedCommands, url]))

            // какая-то из комманд может прервать очередь выполнения
            // комманд
                break;


            // учёт уже выполненных комманд
            usedCommands.push(command);
        }

        $(this)._unlockElement();

    },

    /**
     * можно указать обработчик JSON-комманд, который будет перехватывать
     * все JSON-данные, приходящие от запросов
     *
     * если функция вернёт true, по её выполнении, обработчик попытается
     * заполнить элемент данными, полученными от сервера, если false, на
     * этом всё и остановится
     */
    _useJSON: function (requestResult) {
        return true;
    },


    /**
     * обработка результата - JSON - объект
     */
    _parseRequestResultType_json: function (requestResult, url) {
        // выполение команд сервера
        if (requestResult.__commands) $(this)._parseServerCommands(requestResult, url);
        else {
            $(this)._unlockElement();
            $.client._unlockPage(this);
        }

        // своя обработка данных для этого жлемента
        if ($(this)._useJSON(requestResult))

        //заполнение объекта этими данными
            $(this).setFields(requestResult);

        // вызов события "получение JSON-данных"
        $(this).trigger("onClientParseRequest_json", [this, requestResult]);
    },


    /**
     * предобработка результата запроса: определение типа данных
     * результата и перенаправление на сооьвеьсвующиый обработчик
     */
    _parseRequestResultType: function (requestResult, url) {
        // обработка простого текста
        if (typeof requestResult == "string")
            return $(this)._parseRequestResultType_text(requestResult, url);

        else if (typeof requestResult == "object") {

            if (requestResult.contentType) {
                // XML
                if (requestResult.contentType == "text/xml")
                    return  $(this)._parseRequestResultType_xml(requestResult, url);

                // HTML
                if (requestResult.contentType == "text/html")
                    return  $(this)._parseRequestResultType_html(requestResult, url);
            }

            // по умолчанию объект - это JSON
            $(this)._parseRequestResultType_json(requestResult, url);

        }

        // не знает как интерпретировать
        return false;

    },


    /**
     * не удалось однозначно интерпретировать резльтат запроса
     */
    _parseRequestError: function (requestResult, url) {
        $(this)._unlockElement();
        $.client._unlockPage(this);

        // событие - необработанный результат
        $(this).trigger("onClientParseRequest_error", [this, requestResult, url]);
    },


    /**
     * обращение к серверу
     */
    request: function (url, post, callbackBefore, callbackAfter) {
        var requestSubject = this;

        // конкретный элемент лочится до отправки запроса
        $(requestSubject)._lockElement();

        // создаётся очередь
        $(document).queue(function () {
            if (!post) post = {};
            $(requestSubject).trigger('onBeforeRequest', {url: url, post: post});

            // параметры запроса
            var requestOptions = $.extend(new Object, $.client.config.ajaxOptions);
            requestOptions.url = url;

            // можно указать данные, которые постоянно будут
            // отправляться вместе с любым запросом
            requestOptions.data = $.extend(new Object, requestOptions.data, post);

            // если в конфиге указан другой обработчик, он будет иметь
            // приоритет перед тем, который используется здесь
            // callbackBefore и callbackAfter этого вызова будут прикреплены к результату
            if (!requestOptions.success) {
                // успешная обработка запроса
                requestOptions.success = function (request_result, status) {
                    /**
                     * @TODO: не совсем ясно событиями callbackBefore и callbackAfter:
                     * что передавать в качестве this, и что делать аругментами;
                     * нужно проверить корректный приход данных
                     */

                    // события вызовов до и после
                    if (!callbackBefore)
                        callbackBefore = $.client.config.ajaxAdvancedOptions.callbackBefore;
                    if (!callbackAfter)
                        callbackAfter = $.client.config.ajaxAdvancedOptions.callbackAfter;

                    // вызов события перед обработкой запрса
                    if (callbackBefore) callbackBefore.apply(this, [request_result, requestOptions]);

                    // принятие решение о дальнейшей судьбе результата запроса
                    if (!$(requestSubject)._parseRequestResultType(request_result, url))
                        $(requestSubject)._parseRequestError(request_result, url);

                    // вызов после оработки запроса
                    if (callbackAfter) callbackAfter.apply(this, [request_result, requestOptions]);
                }
            }

            else {
                var definedSuccessCallback = requestOptions.success;

                // вызов переопределённой функции из конфига
                // колбэки callbackBefore и callbackAfter будут прикреплены
                // к результату запроса
                requestOptions.success = function (request_result, status) {
                    // переданные колбэки
                    request_result.callbackAfter = callbackAfter;
                    request_result.callbackBefore = callbackBefore;


                    // вызов функции из конфига
                    definedSuccessCallback.apply(this, [request_result, status])
                }
            }

            // обработка неудачного обращения
            requestOptions.error = function (request_result, status) {
                $(requestSubject)._unlockElement();
                $.client._unlockPage(this);

                // ограничивается тем, что выводит сообщения
                $(requestSubject).message("requestError", "request_error");
            }

            // блокирование страницы непосредственно перед запросом
            $.client._lockPage(requestSubject);

            // запрос
            $.ajax(requestOptions);


            // следующий
            $(document).dequeue();

        });
    }

});
$.client.run();
